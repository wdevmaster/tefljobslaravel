<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::any('/logoff', 'Auth\LoginController@logout')->name('logout');


Route::get('/', function () {
    return view('auth.login');
})->name('main')->middleware('guest');

Route::get('/account-request', function () {
    return view('auth.accountRequest');
})->name('account.request');

Route::get('/{email}/resume', 'UsersController@publicProfile')->name('public.profile')->middleware('auth');

Route::middleware('auth')->prefix('dashboard')->group(function(){
	Route::get('/', 'HomeController@index')->name('home');

	//ADMIN ROUTES
	Route::get('/profile', 'HomeController@profile')->name('profile');
	Route::post('/user/delete', 'UsersController@destroy')->name('users.destroy');
	Route::get('/users', 'UsersController@index')->name('users.index')->middleware('permission:users.index');
	Route::get('/user/{id}/edit', 'UsersController@edit')->name('users.edit')->middleware('permission:users.edit');
	Route::post('/user/update', 'UsersController@update')->name('users.update')->middleware('permission:users.edit');
	Route::get('/user/create', 'UsersController@create')->name('users.create')->middleware('permission:users.create');
	Route::post('/user/store', 'UsersController@store')->name('users.store')->middleware('permission:users.create');
	Route::post('/user/change/password', 'UsersController@changePassword')->name('users.password');
	Route::post('/user/admin/profile/update', 'UsersController@adminPerfilUpdate')->name('user.admin.profile');
	Route::post('/admin/user/delete', 'UsersController@delete')->name('user.delete');

	//EMPLOYE ROUTES
	Route::get('/employer/my-offers', 'EmployerController@myOffers')->name('employer.offers');
	Route::get('/employer/my-offers/create', 'EmployerController@myOffersCreate')->name('employer.offers.create');
	Route::post('/employer/my-offers/store', 'EmployerController@myOffersStore')->name('employer.offers.store');
	Route::post('/employer/my-offers/destroy', 'EmployerController@myOffersDestroy')->name('employer.offers.destroy');
	Route::post('/employer/profile/update', 'EmployerController@profileUpdate')->name('employer.profile.update');
	Route::get('/employer/offer/{id}/{title}', 'OffersController@offerShow')->name('offer.show');
	Route::get('/employer/offer/{id}/{title}/edit', 'OffersController@offerEdit')->name('offer.edit');
	Route::put('/employer/offer/update', 'OffersController@offerUpdate')->name('offer.update');
	Route::post('/employer/offer/delete', 'OffersController@offerDelete')->name('offer.delete');
	Route::get('/employer/offer/{id}/{title}/candidates', 'OffersController@offerShowCandidates')->name('offer.candidates.show');

	Route::get('/employer/candidates', 'CandidatesController@candidatesIndex')->name('candidates.index');
	Route::get('/employer/approve/postulation', 'CandidatesController@approvePostulation')->name('approve.postulation');
	Route::get('/employer/candidate/{email}/offer/{postulation_id}/resume', 'CandidatesController@resumeCandidate')->name('candidate.resume');
	Route::post('/employer/candidate/acepted', 'CandidatesController@resumeAcepted')->name('candidate.acepted');

	//FEEDBACK ROUTES
	Route::get('/employer/feedback', 'ReviewController@index')->name('employer.feedback');
	Route::get('employer/{id}/review/create', 'ReviewController@create')->name('review.create');
	Route::post('employer/review/store', 'ReviewController@store')->name('review.store');
	Route::post('review/update', 'ReviewController@update')->name('review.update');


	//GRADUATE ROUTES
	Route::get('/offer/{id}/{title}/detail', 'GraduateController@offerDetail')->name('offer.detail');
	Route::post('/offer/apply', 'GraduateController@offerApply')->name('offer.apply');
	Route::get('/graduate/profile', 'GraduateController@graduateProfile')->name('graduate.profile');
	Route::get('/graduate/profile/edit', 'GraduateController@graduateProfileEdit')->name('graduate.profile.edit');
	Route::post('/graduate/upload/cv', 'UsersController@uploadCV')->name('graduate.upload.cv');
	Route::post('/graduate/upload/video', 'UsersController@uploadVideo')->name('graduate.upload.video');
	Route::post('/graduate/personal/info', 'ProfilesController@personalInfo')->name('graduate.personal.info');
	Route::post('/graduate/contact/info', 'ProfilesController@contactInfo')->name('graduate.contact.info');
	Route::post('/graduate/lenguage/info', 'ProfilesController@lenguageInfo')->name('graduate.lenguage.info');
	Route::post('/graduate/reference/info', 'ProfilesController@referenceInfo')->name('graduate.reference.info');
	Route::post('/graduate/reference/delete', 'ProfilesController@referenceDelete')->name('graduate.reference.delete');
	Route::post('/graduate/education/create', 'ProfilesController@educationCreate')->name('graduate.education.create');
	Route::post('/graduate/education/delete', 'ProfilesController@educationDelete')->name('graduate.education.delete');
	Route::post('/graduate/work/create', 'ProfilesController@workCreate')->name('graduate.work.create');
	Route::post('/graduate/work/delete', 'ProfilesController@workDelete')->name('graduate.work.delete');
	Route::get('/graduate/my-Offers', 'UsersController@graduateOffers')->name('graduate.offers');
	Route::get('/graduate/my-Offers/{id}/{slu}/details', 'UsersController@graduateOfferDetails')->name('graduate.offers.details');
	Route::get('/graduate/offers', 'OffersController@offersGraduate')->name('graduate.offers.index');
	Route::post('/graduate/offers', 'OffersController@offersGraduate')->name('offers.searching');
	Route::delete('/graduate/cancel/postulation', 'OffersController@cancelPostulation')->name('offers.cancel');

	//AGENCIES
	Route::get('/agency/offers', 'AgenciesController@offers')->name('agency.offers');
	Route::get('/agency/offer/{id}/{slug}/details', 'AgenciesController@offerDetail')->name('agency.offer.detail');
	Route::get('/agency/candidates', 'CandidatesController@candidatesIndex')->name('agency.candidates');

});
