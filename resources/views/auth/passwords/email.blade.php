@extends('layouts.mainv2')

@section('title', 'Job Placement APP | Reset password request')

@section('bodyClass', 'grey lighten-5')
@section('content')
<div class="row space-login">
        <div class="col s12 space">
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s12 center">
                                    <h5>Forgot Password?</h5>
                                    <p class="grey-text text-lighten-1">Recover your account password</p>
                                </div>

                                <div class="col s12">
                                    <form class="col s12" method="POST" action="{{ route('password.email') }}">
                                    {{csrf_field()}}
                                        <div class="row">
                                            @if(Session::has('message'))
                                            <div class="col s12 message center">
                                                <p><i class="material-icons green-text left">check_circle</i>{{Session::get('message')}} An email with password reset instructions has been sent to your email address, if it exists on our system.</p>
                                            </div>
                                            @endif
                                            
                                            <div class="input-field col s12 no-movile-element">
                                                <p class="grey-text text-lighten-1 text-form">YOUR EMAIL ADDRESS:</p>
                                            </div>
                                            <div class="input-field col s12">
                                                <input id="email" name="email" type="email" class="validate input-style" placeholder="Type your email address">
                                            </div>
                                            
                                            <div class="col s12">
                                                <button type="submit" class="btn btn-access purple btn-large white-text">Send Instructions</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="col s12 center">
                                    <p class="grey-text text-lighten-1">Remember your password? <a class="purple-text" href="{{url('/')}}">Login</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection