@extends('layouts.mainv2')

@section('title', 'Job Placement APP | Reset password')

@section('bodyClass', 'grey lighten-5')
@section('content')
<div class="row space-login">
        <div class="col s12 space">
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s12 center">
                                    <h5>Reset Password!</h5>
                                    <p class="grey-text text-lighten-1">Reset your account password</p>
                                </div>

                                <div class="col s12">
                                    <form class="col s12" method="POST" action="{{ route('password.request') }}">
                                        {{csrf_field()}}
                                        <div class="row">
                                            <input type="hidden" name="token" value="{{ $token }}">
                                            <div class="input-field col s12 no-movile-element">
                                                <p class="grey-text text-lighten-1 text-form">YOUR EMAIL ADDRESS:</p>
                                            </div>
                                            <div class="input-field col s12">
                                                <input id="email" name="email" type="email" class="validate input-style" placeholder="Type your email address" value="{{ $email or old('email') }}">
                                            </div>

                                            <div class="input-field col s12 no-movile-element">
                                                <p class="grey-text text-lighten-1 text-form">NEW PASSWORD:</p>
                                            </div>
                                            <div class="input-field col s12">
                                                <input id="password" name="password" type="password" class="validate input-style" placeholder="Password">
                                                @if ($errors->has('password'))
                                                    <center>
                                                        <span class="help-block ">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    </center>
                                                    <br>
                                                @endif
                                            </div>

                                            <div class="input-field col s12 no-movile-element">
                                                <p class="grey-text text-lighten-1 text-form">CONFIRM PASSWORD:</p>
                                            </div>
                                            <div class="input-field col s12">
                                                <input id="password_confirmation" name="password_confirmation" type="password" class="validate input-style" placeholder="Confirm password">
                                            </div>
                                            
                                            <div class="col s12">
                                                <button type="submit" class="btn btn-access purple btn-large white-text">Reset password</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection