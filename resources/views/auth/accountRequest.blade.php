@extends('layouts.mainv2')

@section('title', 'Job Placement APP | Account Request')

@section('bodyClass', 'grey lighten-5')
@section('content')
<div class="row row-help">
        <div class="col s10 offset-s1">
            <!-- Modal Trigger -->
            <a class="modal-trigger right black-text" href="#modal1"><i class="material-icons">help_outline</i></a>

            <!-- Modal Structure -->
            <div id="modal1" class="modal">
                <div class="modal-content">
                    <h5 class="title-app">
                        What is <b class="purple-text color-title">Job Placement</b> APP?
                    </h5>

                    <p>Job Placement APP is a benefit for the students graduated from the TEFL course of Buenos Aires TEFL Institute, where our exclusive internal work proposals will be published for our students, if you have any additional questions you can send an email to <b class="purple-text">buenosairestefl@gmail.com</b> and we will answer you. soon as possible</p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-action modal-close btn-flat">close</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row space-login">
        <div class="col s12 center space-bottom">
            <img src="{{url('/')}}/img/v2/logo/favicon.jpg" class="logo-app">
            <h5 class="title-app">
                <b class="purple-text color-title">Job Placement</b> APP
            </h5>
        </div>

        <div class="col s12 space">
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s12 center">
                                    <h5>Account request</h5>
                                    <p class="grey-text text-lighten-1">If you need an account to access our APP, you can request it by sending an email to</p>
                                    <br>
                                    <p class="purple-text">buenosairestefl@gmail.com</p>
                                    <br>
                                    <p class="grey-text text-lighten-1">You must send us in the email attachment the reason for your request since our security policies prevent access to unauthorized users.</p>
                                </div>

                                <div class="col s12 center">
                                    <br>
                                    <a class="purple-text" href="{{ url('/') }}">Return login</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection