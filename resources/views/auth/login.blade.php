@extends('layouts.mainv2')

@section('title', 'Job Placement APP | Login')

@section('bodyClass', 'grey lighten-5')
@section('content')
<div class="row row-help">
        <div class="col s10 offset-s1">
            <!-- Modal Trigger -->
            <a class="modal-trigger right black-text" href="#modal1"><i class="material-icons">help_outline</i></a>

            <!-- Modal Structure -->
            <div id="modal1" class="modal">
                <div class="modal-content">
                    <h5 class="title-app">
                        What is <b class="purple-text color-title">Job Placement</b> APP?
                    </h5>
                    
                    <p>Job Placement APP is a benefit for the students graduated from the TEFL course of Buenos Aires TEFL Institute, where our exclusive internal work proposals will be published for our students, if you have any additional questions you can send an email to <b class="purple-text">buenosairestefl@gmail.com</b> and we will answer you. soon as possible</p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-action modal-close btn-flat">close</a>
                </div>
            </div>

        </div>
    </div>

    <div class="row space-login">
        <div class="col s12 center space-bottom">
            <img src="{{url('/')}}/img/v2/logo/favicon.jpg" class="logo-app">
            <h5 class="title-app">
                <b class="purple-text color-title">Job Placement</b> APP
            </h5>
        </div>

        <div class="col s12 space">
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s12 center">
                                    <h5>Login</h5>
                                    <p class="grey-text text-lighten-1">Access your account</p>
                                </div>
                                @if ($errors->has('email'))
                                    <div class="col s12 message center">
                                        <p><i class="material-icons red-text left">highlight_off</i> {{ $errors->first('email') }}</p>
                                    </div>
                                @endif
                                <div class="col s12">
                                    <form class="col s12" method="POST" action="{{ route('login') }}">
                                    {{ csrf_field() }}
                                        <div class="row">
                                            <div class="input-field col s12 no-movile-element">
                                                <p class="grey-text text-lighten-1 text-form">YOUR EMAIL ADDRESS:</p>
                                            </div>
                                            <div class="input-field col s12">
                                                <input id="email" name="email" type="email" class="validate input-style" placeholder="Type your email address">
                                            </div>
                                            <div class="input-field col s12 no-movile-element">
                                                <p class="grey-text text-lighten-1 text-form">YOUR PASSWORD:</p>
                                            </div>
                                            <div class="input-field col s12 space-mobile">
                                                <input id="password" name="password" type="password" class="validate input-style" placeholder="Type your password">
                                            </div>
                                            <div class="col s12">
                                                <button type="submit" class="btn btn-access purple btn-large white-text">Login</button>
                                            </div>

                                            <div class="col s12 center">
                                                <a class="grey-text text-lighten-1" href="{{ route('password.request') }}">Forgot password?</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="col s12 center">
                                    <p class="grey-text text-lighten-1">Your don't have an account yet? <a class="purple-text" href="{{ route('account.request') }}">Click here</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection