<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans|Oswald" rel="stylesheet"> 
	<style>
		.main{
			background-color: #EEEEEE;
			border-radius: 10px;
			padding-bottom: 50px;
			;
		}
		.container{
			font-family: 'Open Sans', sans-serif;
			width: 70%;
			margin: 0 auto;
			background-color: #fff;
			border-radius: 10px;
			margin-bottom: 50px;
			padding: 30px;
		}
		.logo{
			font-family: 'Oswald', sans-serif;
			margin-top: 50px;
			width: 70%;
			margin: 0 auto;
			color: #483E2E;
		}	
		.boton{
      		text-shadow: 0px 1px rgba(0, 0, 0, 0.2);
            text-align:center;
            text-decoration: none;
      		font-family: 'Helvetica Neue', Helvetica, sans-serif;
      		display:inline-block;
            color: #FFF;
            background: #7F8C8D;
            padding: 7px 20px;
            white-space: nowrap;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 100px;
            margin: 10px 5px;
            -webkit-transition: all 0.2s ease-in-out;
            -ms-transition: all 0.2s ease-in-out;
            -moz-transition: all 0.2s ease-in-out;
            -o-transition: all 0.2s ease-in-out;
            transition: all 0.2s ease-in-out;
}

.verde:hover{
  background: #32db78;
}
.verde:active{
  box-shadow: 0px 2px 0px 0px #6A1B9A;
}

.verde{
  background: #6A1B9A;
  box-shadow: 0px 5px 0px 0px #6A1B9A;
}
	</style>
</head>
<body>
	<div class="main">
	<br>
<!-- 		<div class="logo">
			<img src="" style="width: 50px" alt="">
			<h2 style="display: inline;"> Recibiste un mensaje desde Delucchi App</h2><br>
		</div> -->
		<div class="container">
			<center>
				<p><b>Congratulations!</b> Your application for the vacancy<br>
					<b>"{{$job->title}}"</b> has been send. Soon the employer will contact you. <b>Good luck...!</b>
				</p>
			</center>

			<center>
				<a href="{{url('/')}}" class="boton verde">Go to Dashboard</a>
			</center>
		</div>
	</div>
</body>
</html>