  <!-- Modal Structure -->
  <div id="submitModal" class="modal">
    <div class="modal-content" id="applyContent">
        <center>
           <img src="{{url('/')}}{{$job->user->path_avatar}}" class="profile-img" style="width: 90px;height: 90px;"><br><br>
             <h6 id="applyHeader">¿Dou you want apply for this vacant? <br><br>  <b>"{{$job->title}}"</b><br><br></h6><br>
          <center>
            <form action="{{ route('jobs.application') }}" style="display: inline" id="sendApplycationForm">
              {{ csrf_field() }}
              <input type="hidden" value="{{$job->id}}" name="job_id">
              <input type="hidden" value="{{Auth::user()->id}}" name="user_id">
              <button type="submit" class="blue darken-4 btn btn-tefl" data-job="{{$job->id}}" data-user="{{Auth::user()->id}}" id="sendApliccation">Send Application</button>
            </form>

            <a class="waves-effect waves-blue btn-flat modal-close" id="cancelApply" href="#">Cancel</a>
            <button class="blue darken-4 btn btn-tefl modal-close" style="visibility: hidden;" id="closeApply">Close</button>
          </center>
        </center>
        <br>
    </div>
  </div>