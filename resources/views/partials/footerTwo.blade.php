<footer class="page-footer transparent">
    <div class="footer-copyright">
        <div class="container center grey-text">
            © 1999 -
            <script>
                document.write(new Date().getFullYear());

            </script> All rights reserved Buenos Aires TEFL Institute.
        </div>
    </div>
</footer>