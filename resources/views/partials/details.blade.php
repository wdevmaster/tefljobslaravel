@extends('layouts.main')

@section('title', 'Details')

@section('keywords', '')

@section('description', '')

@section('bodyClass', '')
@section('content')
	@include('navs.mainNav')
    <div class="row dashboard-space">
        <div class="col s12">
            <div class="col s12">
                <div class="card white card-space">
                    <div class="row">
                        <div class="col s12 m4 l4 center">
                            <center>
                                <img src="{{url('/')}}/img/data/des.png" class="profile-img" style="margin-top:90px;width: 90px;height: 90px;">
                            </center>
                            <h5 class="grey-text center">Wall Street English</h5>
                            <a href="https://www.wallstreetenglish.com.ar/" target="_blank" class="purple-text text-darken-4"><b>Go site</b></a>

                            <p><i class="material-icons left" style="margin:0px -30px 0px 40px;">email</i> sm.recoleta@wsearg.com.ar</p>

                            <p><i class="material-icons left" style="margin:0px -30px 0px 40px;">location_on</i>Buenos Aires - Argentina</p>

                            <center>
                                <a class="blue darken-4 btn btn-tefl" href="submit.php">submit application</a>
                            </center>
                        </div>
                        <div class="col s12 m8 l8" align="justify">
                            <p>At Wall Street English, teaching English is about making a difference in our students’ lives. Our goal is to help people change their futures and realize their dreams. We need professionals like you who seek an international experience and desire to have an impact on people’s lives.</p>

                            <p><b class="purple-text text-darken-4">Our benefits</b><br>
                                We are committed to our employees and we are proud to offer competitive packages and benefits that really add value for our staff. Our benefits include development opportunities on both a local and global level. We really are an employer committed to you.</p>

                            <p><b class="purple-text text-darken-4">Employee training</b><br>

                                As the only ISO certified English Language Teaching School in the market, Wall Street English understands the importance of providing consistent quality learning content and delivery to its staff. Our carefully designed SOPs provide the same level of excellence in our training content to staff across the globe. Our solid career development paths and employee follow-up mean you will be supported every step of the way to grow and excel throughout your career with us.</p>

                            <p><b class="purple-text text-darken-4">Professional development</b><br>

                                How many careers today involve a multicultural environment and communication, time management, organization, training, and presentation skills? Teaching English abroad with Wall Street English incorporates and develops all of these transferable skills. Learning is at the heart of our organization and on-going professional development is part of our company culture. Seminars and classroom observations provide forums where teachers can meet like-minded colleagues and develop their teaching techniques. With a promote-from-within policy, our Managers are dedicated to mentoring teachers’ potential and encouraging progression into management positions.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col s12">
            <br><br>
            <h5 class="purple-text text-darken-4 center">Recently published offers</h5>
            <br><br>

            <a href="details">
                <div class="col s12 m3 l3">
                    <div class="card white card-job">
                        <div class="row">
                            <div class="col s12">
                                <center>
                                    <img src="{{url('/')}}/img/data/des.png" class="job-profile-img">
                                </center>

                                <p class="purple-text text-darken-4 center"><b>Wall Street English School </b></p>
                                <center>
                                    <a class="blue darken-4 btn btn-tefl" href="details.php">More Info</a>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </a>

            <div class="col s12 m3 l3">
                <div class="card white card-job">
                    <div class="row">
                        <div class="col s12">
                            <center>
                                <img src="{{url('/')}}/img/data/iKBIkjbJ_400x400.jpg" class="job-profile-img">
                            </center>

                            <p class="purple-text text-darken-4 center"><b>Vacant language teacher</b></p>
                            <center>
                                <a class="blue darken-4 btn btn-tefl">More Info</a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col s12 m3 l3">
                <div class="card white card-job">
                    <div class="row">
                        <div class="col s12">
                            <center>
                                <img src="{{url('/')}}/img/data/pilsen-cargill-25-kg-D_NQ_NP_917902-MLA25569194228_052017-F.jpg" class="job-profile-img">
                            </center>

                            <p class="purple-text text-darken-4 center"><b>Vacant language teacher</b></p>
                            <center>
                                <a class="blue darken-4 btn btn-tefl">More Info</a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col s12 m3 l3">
                <div class="card white card-job">
                    <div class="row">
                        <div class="col s12">
                            <center>
                                <img src="{{url('/')}}/img/data/Logo-Globant.jpg" class="job-profile-img">
                            </center>

                            <p class="purple-text text-darken-4 center"><b>Vacant language teacher</b></p>
                            <center>
                                <a class="blue darken-4 btn btn-tefl">More Info</a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.footerTwo')
@endsection