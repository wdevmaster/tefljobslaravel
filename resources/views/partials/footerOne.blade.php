    <footer class="page-footer back">
        <div class="container space-footer">
            <div class="row">
                <div class="col s12 m6 l6">
                    <center>
                        <img src="{{url('/')}}/img/logo/logo%20telf%202.png" class="footer-logo">
                    </center>
                    
                    <p align="justify">All our programs are based on the principal of equal opportunity for all, irrespective of gender, marital status, sexual orientation, creed, color, race, ethnic origin, age or disability.</p>
                </div>
                <div class="col s12 m6 l6 center space-top">
                    <h5 class="white-text center">Follow us</h5>
                    <p align="center">
                        <a href="#" target="_blank"><i class="fab fa-facebook-square footer-icon"></i></a>
                        <a href="#" target="_blank"><i class="fab fa-twitter footer-icon"></i></a>
                        <a href="#" target="_blank"><i class="fab fa-instagram footer-icon"></i></a>
                        <a href="#" target="_blank"><i class="fab fa-youtube footer-icon"></i></a>
                    </p>
                    <p><i class="fas fa-map-marker-alt"></i> Avenida Coronel Díaz 1736, 1425 CABA, Argentina</p>
                    <p><a style="text-decoration:none;" class="white-text" target="_blank" href="https://api.whatsapp.com/send?phone=54932644289"><i class="fab fa-whatsapp fa-nav-top"></i> +54 9 11 3264-4289</a></p>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container center">
                © 1999 -
                <script>
                    document.write(new Date().getFullYear());

                </script> All rights reserved Buenos Aires TEFL Institute.
            </div>
        </div>
    </footer>