<!DOCTYPE html>
<html lang="en-USA" id="top" class="scrollspy">

<head>
    <title>@yield('title')</title>

    <!-- Metas -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="TechMov">
    <meta name="theme-color" content="">
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">

    <!-- Favicon -->
    <link rel="icon" href="{{url('/')}}/img/v2/logo/favicon.jpg" type="image/jpg"/>
    <link rel="shortcut icon" href="{{url('/')}}/img/v2/logo/favicon.jpg" type="image/jpg"/>

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Import fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/v2/materialize.min.css') }}" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/v2/style.css') }}" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/v2/movile.css') }}" media="screen,projection" />

    <!-- awesome fonts-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Righteous" rel="stylesheet">
    @yield('head')
</head>

<body class="@yield('bodyClass')">
    @yield('content')
    <!-- FOOTER -->
    <footer class="page-footer transparent">
        <div class="footer-copyright transparent">
            <div class="container grey-text center">
                <p>
                    © 1999 -
                    <script>
                        document.write(new Date().getFullYear());

                    </script> All rights reserved Buenos Aires TEFL Institute.
                </p>
            </div>
        </div>
    </footer>

    <!--  Scripts-->
    <script src="{{ asset('js/v2/jquery.js') }}"></script>
    <script src="{{ asset('js/v2/materialize.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $(".button-collapse").sideNav();

            $('select').material_select();

            $('.scrollspy').scrollSpy();

            $('.modal').modal();

            $('.slider').slider();

            $('input#input_text, textarea#textarea2').characterCounter();

            $('.dropdown-button').dropdown('');

            $('.carousel.carousel-slider').carousel({
                fullWidth: true
            });
            $('.collapsible').collapsible();

            $('.datepicker').pickadate({
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 15 // Creates a dropdown of 15 years to control year,
            });
        });
    </script>
    @yield('scripts')
    <!-- END FOOTER -->
</body>

</html>
