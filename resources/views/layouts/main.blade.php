<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>

    <!-- Metas -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="TechMov">
    <meta name="theme-color" content="#ededed">
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">

    <!-- Favicon -->
    <link rel="icon" href="{{url('/')}}/img/logo/logo.png" type="image/png"/>
    <link rel="shortcut icon" href="{{url('/')}}/img/logo/logo.png" type="image/png"/>

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Import fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.css') }}" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/style.css') }}" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/movile.css') }}" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/extras.css') }}" media="screen,projection" />
    <!-- google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">

    @yield('head')
</head>
<body class="@yield('bodyClass')">
    @yield('content')
    
    <!--  Scripts-->
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/materialize.js') }}"></script>
    <script>
        $(document).ready(function() {
            $(".button-collapse").sideNav();
        });

        $(document).ready(function() {
            $('select').material_select();
        });

        $(document).ready(function() {
            $('.modal').modal();
        });

        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.fixed-action-btn');
            var instances = M.FloatingActionButton.init(elems, {
                direction: 'left'
            });
        });

        $(document).ready(function() {
            $('.slider').slider();
        });

        $(document).ready(function() {
            // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
            $('.modal').modal();
        });

        $(document).ready(function() {
            $('input#input_text, textarea#textarea2').characterCounter();
        });

        $(document).ready(function() {
            $('.dropdown-button').dropdown('');

            $(".dropdown-trigger").dropdown();
        
        });

    </script>
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    @yield('scripts')
</body>
</html>