@extends('layouts.mainv2')

@section('title', 'User Edit')

@section('bodyClass', 'grey lighten-5')
@section('content')
    @include('navs.navCore') 
    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav') 
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">Users</b></p>
                </div>

                <div class="col s12">
                    <div class="message-dashboard inter-space">
                        <div class="row">
                            <div class="col s12 m3 l3">
                                <div class="profile-avatar" style="{{ $user->path_avatar ? 'background: none; background-color: white;' : '' }}">
                                    <img src="{{url('/')}}{{$user->path_avatar}}">
                                </div>
                                <p class="grey-text" align="justify">When a new user is registered, an email is sent with the information created, their password and their user email</p>
                            </div>

                            <div class="col s12 m9 l9">
                                <form class="col s12" method="POST" action="{{ route('users.update') }}">
                                	{{ csrf_field() }}
                                	<input type="hidden" name="user_id" value="{{$user->id}}">
                                    <p>Remember all fields are required.</p>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input placeholder="Type the name" id="name" name="name" type="text" class="validate input-delete" value="{{$user->name}}" required>
                                        </div>

                                        <div class="input-field col s12">
                                            <input placeholder="Type the email" id="email" name="email" type="email" class="validate input-delete" value="{{$user->email}}" required>
                                        </div>

                                        <div class="input-field col s12">
                                            <select class="icons input-delete" name="role">
                                                <option value="" disabled>Choose the level user</option>
                                                @foreach($roles as $role)
                                                <option {{$user->roles->first()->id == $role->id ? 'selected' : ''}} 
                                                        value="{{$role->id}}">
                                                    {{$role->name}}
                                                </option>
												@endforeach
                                            </select>
                                        </div>

                                        <div class="input-field col s12">
                                            <input placeholder="Type the password" id="password" type="password" class="validate input-delete" name="password">
                                        </div>

                                        <div class="col s12">
                                            <div class="row">
                                                <div class="col s8">
                                                    <button type="submit" class="btn purple white-text right">save</button>
                                                </div>
                                                <div class="col s4">
                                                    <button id="btn-delete" data-id="{{$user->id}}" type="button" class="btn red white-text right">delete</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script src="{{ asset('js/sweetalert.min.js') }}"></script>
<script>
    $('#btn-delete').click(function (e) {
        e.preventDefault()
        let id = $(this).data('id')
        swal({
            title: "Are you sure?",
            text: "Once it is deleted, it will not be able to recover!",
            icon: "warning",
            buttons: ["Cancel", {
                text: "Delete",
                closeModal: false,
            }],
            dangerMode: true,
        }).then((willDelete) => {
            if (!willDelete) throw null;
            return fetch("{{route('user.delete')}}", {
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                body: JSON.stringify({ 
                    '_token': '{{csrf_token()}}',
                    'id': id
                })
            });
        }).then(res => {
            swal("It was deleted correctly", {
                icon: "success",
            }).then(() => {
                window.location.href = "{{route('users.index')}}"
            })
        })
    })
</script>
@endsection
