@extends('layouts.main')

@section('title', 'Jobs')

@section('keywords', '')

@section('description', '')

@section('bodyClass', '')
@section('content')
	@include('navs.mainNav')
    <div class="row back-row">
        <div class="col s12 center color-row">
            <h3 class="white-text">All Freelancers</h3>
        </div>
    </div>

    <div class="row dashboard-space">
        <div class="col s3">

              <form action="{{ Route('user.filter') }}" method="POST" autocomplete="off">

                {{csrf_field()}}

                <h5 class="grey-text center">Filter by</h5>
                 <br>

                <p>
                  <label for="alphabetical-order">
                    <input class="with-gap blue" name="filter_professional" type="radio" id="alphabetical-order" value="alphabetical-order" @if(isset($active) && $active === 'alphabetical-order') checked="checked" @endif />
                    <span>Alphabetical Order</span>
                  </label>
                </p>
                
                <p>
                  <label for="graduation_date">
                    <input class="with-gap" name="filter_professional" type="radio" id="graduation_date" value="graduation_date" @if(isset($active) && $active === 'graduation_date') checked="checked" @endif/>
                    <span>Graduation Date</span>
                  </label>
                </p>

                <center>
                    <button type="submit" class="blue darken-4 btn btn-tefl">Go filter</button>
                </center>
              </form>
        </div>

        <div class="col s9">
            @foreach($students->chunk(4) as $chunk)
            <div class="row">
                @foreach($chunk as $student)
                   <div class="col s3">
                        <div class="card white card-job">
                            <center>
                                <img src="{{url('/')}}{{$student->path_avatar}}" class="job-profile-img">
                            </center>
                            <p class="purple-text text-darken-4 center title"><b>{{$student->name}}</b></p>
                            <center>
                                <a class="blue darken-4 btn btn-tefl" href="{{ route('public.profile', ['name' => $student->name, 'email' => $student->email]) }}">More Info</a>
                            </center>
                        </div>
                    </div>
                @endforeach
            </div>
            @endforeach
        </div>
    </div>
    @include('partials.footerTwo')
@endsection

@section('head')
<style>
.card .title {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    padding: 0 15px;
}
</style>
@endsection