@extends('layouts.mainv2')

@section('title', 'User Create')

@section('bodyClass', 'grey lighten-5')
@section('content')
    @include('navs.navCore') 
    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav') 
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">Users</b></p>
                </div>

                <div class="col s12">
                    <div class="message-dashboard inter-space">
                        <div class="row">
                            <div class="col s12 m3 l3">
                                <div class="profile-avatar">
                                    <img src="{{url('/')}}/img/data/profile/default.jpg">
                                </div>
                                <p class="grey-text" align="justify">When a new user is registered, an email is sent with the information created, their password and their user email</p>
                            </div>

                            <div class="col s12 m9 l9">
                                <form class="col s12" method="POST" action="{{ route('users.store') }}" autocomplete="off">
                                	{{ csrf_field() }}
                                    <p>Remember all fields are required.</p>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input placeholder="Type the name" id="name" name="name" type="text" class="validate input-delete" value="{{old('name')}}" required>
                                            @if ($errors->has('name'))
                                            <span class="helper-text red-text" data-error="wrong" data-success="right">
                                                {{ $errors->first('name') }}
                                            </span>
                                            @endif
                                        </div>

                                        <div class="input-field col s12">
                                            <input placeholder="Type the email" id="email" name="email" type="email" class="validate input-delete" value="{{old('email')}}"  required>
                                            @if ($errors->has('email'))
                                            <span class="helper-text red-text" data-error="wrong" data-success="right">
                                                {{ $errors->first('email') }}
                                            </span>
                                            @endif
                                        </div>

                                        <div class="input-field col s12">
                                            <select class="icons input-delete" id="role" name="role">
                                                <option value="" disabled {{old('role') ?'':'selected'}}>Choose the level user</option>
                                                @foreach($roles as $role)
                                                <option {{old('role') == $role->id ? 'selected' : ''}} value="{{$role->id}}">{{$role->name}}</option>
												@endforeach
                                            </select>
                                            @if ($errors->has('role'))
                                            <span class="helper-text red-text" data-error="wrong" data-success="right">
                                                {{ $errors->first('role') }}
                                            </span>
                                            @endif
                                        </div>

                                        <div id="password-input" class="input-field col s12" style="{{$errors->has('password') ?'':'display:none'}}">
                                            <input placeholder="Type the password" id="password" name="password" type="text" class="validate input-delete">
                                            @if ($errors->has('password'))
                                            <span class="helper-text red-text" data-error="wrong" data-success="right">
                                                {{ $errors->first('password') }}
                                            </span>
                                            @endif
                                        </div>

                                        <div class="col s12 space-btn-save">
                                            <button type="submit" class="btn btn-delete purple white-text">save</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script>
$(document).on('change', '#role', function(event) {
    let role = $("#role option:selected").text()

    if (role =='graduate')
        $('#password-input').css('display', 'block')
});
</script>
@endsection