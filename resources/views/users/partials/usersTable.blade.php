                              <center>
                                <h4>Users</h4><br>
                              </center>
        <table class="bordered striped centered responsive-table">
            <thead>
                <th>NOMBRE</th>
                <th>EMAIL</th>
                <th>COUNTRY</th>
                <th>OPCIONES</th>
            </thead>
            <tbody id="results">
                @foreach($users as $user)
                    @if($user->id === 1)
                        <tr id="{{$user->id}}">
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            @if(isset($user->country))
                            <td>{{$user->country}}</td>
                            @else
                            <td>Unspecified</td>
                            @endif
                            <td style="min-width: 100px">
                            </td>
                        </tr>
                    @else
                        <tr id="{{$user->id}}">
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            @if(isset($user->country))
                            <td>{{$user->country}}</td>
                            @else
                            <td>Unspecified</td>
                            @endif
                            <td style="min-width: 100px">
                                @can('users.destroy')
                                <button type="button" style="text-decoration: none;" class="btn-flat btn-user-index" data-id="{{$user->id}}" id="deleteBtn" title="Delete" ><i class="fa fa-trash-alt"></i></button>
                                @endcan
                                @can('users.show')
                                <form  action="{{ route('users.notification') }}" method="POST" style="display: inline; margin-left: 10px" id="sendNotification">
                                    <input type="hidden" name="id" value="{{$user->id}}">
                                      {{csrf_field()}}
                                    <button type="submit" style="text-decoration: none;" class="btn-flat btn-user-index" id="notification" title="Send notification"><i class="fa fa-envelope"></i></button>
                                </form>
                                @endcan
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
        <center>{!! $users->links() !!}</center>