<div id="addUser" class="modal modal-fixed-footer">	
	<div class="modal-content">
		<div class="alert alert-danger alert-dismissible print-error-msg" style="display: none;">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <ul>
          </ul>
        </div>
		<form action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data" autocomplete="off" id="addUserForm">
			{{ csrf_field() }}
			<center><br>
				<h4>New User</h4><br>
			</center>
			<div class="row">
				<div class="input-field col s6">
					
					<input type="text" id="name" name="name" class="validate" value="{{old('name')}}">
					<label for="name">Name</label>
				</div>
				<div class="input-field col s6">
					<input type="email" id="email" name="email" class="validate" value="{{old('email')}}">
					<label for="email">Email</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field file-field col s6">
					<select name="role" id="role">
				      <option value="" disabled selected>Choose a role</option>
				      @foreach($roles as $rol)
				      <option value="{{$rol->id}}">{{$rol->name}}</option>
				      @endforeach
				    </select>
		    		<label>System Role</label>
				</div>

				<div class="input-field col s6">
					<label>
			        	<input type="checkbox" class="filled-in" checked="checked" name="notification" />
			        	<span>Send notification to user</span>
			      	</label>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="submit" id="btnSendFormAddUser" class=" waves-effect waves-purple purple darken-3 btn">save</button>
		</div>
	</form>
</div>