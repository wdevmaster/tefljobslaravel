@foreach($works->chunk(3) as $chunk)
	<div class="row"><br><br>
		@foreach($chunk as $work)
		<div class="col s12 m4 l4">
        	<p><b>Company:</b> {{$work->company}}</p>
        	<p><b>Position:</b> {{$work->position}}</p>
        	<p><b>location:</b> {{$work->location}}</p>
        	<p align="justify"><b>Details:</b> {{$work->description}}</p>
        	<button class="btn btn-flat" data-id="{{$work->id}}" id="deleteWork"><i class="fa fa-trash"></i></button>
    	</div>
		@endforeach
	</div>
@endforeach