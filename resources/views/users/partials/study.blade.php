<div id="addStudy" class="modal modal-fixed-footer">
	<div class="modal-content">
		<form action="{{ route('graduate.study') }}" method="POST" id="addStudyGraduate" autocomplete="off">
		{{ csrf_field() }}	
			<center><br>
				<h4>New Study for your CV Digital</h4><br>
			</center>
			<div class="row">
				<div class="input-field col s6">
					
					<input type="text" id="school" name="school" class="validate" value="{{old('school')}}" required>
					<label for="school">School/University - Name</label>
				</div>
				<div class="input-field col s6">
					<input type="text" id="degree" name="degree" class="validate" value="{{old('degree')}}" required>
					<label for="degree">Degree</label>
				</div>
			</div>

            <div class="row">
                <div class="input-field col s6">
                    <input type="date" name="start_date" class="date" value="{{old('start_date')}}" required>
                    <label for="start_date">Start date</label>
                </div>

                <div class="input-field col s6">
                    <input type="date" name="end_date" class="date" value="{{old('end_date')}}" required>
                    <label for="end_date">End date</label>
                </div>
            </div>

	        <div class="row">
	          <div class="input-field col s12">
	            <textarea id="description" name="description" class="materialize-textarea" data-length="120" required></textarea>
	            <label for="description">Description</label>
	          </div>
	        </div>
		</div>
		<div class="modal-footer">
			<button type="submit" id="" class=" waves-effect waves-purple purple darken-3 btn">save</button>
		</div>
		</form>
</div>