<div id="addWork" class="modal modal-fixed-footer">
	<div class="modal-content">
		<form action="{{route('graduate.work')}}" method="POST" id="addWorkGraduate" autocomplete="off">
		{{ csrf_field() }}	
			<center><br>
				<h4>New Work Experience</h4><br>
			</center>
			<div class="row">
				<div class="input-field col s6">		
					<input type="text" id="company" name="company" class="validate" value="{{old('company')}}" required>
					<label for="company">Company Name</label>
				</div>
				<div class="input-field col s6">
					<input type="text" id="position" name="position" class="validate" value="{{old('position')}}" required>
					<label for="position">Position</label>
				</div>
			</div>

            <div class="row">
				<div class="input-field col s4">		
					<input type="text" id="location" name="location" class="validate" value="{{old('location')}}" required>
					<label for="location">Company Location</label>
				</div>
                <div class="input-field col s4">
                    <input type="text" name="start_date" class="datepicker" value="{{old('start_date')}}">
                    <label for="start_date">Start date</label>
                </div>
                <div class="input-field col s4">
                    <input type="text" name="end_date" class="datepicker" value="{{old('end_date')}}">
                    <label for="end_date">End date</label>
                </div>
            </div>

	        <div class="row">
	          <div class="input-field col s12">
	            <textarea id="description" name="description" class="materialize-textarea" data-length="120" required></textarea>
	            <label for="description">Description</label>
	          </div>
	        </div>
		</div>
		<div class="modal-footer">
			<button type="submit" id="" class=" waves-effect waves-purple purple darken-3 btn">save</button>
		</div>
		</form>
</div>