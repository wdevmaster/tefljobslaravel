@foreach($studies->chunk(3) as $chunk)
	<div class="row"><br><br>
		@foreach($chunk as $study)
		<div class="col s12 m4 l4">
        	<p><b>Institute:</b> {{$study->school}}</p>
        	<p><b>Degree:</b> {{$study->degree}}</p>
        	<p><b>Graduated: </b>{{$study->end_date}}</p>
        	<p align="justify"><b>Details:</b> {{$study->description}}</p>
        	<button class="btn btn-flat" data-id="{{$study->id}}" id="deleteStudy"><i class="fa fa-trash"></i></button>
    	</div>
		@endforeach
	</div>
@endforeach