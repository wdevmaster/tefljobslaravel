@extends('layouts.main')

@section('title', 'Edit Profile')

@section('keywords', '')

@section('description', '')

@section('bodyClass', '')
@section('content')
	@include('navs.mainNav')
  <form action="{{ route('graduate.profile.update') }}" method="POST" autocomplete="off" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row dashboard-space">
        <div class="col s12">
            <div class="card white card-space">
                <div class="row">
                  <div class="col s4">
                    <div class="row">
                      <div class="col s12">
                        <div id="imgPreview">
                          <center>
                              <img src="{{url('/')}}{{$user->path_avatar}}" class="profile-img" style="width: 90px;height: 90px;">
                          </center>
                        </div>
                      </div>
                    </div>
                    <div class="row"><br><br>
                        <div class="col s12">
                           <div class="col s2"><br>
                               Name:
                           </div>
                           <div class=" col s8">
                               <input type="text" id="name" name="name" value="{{$user->name}}" required>
                           </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                           <div class="col s2"><br>
                               Email:
                           </div>
                           <div class=" col s8">
                               <input disabled type="text" id="email" name="email" value="{{$user->email}}" required>
                           </div>
                        </div>
                    </div>

                  </div>

                  <div class="col s8 z-depth-3" style="background-color: #f2f2f2; padding: 20px; border-radius: 10px">
                    <h4>Personal Info:</h4>
                      <div class="row">
                        <div class="input-field col s4">
                          <input type="text" id="autocomplete-country" name="country" class="validate" value="{{$user->country}}">
                          <label for="country">Country</label>
                        </div>
                        <div class="input-field col s4">
                          <input type="text" id="autocomplete-state" name="state" class="validate" value="{{$user->state}}">
                          <label for="state">State</label>
                        </div>
                        <div class="input-field col s4">
                          <input type="text" id="autocomplete-city" name="city" class="validate" value="{{$user->city}}">
                          <label for="city">City</label>
                        </div>
                        <div class="input-field col s6">
                          <input type="text" id="address" name="address" class="validate" value="{{$user->address}}">
                          <label for="address">Address</label>
                        </div>
                        <div class="input-field col s6">
                          <input type="text" name="graduation_date" class="datepicker" value="{{$user->graduation_date}}">
                          <label for="graduation_date">Graduation date</label>
                        </div>
                      </div>

                      <div class="row">
                        <div class="input-field col s6">
                          <input type="text" name="birthdate" class="datepicker" value="{{$user->birthdate}}">
                          <label for="birthdate">Birthdate</label>
                        </div>

                        <div class="input-field col s6">
                          <input type="text" id="phone" name="phone" class="validate" value="{{$user->phone}}">
                          <label for="phone">Phone number</label>
                        </div>
                      </div>

                      <div class="row">
                        <div class="file-field input-field col s6">
                          <div class="purple darken-4 btn btn-tefl">
                            <span>Avatar</span>
                            <input type="file" name="avatar" id="avatar">
                          </div>
                          <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                          </div>
                        </div>
                      </div>
                    <div class="row">
                      <div class="input-field col s12">
                        <textarea id="biography" name="biography" class="materialize-textarea">{{$user->biography}}</textarea>
                        <label for="biography">Biography</label>
                      </div>
                    </div>
                  </div> 
                </div>
                <div class="row">
                  <div class="col s12 center"><br>
                  <br>
                  <br>
                    <button type="submit" class="purple darken-4 btn btn-tefl">Save Changes</button>
                  </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    @include('partials.footerTwo')

@endsection

@section('scripts')
  <script>
        //AUTOCOMPLETE LOCATIONS
        $('input#autocomplete-country').autocomplete({
          data: {
            "Argentina": null,
            "Uruguay": null,
            "Paraguay": null,
            "Brasil": null,
            "Chile": null,
            "Ecuador": null,
            "Peru": null,
            "Venezuela": null,
          }
        });

        $(document).ready(function(){
          $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
          });
        });
  </script>

  <script>
    (function(){
      function imgPreview(input){
        if (input.files && input.files[0]){
          var reader = new FileReader();

          reader.onload = function(e){
            $('#imgPreview').empty().html("<center><img src='"+e.target.result+"' style='width: 200px; height:200px;' class='profile-img'/></center>");
          }

          reader.readAsDataURL(input.files[0]);
        }
      }


      $('#avatar').change(function(){
        imgPreview(this);
      });
    })();
  </script>
    <script src="{{ asset('components/ckeditor/ckeditor.js') }}"></script>
@endsection