@extends('layouts.mainv2')

@section('title', 'Users')

@section('bodyClass', 'grey lighten-5')
@section('content')
    @include('navs.navCore') 
    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav') 
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <div class="col s12 m6 l6">
                        <p>Home / <b class="purple-text">Users</b></p>
                    </div>

                    <div class="col s12 m6 l6">
                        <p><a href="{{ route('users.create') }}" class="purple-text right">Add new user</a></p>
                    </div>
                </div>
                @if(Session::has('success'))
                <div class="col s12 message center">
                    <p><i class="material-icons green-text left">check_circle</i>{{Session::get('success')}} </p>
                </div>
                @endif               

                @if(Session::has('error'))
                <div class="col s12 message center">
                    <p><i class="material-icons red-text left">highlight_off</i> {{Session::get('error')}}</p>
                </div>
                @endif
                @foreach($users->chunk(4) as $chunk)
                <div class="row">
                    @foreach($chunk as $user)
                    <div class="col s12 m3 l3">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="row">
                                    <div class="col s10 offset-s1">
                                        <div class="col s12">
                                            <center>
                                                <img src="{{ url('/') }}{{$user->path_avatar}}" style="border-radius: 50%" width="100px" height="100px">
                                            </center>
                                        </div>

                                        <div class="col s12">
                                            <p class="title-offer-car center">{{$user->name}}</p>
                                            <p class="grey-text center"></p>
                                        </div>
                                        <div class="col s12">
                                            <div class="col s12 center">
                                                <a href="{{route('users.edit', $user->id)}}" class="btn btn-info white-text green" @if($user->id === 1) Disabled @endif>Manage</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach
                <center>{{ $users->links() }}</center>
            </div>
        </div>
    </div>

@endsection