@extends('layouts.main')

@section('title', 'Jobs')

@section('keywords', '')

@section('description', '')

@section('head')
<style>
.card .title {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    padding: 0 15px;
}
</style>
@endsection



@section('content')
	@include('navs.mainNav')

    <div class="row back-row">
        <div class="col s12 center color-row">
            <h3 class="white-text">All jobs</h3>
        </div>
    </div>

    <div class="row dashboard-space">
        <div class="col s3">
              <form action="{{ Route('jobs.filter') }}" method="POST" autocomplete="off">

                {{csrf_field()}}

                <h5 class="grey-text center">Filter by</h5>
                 <br>

                <p>
                  <label for="alphabetical-order">
                    <input class="with-gap blue" name="filter_job" type="radio" id="alphabetical-order" value="alphabetical-order" @if(isset($active) && $active === 'alphabetical-order') checked="checked" @endif />
                    <span>Alphabetical Order</span>
                  </label>
                </p>
                <p>
                  <label for="publication-date">
                    <input class="with-gap" name="filter_job" type="radio" id="publication-date" value="publication-date" @if(isset($active) && $active === 'publication-date') checked="checked" @endif/>
                    <span>Publication date</span>
                  </label>
                </p>
                <p>
                  <label for="today">
                    <input class="with-gap" name="filter_job" type="radio" id="today" value="today" @if(isset($active) && $active === 'today') checked="checked" @endif/>
                    <span>Today</span>
                  </label>
                </p>
<!--                 <p>
                  <label for="week">
                    <input class="with-gap" name="filter_job" type="radio" id="week" value="week" @if(isset($active) && $active === 'week') checked="checked" @endif/>
                    <span>This week</span>
                  </label>
                </p> -->                
                <p>
                  <label for="month">
                    <input class="with-gap" name="filter_job" type="radio" id="month" value="month" @if(isset($active) && $active === 'month') checked="checked" @endif/>
                    <span>This month</span>
                  </label>
                </p>

                <center>
                    <button type="submit" class="blue darken-4 btn btn-tefl">Go filter</button>
                </center>
              </form>
        </div>

        <div class="col s9">
            @foreach($jobs->chunk(4) as $chunk)
            <div class="row">
                @foreach($chunk as $job)
                   <div class="col s3">
                        <div class="card white card-job">
                            <center>
                                <img src="{{url('/')}}{{$job->user->path_avatar}}" class="job-profile-img">
                            </center>
                            <p class="purple-text text-darken-4 center title"><b>{{$job->title}}</b></p>
                            <center>
                                <a class="blue darken-4 btn btn-tefl" href="{{ route('jobs.show', ['id' => $job->id, 'slug' => $job->slug]) }}">More Info</a>
                            </center>
                        </div>
                    </div>
                @endforeach
            </div>
            @endforeach
            <br>
            {{ $jobs->links() }}
        </div>
    </div>
    @include('partials.footerTwo')
@endsection