@extends('layouts.main')

@section('title', 'Find at job')

@section('keywords', '')

@section('description', '')

@section('bodyClass', '')
@section('content')
	@include('navs.mainNav')
    <div class="row back-row">
        <div class="col s12 center color-row">
            <h3 class="white-text">Search Result</h3>
        </div>
    </div>

    <div class="row dashboard-space">
        <div class="col s3">
            <h5 class="grey-text center">Filter by</h5>
            <br><br>

            <form action="col s12">
<!--                 <p>
                    <input type="checkbox" id="test5" />
                    <label for="test5">Most recent</label>
                </p>
                <p>
                    <input type="checkbox" id="test5" />
                    <label for="test5">vacant open</label>
                </p> -->
                <p>
                    <input type="checkbox" id="test5" />
                    <label for="test5">In Buenos Aires</label>
                </p>
<!-- 
                <center>
                    <a class="blue darken-4 btn btn-tefl" href="#">Go filter</a>
                </center> -->
            </form>
        </div>

        <div class="col s9">
            @foreach($jobs->chunk(4) as $chunk)
            <div class="row">
                @foreach($chunk as $job)
                   <div class="col s3">
                        <div class="card white card-job" style="min-height: 300px">
                            <center>
                                <img src="{{url('/')}}{{$job->user->path_avatar}}" class="job-profile-img">
                            </center>
                            <p class="purple-text text-darken-4 center title"><b>{{$job->title}}</b></p>
                            <center>
                                <a class="blue darken-4 btn btn-tefl" href="{{ route('jobs.show', ['id' => $job->id, 'slug' => $job->slug]) }}">More Info</a>
                            </center>
                        </div>
                    </div>
                @endforeach
            </div>
            @endforeach
            <br>
            {{ $jobs->links() }}
        </div>
    </div>
    @include('partials.footerTwo')
@endsection

@section('head')
<style>
.card .title {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    padding: 0 15px;
}
</style>