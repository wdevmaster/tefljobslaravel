@extends('layouts.main')

@section('title', 'Details')

@section('keywords', '')

@section('description', '')

@section('bodyClass', '')
@section('content')
	@include('navs.mainNav')
    <div class="row dashboard-space">
        <div class="col s12">
            <div class="col s12">
                <div class="card white card-space">
                    <div class="row">
                        <div class="col s12 m4 l4 center">
                            <center>
                                <img src="{{url('/')}}{{$job->user->path_avatar}}" class="profile-img" style="width: 90px;height: 90px; margin-top:90px;">
                            </center>
                            <h5 class="grey-text center">{{$job->title}}</h5>
                            <a href="{{$job->website}}" target="_blank" class="purple-text text-darken-4"><b>Go site</b></a>

                            <p><i class="material-icons left" style="margin:0px -30px 0px 40px;">email</i> {{$job->user->email}}</p>

                            <p><i class="material-icons left" style="margin:0px -30px 0px 40px;">location_on</i>{{$job->location}}</p>
                            @can('jobs.application')
                            <center>
                                <a class="blue darken-4 btn btn-tefl modal-trigger" href="#submitModal" data-id="{{$job->id}}">submit application</a>
                            </center>
                            @endcan
                        </div>
                        <div class="col s12 m8 l8" align="justify">
                            
                            {!! $job->description !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col s12">
            @role('graduate')
                @include('widgets.lastOffers')
            @endrole            

            @role('employer')
                @include('widgets.employer.applicationsUsers')
            @endrole
        </div>
    </div>
    @include('partials.footerTwo')
    @include('partials.modalAplicattionSubmit')
@endsection

@section('scripts')
<script>
    $('#sendApplycationForm').on('submit', function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = $(this).attr('action');
            $.ajax({
              url: url,
              data: data,
              dataType: 'json',
              type: 'POST',
              beforeSend:function(){
                $('#sendApliccation').attr('disabled', true);
              },
              success:function(data){
                $('#sendApliccation').css('display', 'none');
                $('#cancelApply').css('display', 'none');
                $('#closeApply').css('visibility', 'visible');
                $('#applyHeader').empty().text("Your CV has been send for employer, opting for this vacant");
                console.log(data);
              }  
            });
    });
</script>
@endsection