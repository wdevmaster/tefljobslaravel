@extends('layouts.main')

@section('title', $user->name.' | CV')

@section('keywords', '')

@section('description', '')

@section('bodyClass', '')
@section('content')
	@include('navs.mainNav')
    <div class="row dashboard-space">
        <div class="col s12">
            <div class="col s12">
                <div class="card grey lighten-5 card-space">
                    <div class="row">
                        <div class="col s12 m4 l4">
                            <center>
                                <img src="{{url('/')}}{{$user->path_avatar}}" class="profile-img" style="width: 90px;height: 90px;">
                            </center>
                            <h5 class="grey-text center">{{$user->name}}</h5>

                            @role('employer')
                            @if($isPostulation)
                            <center>
                                <a class="{{ !$review ? 'blue' : 'grey' }} darken-4 btn btn-tefl" 
                                   href="{{ route('review.create', ['id' => $user->id])}}">
                                    {{ !$review ? 'Create' : 'Edit' }} reviews
                                </a>
                            </center>
                            @endif
                            @endrole
                        </div>

                        <div class="col s12 m8 l8">
                            <h4>
                                Ranking 
                                <!--
                                <br>
                                <small>It is not ranked</small>
                                -->
                            </h4>
                            <h6>Accumulated points: {{$user->getRating()}}</h6>
                        </div>
                        
                        <div class="col s12">
                            <h5 class="purple-text text-darken-4 center title-cv">Reviews</h5>
                        </div>
                        @foreach($user->reviews as $review)
                            <div class="col s12 m4 l4">
                                <!--
                                <p><b>Author:</b> {{$review->author->name}}</p>
                                -->
                                <p align="justify">
                                    <b>{{$review->title}}</b><br>
                                    <i>"{{$review->body}}"</i>
                                </p>
                            </div>
                        @endforeach

                        @if($user->reviews->count() == 0)
                        <div class="col s12">
                            <h5 class="purple-text text-darken-4 center title-cv">The user has not obtained opinions</h5>
                        </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.footerTwo')
@endsection