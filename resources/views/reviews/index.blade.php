@extends('layouts.mainv2')

@section('title', 'My feedbacks')

@section('bodyClass', 'grey lighten-5')

@section('content')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">My feedbacks</b></p>
                </div>

                <div class="col s12">
                    @role('employer')
                        @if($users->count() > 0)
                        <a class="modal-trigger" href="#modalFormFeedback">
                            <div class="col s12 m4 l4">
                                <div class="card-offer-add">
                                    <div class="row">
                                        <div class="col s10 offset-s1">
                                            <div class="col s12 center">
                                                <i class="material-icons add-icon grey-text">
                                                    add_circle_outline
                                                </i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        @endif
                    @endrole

                    @if($users->count() == 0 && $reviews->count() == 0)
                    <div class="col s12 message-alert center">
                        @role('employer')
                        <h6>We are sorry at the moment you can not register a feedback.</h6>
                        @endrole
                        @role('graduate')
                        <h6>We are sorry at this moment you have not registered any comments.</h6>
                        @endrole
                    </div>
                    @endif

                    @foreach($reviews as $review)
                    <div class="col s12 m4 l4">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12">
                                        <p class="title-offer-car center">{{$review->title}}</p>
                                        <p align="justify" class="grey-text">
                                            <i>"{{str_limit($review->body, 120)}}"</i>
                                            <!--
                                            <a class="purple-text modal-trigger" href="#modalMoreEmployee">read more</a>
                                            -->
                                        </p>
                                    </div>
                                    <div class="col s12">
                                        <div class="row">
                                            <!--
                                            <div class="col s12 m3 l3">
                                                <img 
                                                    src="{{url('/')}}{{ Auth::user()->isRole('employer') ? $review->reviewable->path_avatar : $review->author->path_avatar}}" 
                                                    class="profile-card-employee"
                                                    style="width: 40px;height: 40px;">
                                            </div>
                                            
                                            <div class="col s12 m7 l7">
                                                <p>
                                                    <b>
                                                        {{ 
                                                            Auth::user()->isRole('employer') 
                                                                ? $review->reviewable->name 
                                                                : $review->author->name
                                                        }}
                                                    </b>
                                                    <br>
                                                    {{ 
                                                        Auth::user()->isRole('employer') 
                                                            ? $review->reviewable->email 
                                                            : $review->author->email
                                                    }}
                                                </p>
                                            </div>
                                            -->
                                            <div class="col s12 center">
                                                <p class="amber-text value">
                                                    @for($i = 0; $i < $review->rating; $i++)
                                                        <i class="fas fa-star"></i>
                                                    @endfor
                                                    @for($j = 0; $j < 5 - $review->rating; $j++)
                                                        <i class="far fa-star"></i>
                                                    @endfor
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>

        </div>
    </div>
@role('employer')
<div id="modalFormFeedback" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            New <b class="purple-text color-title">feedback</b>
        </h5>

        <div class="row">
            {!! Form::open(['route' => 'review.store', 'class' => 'col s12']) !!}
                <p>Remember all fields are required.</p>
                <div class="row">
                    <div class="input-field col s12">
                        <select name="candidate" class="icons input-delete">
                            <option value="" disabled selected>Choose the candidate name</option>
                            @foreach($users as $user)
                            <option {{ old('candidate') == $user->id ? 'selected' : '' }} 
                                value="{{$user->id}}" 
                                data-icon="{{url('/')}}{{$user->path_avatar}}" 
                                class="left circle">
                                    {{$user->name}}
                            </option>
                            @endforeach
                        </select>
                        @if ($errors->has('candidate'))
                            <span class="helper-text red-text" data-error="wrong" data-success="right">
                                {{ $errors->first('candidate') }}
                            </span>
                        @endif
                    </div>

                    <div class="input-field col s12">
                        {{Form::textarea('body', old('body'), [
                            'id' => 'description',
                            'data-length' => 120,
                            'class' => 'materialize-textarea input-delete', 
                            'placeholder' => 'Type details about the feedback',
                        ])}}
                        @if ($errors->has('body'))
                            <span class="helper-text red-text" data-error="wrong" data-success="right">
                                {{ $errors->first('body') }}
                            </span>
                        @endif
                    </div>

                    <div class="input-field col s12">
                        <p class="grey-text">Select a calification 1 to 5</p>
                        <p class="amber-text value">
                            <div id="rating"></div>
                            {{Form::hidden('rating', old('rating'), ['id' => 'input-rating'])}}
                            @if ($errors->has('rating'))
                            <span class="helper-text red-text" data-error="wrong" data-success="right">
                                {{ $errors->first('rating') }}
                            </span>
                            @endif
                        </p>
                        <br><br>
                    </div>

                    <div class="col s12">
                        <button type="submit" class="btn btn-delete purple white-text">
                            Save
                        </button>
                    </div>
                </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>
@endrole
@endsection

@section('scripts')
<script src="{{ asset('js/emojiRatings.js') }}"></script>
<script>
    $('#rating').emojiRating({ 
        onUpdate: function(value) {
            $('#input-rating').val(value)
        }
    });
    @role('employer')
    @if ($errors->any())
        $('#modalFormFeedback')
            .addClass('open')
            .css('z-index', '1005')
            .css('top', '10%')
            .css('opacity', '1')
            .css('transform', 'scaleX(1)')
            .css('display', 'block')
        
            $('body').css('overflow', 'hidden')

            $('body').append('<div class="modal-overlay" style="z-index: 1002; display: block; opacity: 0.5;"></div>')

        $('.modal-close').click(function(e) {
            e.preventDefault()
            modalClose()
        })

        $('.modal-overlay').click(function(e) {
            e.preventDefault()
            modalClose()
        })

        function modalClose () {
            $('#modalFormFeedback')
                .removeClass('open')
                .css('z-index', '1003')
                .css('top', '4%')
                .css('opacity', '0')
                .css('transform', 'scaleX(0.7)')
                .css('display', 'none')

            $('.modal-overlay').remove()

            $('.helper-text').remove()

            $('body').css('overflow', 'initial')
        }

        var starts = $('#input-rating').val()
        $('.jqEmoji').each(function(i){
            if(i < starts)
                $(this).css('opacity', '1')
        })

    @endif
    @endrole
</script>
@endsection
