@extends('layouts.mainv2')

@section('title', 'Employer offer create')

@section('bodyClass', 'grey lighten-5')

@section('content')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">{{!isset($review) ? 'New' : 'Edit'}} review</b></p>
                </div>

                <div class="col s12">
                    <div class="col s12">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s12 m4 l4">
                                    <div class="col s12">
                                        <img src="{{url('/')}}{{$user->path_avatar}}" class="profile-card-employee-two">
                                    </div>

                                    <div class="col s12">
                                        <h5 class="purple-text center">{{$user->name}}</h5>
                                        <p class="center">{{$user->email}}</p>
                                        <p class="center">Location: {{Auth::user()->country}}</p>
                                    </div>
                                </div>

                                <div class="col s12 m8 l8 space-right">
                                    @if(!isset($review))
                                    {!! Form::open(['route' => 'review.store']) !!}
                                    @else
                                    {!! Form::model($review, ['route' => ['review.update', $review->id]]) !!}
                                    @endif
                                        <p>Remember all fields are required.</p>
                                        {{Form::hidden('user_id', $user->id)}}
                                        <div class="input-field col s12">
                                            {{Form::text('title', old('title'), ['class' => 'validate input-delete', 'placeholder' => 'Type the title review'])}}
                                            @if ($errors->has('title'))
                                            <span class="helper-text red-text" data-error="wrong" data-success="right">
                                                {{ $errors->first('title') }}
                                            </span>
                                            @endif
                                        </div>
                                        <div class="input-field col s12">
                                            {{Form::textarea('body', old('body'), ['class' => 'materialize-textarea input-delete ckeditor'])}}
                                            @if ($errors->has('body'))
                                            <span class="helper-text red-text" data-error="wrong" data-success="right">
                                                {{ $errors->first('body') }}
                                            </span>
                                            @endif
                                        </div>
                                        <div class="input-field col s12">
                                            <div id="rating"></div>
                                            {{Form::hidden('rating', null, ['id' => 'input-rating'])}}
                                            @if ($errors->has('rating'))
                                            <span class="helper-text red-text" data-error="wrong" data-success="right">
                                                {{ $errors->first('rating') }}
                                            </span>
                                            @endif
                                        </div>

                                        <div class="input-field col s12">
                                            <button type="submit" class="btn btn-delete purple white-text right">
                                                {{ !isset($review) ? 'Save' : 'Edit' }}
                                            </button>
                                        </div>

                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="{{ asset('components/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('js/emojiRatings.js') }}"></script>
<script>
    $('#rating').emojiRating({ 
        onUpdate: function(value) {
            $('#input-rating').val(value)
        }
    });
    @if(isset($review))
        var starts = '{{$review->rating}}'
        $('.jqEmoji').each(function(i){
            if(i < starts)
                $(this).css('opacity', '1')

        })
    @endif
</script>
@endsection