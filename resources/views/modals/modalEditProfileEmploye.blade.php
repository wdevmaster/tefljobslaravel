<!-- Edit Profile Employer-->
<div id="modalEditProfileEmploye" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            Profile <b class="purple-text color-title">information</b>
        </h5>

        <div class="row">
            <form class="col s12" autocomplete="off" action="{{ Route('employer.profile.update') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <p>Remember all fields are required.</p>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Type your name" id="name" name="name" type="text" class="validate input-delete" value="{{Auth::user()->name}}">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type your email" id="email" name="email" type="email" class="validate input-delete" required value="{{Auth::user()->email}}">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type your address" id="address" name="address" type="text" class="validate input-delete" value="{{Auth::user()->address}}">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type your website" id="website" name="website" type="text" class="validate input-delete" value="{{Auth::user()->website}}">
                    </div>

                    <div class="file-field input-field col s12">
                      <div class="btn purple">
                        <span>File</span>
                        <input type="file" name="avatar">
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                      </div>
                    </div>

                    <div class="col s12"><br>
                        <button type="submit" class="btn btn-delete purple white-text">save</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>