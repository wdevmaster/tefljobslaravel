<div id="modalPassword" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            Change your <b class="purple-text color-title">password</b>
        </h5>

        <div class="row">
            <form class="col s12" autocomplete="off" method="POST" action="{{ route('users.password') }}">
                {{csrf_field()}}
                <p>Remember all fields are required.</p>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Type your old password" id="password" name="current_password" type="password" class="validate input-delete" required>
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type your new password" id="password" name="password" type="password" class="validate input-delete" required>
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Confirm your new password" id="password" name="password_confirmation" type="password" class="validate input-delete" required>
                    </div>

                    <div class="col s12">
                        <button type="submit" class="btn btn-delete purple white-text">save</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>