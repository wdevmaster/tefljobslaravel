<!-- Modal DElETE -->
<div id="modaldelete" class="modal">
    <div class="modal-content">
        <h5 class="title-app center">
            Are you sure <b class="purple-text color-title">you want to delete</b> your account?
        </h5>

        <div class="row">
            <form class="col s12" method="POST" action="{{ route('users.destroy') }}">
                {{ csrf_field() }}
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                <p>If you are sure you want to delete your account, you must send us the reason why you want to delete your account in the following form, and within 24 hours we will unsubscribe your profile.</p>
                <div class="row">
                    <div class="input-field col s12 m6 l6">
                        <input placeholder="Type your motive" id="motive" name="motive" type="text" class="validate input-delete" required>
                    </div>
                    <div class="col s12">
                        <button type="submit" class="btn btn-delete purple white-text">Send</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat">close</a>
    </div>
</div>