<div id="addjob" class="modal modal-fixed-footer">	
	<div class="modal-content">
		<div class="alert alert-danger alert-dismissible print-error-msg" style="display: none;">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <ul>
          </ul>
        </div>
		<form action="{{ route('jobs.store') }}" method="POST" enctype="multipart/form-data" autocomplete="off" id="addJobsForm">
			{{ csrf_field() }}
			<center><br>
				<h4>New offer job</h4><br>
			</center>
			<div class="row">
                <div class="input-field col s6">
                    <input type="email" id="autocomplete-input" name="email" class="autocomplete field-email" 
						value="{{ Auth::user()->isEmployer() ? Auth::user()->email : '' }}"  
						onkeyup="emptySearch();" 
						required>
                    <label for="autocomplete-input">Email user</label>
                </div> 
				<div class="input-field col s6">
					<input type="text" id="title" name="title" class="validate" value="{{old('title')}}" required>
					<label for="title">Title jobs</label>
				</div>
			</div>
			<div class="row">
                <div class="input-field error col s6">
					<input type="text" id="website" name="website" class="validate" value="{{old('website')}}" >
					<label for="website">Website</label>
                </div> 
				<div class="input-field col s6">
					<input type="text" id="autocomplete-location" name="location" class="autocomplete" required>
                    <label for="autocomplete-location">Location</label>
				</div>
			</div>
			<div class="row">
                <div class="input-field col s12">
					<textarea name="description" id="description" class="ckeditor" rows="10">{{old('description')}}</textarea>
                </div> 
			</div>
		</div>
		<div class="modal-footer">
			<div class="switch " style="display: inline; margin-right: 20px">
				<label>
					Draft
					<input type="checkbox" checked="" name="status" id="status">
					<span class="lever"></span>
					Publish
				</label>
			</div>
			<button type="submit" id="btnSendFormAddJobs" class=" waves-effect waves-purple purple darken-3 btn">save</button>
		</div>
	</form>
</div>