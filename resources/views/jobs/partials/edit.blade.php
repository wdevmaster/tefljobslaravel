@extends('layouts.main')

@section('title', 'Details')

@section('keywords', '')

@section('description', '')

@section('bodyClass', '')
@section('content')
	@include('navs.mainNav')
    <form action="{{ route('jobs.update') }}" method="POST" autocomplete="off">
        {{csrf_field()}}
    <input type="hidden" id="id" name="id" value="{{$job->id}}">
    <div class="row dashboard-space">
        <div class="col s12">
            <div class="col s12">
                <div class="card white card-space">
                    <div class="row">
                        <div class="col s12 m4 l4 center">
                            <div class="row">
                                <div class="col s12">
                                   <div class="col s4"><br>
                                       Title Job:
                                   </div>
                                   <div class=" col s8">
                                       <input type="text" id="title" name="title" value="{{$job->title}}" required>
                                   </div>
                                </div>
                            </div><br>

                            <div class="row">
                                <div class="col s12">
                                   <div class="col s4"><br>
                                       Employer Email:
                                   </div>
                                    <div class="input-field col s8">
                                        <input type="email" id="autocomplete-input" name="email" class="autocomplete field-email" value="{{$job->user->email}}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                   <div class="col s4"><br>
                                       Job Location:
                                   </div>
                                    <div class="input-field col s8">
                                        <input type="text" id="autocomplete-location" name="location" class="autocomplete" value="{{$job->location}}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                   <div class="col s4"><br>
                                       Website:
                                   </div>
                                   <div class="col s8">
                                       <input type="text" id="website" name="website" value="{{$job->website}}" required>
                                   </div>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m8 l8" align="justify">
                            
                            <div class="input-field col s12">
                                <textarea name="description" id="description" class="ckeditor" rows="10">{{$job->description}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <center>
            <a  href="{{ route('jobs.index') }}" class="grey darken-4 btn btn-tefl">Cancel</a>
            <input type="submit" value="Update" class="blue darken-4 btn btn-tefl">
        </center>
    </div>
    </form>
    @include('partials.footerTwo')

@endsection

@section('scripts')
<script src="{{ asset('components/ckeditor/ckeditor.js') }}"></script>

    <script>
      $(document).ready(function(){
        //AUTOCOMPLETE USERS
        $('input.autocomplete').autocomplete({
          data: {
            @foreach($users as $user)
            "{{$user->email}}": null,
            @endforeach
          }
        });
        //AUTOCOMPLETE LOCATIONS
        $('input#autocomplete-location').autocomplete({
          data: {
            "Buenos Aires, AR": null,
            "La Plata, Buenos Aires, AR": null,
          }
        });
      });
    </script>
@endsection