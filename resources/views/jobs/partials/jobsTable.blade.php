        @can('jobs.create')
        <br>
        <a href="#addjob" class="waves-effect waves-purple purple darken-3 btn modal-trigger">Publish new job</a><br>
        @endcan
        <br>
        <table class="bordered striped centered responsive-table">
            <thead>
                <th>NOMBRE</th>
                <th>EMAIL</th>
                <th>STATUS</th>
                <th>OPCIONES</th>
            </thead>
            <tbody id="results">
                @foreach($jobs as $job)
                <tr id="{{$job->id}}">
                    <td>{{$job->title}}</td>
                    <td>{{$job->user->email}}</td>
                    <td>@if($job->status === 'open')
                        <button class="btn waves-effect waves-light purple darken-3" type="submit" name="action">Open</button>
                        @endif
                        @if($job->status === 'close')
                        <button class="btn waves-effect waves-light grey" type="submit" name="action">Close</button>
                        @endif
                        @if($job->status === 'draft')
                        <button class="btn waves-effect waves-light blue" type="submit" name="action">Draft</button>
                        @endif
                    </td>
                    <td style="min-width: 300px">
                        @can('jobs.destroy')
                        <button type="button" style="text-decoration: none; margin: 0px" class="btn-flat btn-job-index" data-id="{{$job->id}}" id="deleteBtn" title="Delete" ><i class="fa fa-trash-alt"></i></button>
                        @endcan
                        @can('jobs.show')
                        <form  action="{{ route('jobs.edit') }}" method="POST" style="display: inline; margin-left: 0px">
                            <input type="hidden" name="id" value="{{$job->id}}">
                            {{csrf_field()}}
                            <button type="submit" style="text-decoration: none; margin: 0px" class="btn-flat btn-job-index" title="Edit" ><i class="fa fa-edit"></i></button>
                        </form>
                        @endcan
                        @can('jobs.show')
                        <a href="{{ route('jobs.show', ['id' => $job->id, 'slug' => $job->slug]) }}" target="_blank" style="text-decoration: none; margin: 0px" class="btn-flat btn-job-index" title="View" ><i class="fa fa-eye"></i></a>
                        @endcan

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <center>{!! $jobs->links() !!}</center>