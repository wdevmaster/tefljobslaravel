@extends('layouts.main')

@section('title', 'Jobs')

@section('bodyClass', '')

@section('head')
<!--CSRF-->
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
	@include('navs.mainNav')
  <input type="hidden" name="url" value="{{Route('jobs.index')}}" id="url">
    <div class="row dashboard-space">
        <div class="col {{ !Auth::user()->isEmployer() ? 's8' : 's12' }}">
            <div class="col s12">
                <div class="card grey lighten-5 card-space">
                    <div class="row">
                        <div class="col s12">
							             <div id="table-content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(!Auth::user()->isEmployer())
        <div class="col s4">
            <div class="col s12">
                <div class="card grey lighten-5 card-space">
                    <div class="row">
                        <div class="col s12 center">
                           <h6>Search for user email</h6><br>
                           <form action="{{ route('jobs.search') }}" method="POST" id="searchForm">
                            {{csrf_field()}}
                              <div class="input-field col s12">
                                  <input type="email" id="autocomplete-input" name="email" class="autocomplete" required="" onkeyup="emptySearch();">
                                  <label for="autocomplete-input">Email</label>
                              </div>                              
                              <div class="input-field col s12">
                                  <center>
                                    <button type="submit" id="btnSendFormAddUser" class=" waves-effect waves-purple purple darken-3 btn">SEARCH</button>
                                  </center>
                              </div>
                           </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
    @include('partials.footerTwo')
    @include('jobs.partials.addJobs')
@endsection

@section('scripts')
  

  <!-- ESCRIPT PARA CONFIGURAR LOS HEADERS PARA PETICIONES AJAX -->
    <script>
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    </script>
    <!-- LOAD JOBS SCRIPT -->
    <script>
      window.onload = function(){
        $.get("{{ route('jobs.get') }}", function(data){
          $('#table-content').empty().html(data);
        });
      }
    </script>
    <script>
      $(document).ready(function(){
        //AUTOCOMPLETE USERS
        $('input.autocomplete').autocomplete({
          data: {
            @foreach($users as $user)
            "{{$user->email}}": null,
            @endforeach
          }
        });
        //AUTOCOMPLETE LOCATIONS
        $('input#autocomplete-location').autocomplete({
          data: {
            "Buenos Aires, AR": null,
            "La Plata, Buenos Aires, AR": null,
          }
        });
      });
    </script>

    <!-- SEARCH FOR EMAIL -->
    <script>
      $('#searchForm').on('submit', function(e){
        e.preventDefault();
        var url   = $(this).attr('action');
        var data  = $(this).serialize();
        $.ajax({
          url: url,
          method: "POST",
          data: data,
          type: 'json',
          beforeSend:function(){
              swal("Please wait, searching!");
          },
          success:function(data){
            $('#btnSendFormAddUser').attr('disabled', false);
            $('#table-content').empty().html(data);
            swal.close();
          },
        });
      });
      function emptySearch(){
        var field = $('#autocomplete-input').val();
        if (field === '') {
          $.get("{{ route('jobs.get') }}", function(data){
            $('#table-content').empty().html(data);
          });
        }
      }
    </script>
  <!-- PAGINATION AJAX -->
  <script>
    $(document).on('click', '.pagination a', function(e){
      e.preventDefault();
      var page = $(this).attr('href').split('page=')[1];
      var url = $('#url').val();

      $.ajax({
        url       : url,
        data      : {page:page},
        dataType  : "json",
        type      : "GET",
        success   : function(data){
          $('#table-content').empty().html(data);
        }        
      });

      console.log(page)
    });
  </script>
    <script>
      $(document).ready(function(){
          $('.modal').modal();
          $('select').formSelect();
      });
    </script>

    <!-- STORE SCRIPS -->
    <script>
        $('#addJobsForm').on('submit', function(e){
            e.preventDefault();
            var email  = $('.field-email').val();
            var title  = $('#title').val();
            var website  = $('#website').val();
            var location  = $('#autocomplete-location').val();
            var description = CKEDITOR.instances['description'].getData();
            if ($('#status').is(':checked')) {
              var status = 'open';
            }else{
              var status = 'draft';
            }
            var url   = $(this).attr('action');
            $.ajax({
              url: url,
              data: {
                'email': email,
                'title': title,
                'website': website,
                'location': location,
                'status': status,
                'description': description
              },
              dataType: 'json',
              type: 'POST',
              beforeSend:function(){
                $('#btnSendFormAddJobs').attr('disabled', true);
                swal("Please wait, Saving job!");
              },
              success:function(data){
                if($.isEmptyObject(data.error)){
                  $('#addjob').modal('close');
                    $.get("{{ route('jobs.get') }}", function(data){
                      $('#table-content').empty().html(data);
                      swal("Good job!", "Save succefully!", "success");

                      $('#btnSendFormAddJobs').attr('disabled', false);
                       /*EMPTY FORM FIELDS*/
                      $('#addJobsForm')[0].reset();
                    });
                }else{
                  $('#btnSendFormAddJobs').attr('disabled', false);
                  prinErrors(data.error);
                  swal("uups!", "Error to save!", "error");
                }
                
              }  
            });
        });

    function prinErrors(msg){
        $('.print-error-msg').find('ul').empty();
        $('.print-error-msg').css('display', 'block');

        $.each(msg, function(key, value){
          $('.print-error-msg').find('ul').append("<li>"+ value + "</li>");
        });
    }
    </script>

  <!-- DELETE SCRIPT -->
  <script>
    $(document).on('click', '#deleteBtn', function(e){
        swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this job!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          e.preventDefault();
          var id = $(this).data('id');
          $.post("{{ route('jobs.destroy') }}",{id:id}, function(data){
            $('#results #'+id).remove();
          });
          swal("Poof! This job has been deleted!", {
            icon: "success",
          });
        } else {
          swal("Job not deleted!");
        }
      });
    });
  </script>

  <script src="{{ asset('components/ckeditor/ckeditor.js') }}"></script>

  @if(Session::has('success'))
    <script>
      window.onload = function(){
        swal("Good job!", "Updated job succefully!", "success");
        $.get("{{ route('jobs.get') }}", function(data){
            $('#table-content').empty().html(data);
        });
      }
    </script>
  @endif
  @if(Session::has('error'))
    <script>
      window.onload = function(){
        swal("Upss!", "Dont' update!", "error");
        $.get("{{ route('jobs.get') }}", function(data){
            $('#table-content').empty().html(data);
        });
      }
    </script>
  @endif

@endsection
