<div class="navbar-fixed">
    <nav class="purple darken-4">
        <div class="row row-space-nav">
            <div class="col s12">

                <div class="nav-wrapper">
                    <div class="col s12 m3 l3">
                        <a href="{{ route('home') }}" class="brand-logo">Job Placement APP</a>
                        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                    </div>

                    @include('elements.formSearch')

                    

                    <div class="col s12 m4 l4">
                        <ul class="right hide-on-med-and-down">
                            <li><a href="{{ route('home') }}">Home</a></li>
                            <li><a href="{{ route('graduate.offers') }}">My offers</a></li>
                            <li><a href="{{ route('graduate.profile') }}"><img src="{{url('/')}}{{Auth::user()->path_avatar}}" class="profile-nav" style="width: 40px!important;height: 65px!important;"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>


<!-- responsive menu -->
<ul class="side-nav" id="mobile-demo">
    <li>
        <div class="user-view">
            <div class="background">
                <img src="{{url('/')}}/img/v2/2_large.jpg">
            </div>
            <a href="{{ route('graduate.profile') }}"><img class="circle" src="{{url('/')}}{{Auth::user()->path_avatar}}"></a>
            <a href="{{ route('graduate.profile') }}"><span class="white-text name">{{Auth::user()->name}}</span></a>
            <a href="{{ route('graduate.profile') }}"><span class="white-text email">{{Auth::user()->email}}</span></a>
        </div>
    </li>
    <li><a href="{{ route('home') }}"><i class="material-icons">home</i>Home</a></li>
    <li><a href="{{ route('employer.feedback') }}"><i class="material-icons">work</i>My offers</a></li>
    <li><a href="{{ route('employer.feedback') }}"><i class="material-icons">grade</i>My feedback</a></li>
    <li><a href="{{ route('graduate.profile') }}"><i class="material-icons">account_circle</i>My Profile</a></li>
    <li><a href="{{ route('profile') }}"><i class="material-icons">settings</i>Configurations</a></li>
    <li><a href="{{ route('logout') }}"><i class="material-icons">power_settings_new</i>Sign out</a></li>
</ul>
