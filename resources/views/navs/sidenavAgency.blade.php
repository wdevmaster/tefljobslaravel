<p class="grey-text title-nav"><b>AGENCY</b></p>
<ul>
    <li><a href="{{ route('agency.candidates') }}" class="grey-text text-darken-1"><i class="material-icons left">supervised_user_circle</i> View candidates</a></li>
    <br>
    <li><a href="{{ route('agency.offers') }}" class="grey-text text-darken-1"><i class="material-icons left">book</i> View offers</a></li>
    <br>
    <li><a href="{{route('profile')}}" class="grey-text text-darken-1"><i class="material-icons left">settings</i> Configurations</a></li>
    <br>
    <li><a href="{{route('logout')}}" class="grey-text text-darken-1"><i class="material-icons left">power_settings_new</i>Sign out</a></li>
</ul>