@role('admin')
    @include('navs.navAdmin')
@endrole
@role('employer')
    @include('navs.navEmploye')
@endrole 
@role('graduate')
    @include('navs.navGraduate')
@endrole 
@role('supervisor')
   @include('navs.navAgency') 
@endrole