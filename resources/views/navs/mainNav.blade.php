<nav class="grey lighten-5 fixed">
    <div class="row">
        <div class="col s12 nav-space">
            <div class="nav-wrapper">
                <div class="row">
                    <div class="col s1">
                        <img src="{{url('/')}}/img/logo/logo.png" class="log-app-nav right">
                    </div>
                    <div class="col s2">
                        <a href="{{ route('home') }}" class="brand-logo grey-text text-darken-3">Job Placement APP</a>
                    </div>
                    <div class="col s4">
                        <form class="col s12" method="POST" action="{{ Route('jobs.searchForm') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix grey-text text-darken-3">search</i>
                                    <input id="icon_prefix" type="text" placeholder="Search job offers, profiles and more ..." class="validate input-search" name="search">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col s5">
                        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                        <ul class="right hide-on-med-and-down">
                            <li><a href="{{ route('home') }}" class="grey-text text-darken-3"><i class="material-icons">home</i></a></li>
                            @can('jobs.index')
                                <li><a href="{{ route('jobs.index.graduate') }}" class="grey-text text-darken-3"><i class="material-icons left">work_outline</i> Offers</a></li>
                            @endcan
                            @can('graduates.index')
                                <li><a href="{{ route('freelancers.index') }}" class="grey-text text-darken-3"><i class="material-icons left">supervised_user_circle</i> Professionals</a></li>
                            @endcan
                            <li><a href="{{ route('profile') }}" class="grey-text text-darken-3"><i class="material-icons left">account_circle</i> Profile</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>




