
<p class="grey-text title-nav"><b>ADMIN</b></p>
<ul>
    <li><a href="{{ route('users.index') }}" class="grey-text text-darken-1"><i class="material-icons left">supervised_user_circle</i> View all users</a></li>
    <br>
    <li><a href="{{ route('profile') }}" class="grey-text text-darken-1"><i class="material-icons left">settings</i> Configurations</a></li>
    <br>
    <li><a href="{{route('logout')}}" class="grey-text text-darken-1"><i class="material-icons left">power_settings_new</i>Sign out</a></li>
</ul>