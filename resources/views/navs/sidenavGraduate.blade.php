<p class="grey-text title-nav"><b>GRADUATE</b></p>
<ul>
    <li><a href="{{ route('graduate.offers.index') }}" class="grey-text text-darken-1"><i class="material-icons left">view_carousel</i> View offers</a></li>
    <br>
    <li><a href="{{ route('graduate.offers') }}" class="grey-text text-darken-1"><i class="material-icons left">book</i> My offers</a></li>
    <br>
    <li><a href="{{ route('employer.feedback') }}" class="grey-text text-darken-1"><i class="material-icons left">grade</i> My feedback</a></li>
    <br>
    <li><a href="{{ route('graduate.profile') }}" class="grey-text text-darken-1"><i class="material-icons left">account_circle</i> My Profile</a></li>
    <br>
    <li><a href="{{ route('profile') }}" class="grey-text text-darken-1"><i class="material-icons left">settings</i> Configurations</a></li>
    <br>
    <li><a href="{{ route('logout') }}" class="grey-text text-darken-1"><i class="material-icons left">power_settings_new</i>Sign out</a></li>
</ul>