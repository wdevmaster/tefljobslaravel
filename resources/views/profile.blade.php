@extends('layouts.mainv2')

@section('title', 'Welcome to our Job Placement App!')

@section('bodyClass', 'grey lighten-5')
@section('content')
    @include('navs.navCore')

    
    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">Configurations Profile</b></p>
                </div>

                <div class="col s12">
                    <div class="card card-profile">
                        <div class="row">
                            <div class="col s12 m3 l3">
                                <div class="col s12">
                                    <div class="profile-avatar" style="{{ Auth::user()->path_avatar ? 'background: none; background-color: white;' : '' }}">
                                        <img src="{{url('/')}}{{Auth::user()->path_avatar}}">
                                    </div>
                                </div>
                            </div>

                            <div class="col s12 m3 l3 specia-profile-conf-space">
                                <h5 class="title">{{Auth::user()->name}}</h5>
                                <p class="grey-text sub-title">YOUR PERSONAL INFORMATION</p>
                                <p class="content">{{Auth::user()->email}}</p>
                            </div>
                            @role('employe')
                            <div class="col s12 m5 l5 specia-profile-conf-space">
                                <div class="row">
                                    <div class="col s10 offset-s1">
                                        @if(isset(Auth::user()->website))<p class="content">{{Auth::user()->website}}</p>@endif
                                        @if(isset(Auth::user()->address))<p class="content">Country: {{Auth::user()->address}}</p>@endif
                                    </div>
                                </div>
                            </div>
                            @endrole
                            @role('graduate')
                            <div class="col s12 m5 l5 specia-profile-conf-space">
                                <div class="row">
                                    <div class="col s10 offset-s1">
                                        <div class="col s12 m3 l3">
                                            <img src="{{url('/')}}/img/v2/favourites.png" class="icon-dashboard">
                                        </div>

                                        <div class="col s12 m9 l9">
                                            <p class="grey-text title-extra">YOUR FEEDBACK SCORING</p>
                                            <p class="center">{{ Auth::user()->getRating() }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endrole
                            <div class="col s12 m5 l5 specia-profile-conf-space">
                                <div class="row">

                                </div>
                            </div>

                            <div class="col s12 m1 l1">
                                <!-- Modal Trigger -->
                                <a class="modal-trigger right black-text" href="#modalR" style="position: absolute;top: 25px;right: 30px;">
                                    <i class="material-icons">help_outline</i>
                                </a>

                                <!-- Modal Structure -->
                                <div id="modalR" class="modal">
                                    <div class="modal-content">
                                        <h5 class="title-app">
                                            Recommendations for <b class="purple-text color-title">your Account</b>
                                        </h5>

                                        <p align="justify">Select a front photo for your profile.</p>
                                        <p align="justify">Place a password that you remember easily and comply with our security standards, since our policies against unauthorized access may imply penalties.</p>
                                        <p align="justify">If you have any questions in your operation you can contact us by sending an email to <b class="purple-text">buenosairestefl@gmail.com</b></p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="#!" class="modal-action modal-close btn-flat">close</a>
                                    </div>
                                </div>
                            </div>
                            @if(Session::has('message'))
                            <div class="col s12 message center">
                                 <p><i class="material-icons red-text left">highlight_off</i> {{ Session::get('message') }}</p>
                            </div>
                            @endif                           
                             @if(Session::has('success'))
                            <div class="col s12 message center">
                                <p><i class="material-icons green-text left">check_circle</i>{{Session::get('success')}} </p>
                            </div>
                            @endif
                            @if(Session::has('error'))
                            <div class="col s12 message center">
                                <p><i class="material-icons red-text left">highlight_off</i> {{Session::get('error')}}</p>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                @role('admin')
                <div class="col s12 m3 l3">
                    <a class="modal-trigger" href="#modalPassword">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12">
                                        <img src="{{url('/')}}/img/v2/key.png" class="icon-config">
                                    </div>

                                    <div class="col s12">
                                        <p class="grey-text center">CHANGE PASSWORD</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                
                <div class="col s12 m3 l3">
                    <a class="modal-trigger" href="#modalEditProfileAdmin">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12">
                                        <img src="{{url('/')}}/img/v2/user.png" class="icon-config">
                                    </div>

                                    <div class="col s12">
                                        <p class="grey-text center">EDIT YOUR PROFILE</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endrole

                @role('employer')
                <div class="col s12 m3 l3">
                    <a class="modal-trigger" href="#modalEditProfileEmploye">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12">
                                        <img src="{{url('/')}}/img/v2/user.png" class="icon-config">
                                    </div>

                                    <div class="col s12">
                                        <p class="grey-text center">EDIT YOUR PROFILE</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endrole

                @role('graduate')
                <div class="col s12 m3 l3">
                    <a class="modal-trigger" href="#modalEditProfileGraduate">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12">
                                        <img src="{{url('/')}}/img/v2/user.png" class="icon-config">
                                    </div>

                                    <div class="col s12">
                                        <p class="grey-text center">EDIT YOUR PROFILE</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endrole
                @role('supervisor')
                <div class="col s12 m3 l3">
                    <a class="modal-trigger" href="#modalEditProfileEmploye">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12">
                                        <img src="{{url('/')}}/img/v2/user.png" class="icon-config">
                                    </div>

                                    <div class="col s12">
                                        <p class="grey-text center">EDIT YOUR PROFILE</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endrole
                @if(Auth::user()->id != 1)
                    <div class="col s12 m3 l3">
                        <a class="modal-trigger" href="#modaldelete">
                            <div class="message-dashboard">
                                <div class="row">
                                    <div class="col s10 offset-s1">
                                        <div class="col s12">
                                            <img src="{{url('/')}}/img/v2/trash.png" class="icon-config">
                                        </div>

                                        <div class="col s12">
                                            <p class="grey-text center">DELETE YOUR ACCOUNT</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endif

                <div class="col s12 m3 l3">
                    <a class="modal-trigger" href="#modalhelp">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12">
                                        <img src="{{url('/')}}/img/v2/help.png" class="icon-config">
                                    </div>

                                    <div class="col s12">
                                        <p class="grey-text center">DO YOU NEED HELP?</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col s12">
                    <p align="justify" class="alert-message"><b>IMPORTANT:</b> The JOB PLACEMENT APP platform protects all sensitive data and personal information, we do not request information from credit cards or bank accounts, so our processes are exclusive to Buenos Aires TEFL, if at any time you receive a request of this type of data, ignore and communicate immediately to the email <b class="purple-text">buenosairestefl@gmail.com.</b></p>
                </div>
            </div>
        </div>
    </div>   

     <!--MODAL HELP-->
     <div id="modalMore" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            Your are Amazing!
        </h5>

        <div class="row">
            <div class="col s12">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus finibus non velit non volutpat. Quisque sed nulla ut eros convallis dapibus a nec quam. Nullam finibus pharetra neque, ut venenatis metus sollicitudin nec. Donec non mauris lorem. Mauris tempus ante nisi. Ut sed magna vitae lorem luctus vehicula sit amet a diam. In dignissim placerat risus vitae malesuada. Nullam sit amet convallis sem. Curabitur nisi odio, mollis ac tellus sed, luctus bibendum velit. Nunc lacinia consectetur nisi, vitae lobortis libero cursus et. Phasellus in nisl ligula.</p>

                <p>Vivamus placerat nunc id quam bibendum placerat. Sed id consectetur lorem. Suspendisse potenti. Donec convallis neque mattis sodales semper. Morbi facilisis purus lacus, eget porta tortor fermentum id. Vestibulum interdum ligula sapien, at tempus augue efficitur a. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer vestibulum ligula in libero accumsan, id bibendum orci eleifend. Donec vitae pulvinar ante. Morbi lacus risus, rutrum sit amet vestibulum eget, viverra non eros.</p>
            </div>

            <div class="col s12">
                <div class="row">
                    <div class="col s12 m1 l1">
                        <img src="{{url('/')}}/img/v2/descarga.jpeg" class="profile-card-employee">
                    </div>

                    <div class="col s12 m7 l7">
                        <p>
                            <b>English Services</b><br>
                            rrhh@englishservices.com.ar</p>
                    </div>

                    <div class="col s4 center">
                        <p class="amber-text value">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star-half-alt"></i>
                            <i class="far fa-star"></i>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>

<div id="modalMoreEmployee" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            Your are Amazing!
        </h5>

        <div class="row">
            <div class="col s12">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus finibus non velit non volutpat. Quisque sed nulla ut eros convallis dapibus a nec quam. Nullam finibus pharetra neque, ut venenatis metus sollicitudin nec. Donec non mauris lorem. Mauris tempus ante nisi. Ut sed magna vitae lorem luctus vehicula sit amet a diam. In dignissim placerat risus vitae malesuada. Nullam sit amet convallis sem. Curabitur nisi odio, mollis ac tellus sed, luctus bibendum velit. Nunc lacinia consectetur nisi, vitae lobortis libero cursus et. Phasellus in nisl ligula.</p>

                <p>Vivamus placerat nunc id quam bibendum placerat. Sed id consectetur lorem. Suspendisse potenti. Donec convallis neque mattis sodales semper. Morbi facilisis purus lacus, eget porta tortor fermentum id. Vestibulum interdum ligula sapien, at tempus augue efficitur a. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer vestibulum ligula in libero accumsan, id bibendum orci eleifend. Donec vitae pulvinar ante. Morbi lacus risus, rutrum sit amet vestibulum eget, viverra non eros.</p>
            </div>

            <div class="col s12">
                <div class="row">
                    <div class="col s12 m1 l1">
                        <img src="{{url('/')}}/img/v2/persona1.jpg" class="profile-card-employee">
                    </div>

                    <div class="col s12 m7 l7">
                        <p>
                            <b>Emilia Fong</b><br>
                            emiliafong@gmail.com</p>
                    </div>

                    <div class="col s4 center">
                        <p class="amber-text value">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star-half-alt"></i>
                            <i class="far fa-star"></i>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>

<div id="modal3" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            Personal <b class="purple-text color-title">information</b>
        </h5>

        <div class="row">
            <form class="col s12">
                <p>Remember all fields are required.</p>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Type your full name" id="name" type="text" class="validate input-delete">
                    </div>

                    <div class="input-field col s12">
                        <textarea placeholder="Type your personal description" id="description" class="materialize-textarea input-delete" data-length="120"></textarea>
                    </div>

                    <div class="col s12">
                        <a class="btn btn-delete purple white-text" href="dashboard.php">save</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>


<div id="modalFormFeedback" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            New <b class="purple-text color-title">feedback</b>
        </h5>

        <div class="row">
            <form class="col s12">
                <p>Remember all fields are required.</p>
                <div class="row">
                    <div class="input-field col s12">
                        <select class="icons input-delete">
                            <option value="" disabled selected>Choose the candidate name</option>
                            <option value="" data-icon="img/person_1.jpg" class="left circle">Emilia Fong</option>
                            <option value="" data-icon="img/persona1.jpg" class="left circle">Frank Villa</option>
                            <option value="" data-icon="img/persona2.jpg" class="left circle">Eduardo Dizeo</option>
                        </select>
                    </div>

                    <div class="input-field col s12">
                        <textarea placeholder="Type details about the feedback" id="description" class="materialize-textarea input-delete" data-length="120"></textarea>
                    </div>

                    <div class="input-field col s12">
                        <p class="grey-text">Select a calification 1 to 5</p>
                        <p class="amber-text value">
                            <i class="far fa-star"></i>
                            <i class="far fa-star"></i>
                            <i class="far fa-star"></i>
                            <i class="far fa-star"></i>
                            <i class="far fa-star"></i>
                        </p>
                        <br><br>
                    </div>

                    <div class="col s12">
                        <a class="btn btn-delete purple white-text" href="dashboard.php">save</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>


<!-- CV -->
<div id="modal4" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            Personal <b class="purple-text color-title">information</b>
        </h5>

        <div class="row">
            <form class="col s12">
                <p>Remember all fields are required.</p>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Type your full name" id="name" type="text" class="validate input-delete">
                    </div>

                    <div class="input-field col s12">
                        <textarea placeholder="Type your personal description" id="description" class="materialize-textarea input-delete" data-length="120"></textarea>
                    </div>

                    <div class="col s12">
                        <a class="btn btn-delete purple white-text" href="dashboard.php">save</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>

<!-- ADDRESS -->
<div id="modalAddress" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            Contact <b class="purple-text color-title">information</b>
        </h5>

        <div class="row">
            <form class="col s12" autocomplete="off">
                <p>Remember all fields are required.</p>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Type your address" id="address" type="text" class="validate input-delete">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type your Phone numer or your whatsapp" id="text" type="number" class="validate input-delete">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type your Emails" id="text" type="email" class="validate input-delete">
                    </div>

                    <div class="col s12">
                        <a class="btn btn-delete purple white-text" href="dashboard.php">save</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>

<!-- Edit Profile -->
<div id="modalEditProfile" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            Profile <b class="purple-text color-title">information</b>
        </h5>

        <div class="row">
            <form class="col s12" autocomplete="off">
                <p>Remember all fields are required.</p>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Type your name" id="address" type="text" class="validate input-delete">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type your email" id="text" type="email" class="validate input-delete">
                    </div>

                    <div class="file-field input-field col s12">
                        <div class="btn purple">
                            <span>Picture</span>
                            <input type="file">
                        </div>
                        <div class="file-path-wrapper">
                            <input placeholder="Add your pic profile" class="validate input-delete" type="text">
                        </div>
                    </div>

                    <div class="col s12">
                        <a class="btn btn-delete purple white-text" href="dashboard.php">save</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>

@include('modals.editPersonalInfoAdmin')
@include('modals.modalEditProfileEmploye')
@include('modals.modalEditProfileGraduate')

@include('modals.deleteUser')

<!-- Modal help -->
<div id="modalhelp" class="modal">
    <div class="modal-content">
        <h5 class="title-app">
            You need <b class="purple-text color-title">help</b>?
        </h5>

        <div class="row">
            <div class="col s12">
                <p>If you need help to use this platform, please send an email to <b>buenosairestefl@gmail.com</b> and we respond all your questions soon.</p>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat">close</a>
    </div>
</div>

<!-- PASSWORD -->
    @include('modals.changePassword')

<!-- Logation and lenaguaje -->
<div id="modalLanguaje" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            Nationality <b class="purple-text color-title">information</b>
        </h5>

        <div class="row">
            <form class="col s12" autocomplete="off">
                <p>Remember all fields are required.</p>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Type your nationality" id="address" type="text" class="validate input-delete">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type your native languaje" id="text" type="text" class="validate input-delete">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type your brithdate" id="text" type="text" class="validate input-delete datepicker">
                    </div>

                    <div class="col s12">
                        <a class="btn btn-delete purple white-text" href="dashboard.php">save</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>

<!-- Logation and References -->
<div id="modalReferences" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            References <b class="purple-text color-title">information</b>
        </h5>

        <div class="row">
            <form class="col s12" autocomplete="off">
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Type the full name" id="namereference" type="text" class="validate input-delete">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type the email" id="text" type="text" class="validate input-delete">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type the phone number" id="text" type="text" class="validate input-delete">
                    </div>

                    <div class="col s12">
                        <a class="btn btn-delete purple white-text" href="dashboard.php">save</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>

<!-- Education -->
<div id="modalEducation" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            Education <b class="purple-text color-title">information</b>
        </h5>

        <div class="row">
            <form class="col s12" autocomplete="off">
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Type the date when you finish your education level" id="education" type="text" class="validate input-delete">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type the name of the site" id="text" type="text" class="validate input-delete">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type a short detail" id="text" type="text" class="validate input-delete">
                    </div>

                    <div class="col s12">
                        <a class="btn btn-delete purple white-text" href="dashboard.php">save</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>

<!-- work -->
<div id="modalWork" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            Work <b class="purple-text color-title">information</b>
        </h5>

        <div class="row">
            <form class="col s12" autocomplete="off">
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Type the last year in your work" id="education" type="text" class="validate input-delete">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type the position or role" id="text" type="text" class="validate input-delete">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type the location" id="text" type="text" class="validate input-delete">
                    </div>

                    <div class="col s12">
                        <a class="btn btn-delete purple white-text" href="dashboard.php">save</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>

@endsection