@extends('layouts.main')

@section('title', 'Digital')

@section('keywords', '')

@section('description', '')
@section('head')
<!--CSRF-->
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('bodyClass', '')
@section('content')
	@include('navs.mainNav')
    <div class="row dashboard-space">
        <div class="col s12">
            <div class="col s12">
                <div class="card grey lighten-5 card-space">
                    <div class="row">
                        <div class="col s12 m4 l4">
                            <center>
                                <img src="{{url('/')}}{{Auth::user()->path_avatar}}" class="profile-img" style="width: 90px;height: 90px;">
                            </center>
                            <h5 class="grey-text center">{{Auth::user()->name}}</h5>
                            <center>
                                <a href="{{ route('graduate.profile.edit') }}" class="purple darken-4 btn btn-tefl">edit personal information</a>
                            </center>
                            @role('graduate')
                                @if(Auth::user()->path_cv != null)
                                <br>
                                    <center>
                                        <a class="purple darken-4 btn btn-tefl" href="{{ url('/') }}/{{ Auth::user()->path_cv }}">View your resume</a>
                                        <a href="{{ route('cv.delete') }}" class="btn-flat" style="color: #000; margin-left: 10px; text-decoration: text-decoration: none;" title="Delete resume"><i class="fa fa-trash"></i></a>
                                    </center>
                                @else
                                    <center><br>
                                        <a class="purple darken-4 btn btn-tefl modal-trigger" href="#attachCV" ">attach resume in Word/PDF</a>
                                    </center>
                                @endif
                            @endrole
                        </div>

                        <div class="col s12 m8 l8">
                            <p><b>Address:</b> @if(isset($user->address)) {{$user->address}}, @endif @if(isset($user->city)) {{$user->city}}, @endif @if(isset($user->state)){{$user->state}} @endif - {{$user->country}}</p>
                            <p><b>Email:</b> {{$user->email}}</p>
                            <p style="display: inline"><b>Biography:</b> {!! $user->biography !!}</p>
                        </div>
                        
                        <div class="col s12"><br><br>
                            <center>
                                <h5 class="purple-text text-darken-4 center title-cv" style="display: inline; margin-right: 20px">Education</h5>
                                @can('freelancer.study')
                                    <a class="purple darken-4 btn btn-tefl modal-trigger" href="#addStudy">Add Study</a>
                                @endcan
                            </center>
                        </div>
                        <div class="row" id="studyContent" style="padding-left: 20px">
                        </div>
                        
                        <div class="col s12"><br><br>
                            <center><h5 class="purple-text text-darken-4 center title-cv " style="display: inline; margin-right: 20px">Work Experience</h5>
                                @can('freelancer.experience')
                                    <a class="purple darken-4 btn btn-tefl modal-trigger" href="#addWork">Add Work</a>
                                @endcan
                            </center>
                        </div>
                        <div class="row" id="workContent" style="padding-left: 20px">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.footerTwo')
    @include('users.partials.study')
    @include('users.partials.work')

  <!-- Modal Structure -->
  <div id="attachCV" class="modal">
    <div class="modal-content">
      <center><h5>Attach your resume in PDF or Word file</h5></center>
    
    <form action="{{ Route('cv.upload') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
        {{csrf_field()}}
        <div class="row"><br><br>
                <div class="file-field input-field col s8 offset-s2">
                  <div class="purple darken-4 btn btn-tefl ">
                    <span class="">File</span>
                    <input type="file" name="cv_file" >
                  </div>
                  <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                  </div>
                </div>
        </div>                    
        @if ($errors->has('cv_file'))
        <div class="row">
            <div class="col s12 center">
                <span class="help-block">
                    <strong>{{ $errors->first('cv_file') }}</strong>
                </span>
            </div>
        </div>
        @endif    
    </div>
    <div class="modal-footer">
      <button type="submit" class="purple darken-4 btn btn-tefl">Publish</button>
    </div>
    </form>
  </div>

@endsection
@section('scripts')
    <script>
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    </script>

    <script>
      $(document).ready(function(){
          $('.modal').modal();
          $('input#input_text, textarea#description').characterCounter();
      });
    </script>
    <script>
          $(document).ready(function(){
            $('.datepicker').datepicker({
                container: 'body',
                format: 'yyyy-mm-dd',
            });
          });
    </script>
    
    <!-- LOAD DATA STUDY -->
    <script>
        window.onload = function(){
            $.get("{{ route('graduate.get.studies') }}", function(data){
                $('#studyContent').empty().html(data);
            });

            $.get("{{ route('graduate.get.works') }}", function(data){
                $('#workContent').empty().html(data);
            });
        }
    </script>

    <!-- SAVE STUDY -->
    <script>
        $('#addStudyGraduate').on('submit', function(e){
            e.preventDefault();
            var url = $(this).attr('action');
            var data = $(this).serialize();

            $.ajax({
                url: url,
                data: data,
                type: 'post',
                dataType: 'json',
                success:function(data){
                    $.get("{{ route('graduate.get.studies') }}", function(data){
                        $('#studyContent').empty().html(data);
                    });
                    $('#addStudy').modal('close');
                    swal("Good job!", "Successfully study saved!", "success");
                    /*EMPTY FORM FIELDS*/
                    $('#addStudyGraduate')[0].reset();
                },
            });
        });
    </script>

    <!-- DELETE STUDY SCRIPT -->
    <script>
        $(document).on('click', '#deleteStudy', function(e){
            swal({
            title: "Are you sure?",
            text: "You will delete this study from your CV Digital!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              e.preventDefault();
              var id = $(this).data('id');
              $.post("{{ route('graduate.delete.studies') }}",{id:id}, function(data){
                    $.get("{{ route('graduate.get.studies') }}", function(data){
                        $('#studyContent').empty().html(data);
                    });
              });
              swal("Poof! This study has been deleted!", {
                icon: "success",
              });
            } else {
              swal("Study not deleted!");
            }
          });
        });
    </script>

    <!-- DELETE WORK SCRIPT -->
    <script>
        $(document).on('click', '#deleteWork', function(e){
            swal({
            title: "Are you sure?",
            text: "You will delete this work experience from your CV Digital!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              e.preventDefault();
              var id = $(this).data('id');
              $.post("{{ route('graduate.delete.works') }}",{id:id}, function(data){
                    $.get("{{ route('graduate.get.works') }}", function(data){
                        $('#workContent').empty().html(data);
                    });
              });
              swal("Poof! This work experience has been deleted!", {
                icon: "success",
              });
            } else {
              swal("Work Experience not deleted!");
            }
          });
        });
    </script>

    <!-- SAVE WORK -->
    <script>
        $('#addWorkGraduate').on('submit', function(e){
            e.preventDefault();
            var url = $(this).attr('action');
            var data = $(this).serialize();

            $.ajax({
                url: url,
                data: data,
                type: 'post',
                dataType: 'json',
                success:function(data){
                    $.get("{{ route('graduate.get.works') }}", function(data){
                        $('#workContent').empty().html(data);
                    });
                    $('#addWork').modal('close');
                    swal("Good job!", "Successfully study saved!", "success");
                    /*EMPTY FORM FIELDS*/
                    $('#addWorkGraduate')[0].reset();
                },
            });
        });
    </script>
@endsection