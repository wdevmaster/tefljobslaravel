<div class="col s12 m5 l5 hide-on-med-and-down">
    <form class="col s12" action="{{route('offers.searching')}}" method="POST" autocomplete="off">
        <div class="row">
            <div class="input-field col s12">
                <i class="material-icons prefix white-text">search</i>
                <input placeholder="Search offers, profiles and more.." name="search" type="text" class="input-searh">
            </div>
        </div>
    </form>
</div>