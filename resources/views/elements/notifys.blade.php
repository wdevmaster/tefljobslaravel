<script src="{{asset('js/sweetalert.min.js', env('APP_SSL', false))}}"></script>
@if (notify()->ready())
    <script>
        swal({
            title: "{!! notify()->message() !!}",
            text: "{!! notify()->option('text') !!}",
            icon: "{{ notify()->type() }}",
        });
    </script>
@endif