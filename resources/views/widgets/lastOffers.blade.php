            <br><br>
            <h5 class="purple-text text-darken-4 center">Recently published offers</h5>
            <br><br>
            @foreach($lastOffer as $job)
            <a href="details.php">
                <div class="col s12 m3 l3">
                    <div class="card white card-job">
                        <div class="row">
                            <div class="col s12">
                                <center>
                                    <img src="{{url('/')}}{{$job->user->path_avatar}}" class="job-profile-img">
                                </center>

                                <p class="purple-text text-darken-4 center"><b>{{$job->title}}</b></p>
                                <center>
                                    <a class="blue darken-4 btn btn-tefl" href="{{ route('jobs.show', ['id' => $job->id, 'slug' => $job->slug]) }}">More Info</a>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            @endforeach
        </div>