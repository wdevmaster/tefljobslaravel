<br><br>
   <h5 class="purple-text text-darken-4 center">Users Applications!</h5>
   <br><br>
   @foreach($postulations as $postulation)
   <a href="">
       <div class="col s12 m3 l3">
           <div class="card white card-job">
               <div class="row">
                   <div class="col s12">
                       <center>
                           <img src="{{url('/')}}{{$postulation->user->path_avatar}}" class="job-profile-img">
                       </center>

                       <p class="purple-text text-darken-4 center"><b>{{$postulation->user->name}}</b></p>
                       <center>
                           <a class="blue darken-4 btn btn-tefl" href="{{url('/')}}/dashboard/user/{{$postulation->user->name}}/email/{{$postulation->user->email}}/profile">Review CV</a>
                       </center>
                   </div>
               </div>
           </div>
       </div>
   </a>
   @endforeach
</div>