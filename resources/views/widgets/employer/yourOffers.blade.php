<br><br>
    <h5 class="purple-text text-darken-4 center">
        @role('employer')
            Your Published offer jobs
        @endrole
        @role('admin')
            Published jobs
        @endrole
    </h5>
    <br><br>
    @foreach($yourOffer as $job)
    <a href="{{ route('jobs.show', ['id' => $job->id, 'slug' => $job->slug]) }}">
        <div class="col s12 m3 l3">
            <div class="card white card-job">
                <div class="row">
                    <div class="col s12">
                            <center>
                                <img src="{{url('/')}}{{$job->user->path_avatar}}" class="job-profile-img">
                            </center>

                            <p class="purple-text text-darken-4 center title"><b>{{$job->title}}</b></p>

                            <div class="{{$job->postulations->count() == 0 ? 'grey-text' : 'blue-text'}} ico-postulan">
                                <span>{{$job->postulations->count()}}</span>
                                <i class="material-icons">supervised_user_circle</i>
                            </div>

                            <center>
                                <a class="blue darken-4 btn btn-tefl" href="{{ route('jobs.show', ['id' => $job->id, 'slug' => $job->slug]) }}">More Info</a>
                            </center>
                    </div>
                </div>
            </div>
        </div>
    </a>
    @endforeach
</div>