@extends('layouts.mainv2')

@section('title', 'View candidates')

@section('bodyClass', 'grey lighten-5')

@section('content')
    @role('employer')
        @include('navs.navCore')
    @endrole    
    @role('supervisor')
        @include('navs.navCore')
    @endrole

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @role('employer')
                    @include('navs.sideNav')
                @endrole
                @role('supervisor')
                    @include('navs.sideNav')
                @endrole
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">Candidates</b></p>
                </div>

                <div class="col s12">
                    <p>You will find all registered TEFL graduates on this screen</p>
                </div>
                <div class="row">
                    
                </div>
                <div class="col s12">
                    @foreach($users->chunk(3) as $chunk)
                    <div class="row">
                        @foreach($chunk as $user)
                        <div class="col s12 m4 l4">
                            <div class="message-dashboard">
                                <div class="row">
                                    <div class="col s10 offset-s1">
                                        <div class="col s12">
                                            <img src="{{url('/')}}{{$user->path_avatar}}" class="candidate-profile" style="width: 80px;height: 80px;">
                                        </div>
                                        
                                        <div class="col s12">
                                            <p class="title-offer-car center">{{$user->name}}</p>
                                            <p class="grey-text center">
                                                @if ($user->getRating() != 0)
                                                @for($i = 0; $i < $user->points(); $i++)
                                                    <i class="yellow-text text-darken-1 material-icons">star</i>
                                                @endfor
                                                @for($j = 0; $j < 5 - $user->points(); $j++)
                                                    <i class="grey-text material-icons">star_border</i>
                                                @endfor
                                                @else
                                                    @for($j = 0; $j < 5; $j++)
                                                        <i class="grey-text material-icons">star_border</i>
                                                    @endfor
                                                @endif
                                            </p>
                                        </div>
                                        <div class="col s12">
                                            <div class="col s12 center">
                                                <a href="{{route('public.profile', $user->email)}}" class="btn btn-info white-text green">More Info</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
 <script src="{{ asset('components/ckeditor/ckeditor.js') }}"></script>
@endsection