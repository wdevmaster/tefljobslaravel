@extends('layouts.mainv2')

@section('title', 'Employer offer Edit')

@section('bodyClass', 'grey lighten-5')

@section('content')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">Edit offer</b></p>
                </div>

                <div class="col s12">
                    <div class="col s12">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s12 m4 l4">
                                    <div class="col s12">
                                        <img src="{{url('/')}}{{$job->user->path_avatar}}" class="profile-card-employee-two">
                                    </div>

                                    <div class="col s12">
                                        <h5 class="purple-text center">{{$job->user->name}}</h5>
                                        <p class="center">{{$job->user->email}}</p>
                                        <p class="center"><a href="https://www.englishservices.com.ar/" class="center grey-text" target="_blank">{{$job->user->website}}</a></p>
                                        @if(isset($job->user->address))<p class="center">Location: {{$job->user->address}}</p>@endif
                                    </div>
                                </div>

                                <div class="col s12 m8 l8 space-right">
                                    <form class="col s12" method="POST" action="{{ route('offer.update') }}" autocomplete="off">
                                    	<input type="hidden" name="_method" value="PUT">
                                    	<input type="hidden" name="offer_id" value="{{$job->id}}">
                                    	{{ csrf_field() }}
                                        <p>Remember all fields are required.</p>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input placeholder="Type the title offer" id="title" name="title" type="text" class="validate input-delete" required value="{{$job->title}}">
                                                @if ($errors->has('title'))
				                                    <div class="col s12 message center">
				                                        <p><i class="material-icons red-text left">highlight_off</i> {{ $errors->first('title') }}</p>
				                                    </div>
				                                @endif
                                            </div>

                                            <div class="input-field col s12">
                                            	<label for="description">Description </label><br>
                                                <textarea placeholder="Type the offer details" id="description-input-textarea" class="materialize-textarea input-delete ckeditor" data-length="300" name="description" required>{{$job->description}}</textarea>
                                                @if ($errors->has('description'))
				                                    <div class="col s12 message center">
				                                        <p><i class="material-icons red-text left">highlight_off</i> {{ $errors->first('description') }}</p>
				                                    </div>
				                                @endif
                                            </div>                                            
                                            <div class="input-field col s12">

											  <button type="submit" class="btn btn-delete purple white-text right">update</button>
                                            </div>

                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
 <script src="{{ asset('components/ckeditor/ckeditor.js') }}"></script>
@endsection