@extends('layouts.mainv2')

@section('title', 'Employer offer | '.$job->title)

@section('bodyClass', 'grey lighten-5')

@section('content')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">Offer details</b></p>
                </div>

                <div class="col s12">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s12 m4 l4">
                                <div class="col s12">
                                    <img src="{{url('/')}}{{$job->user->path_avatar}}" class="profile-card-employee-two">
                                </div>

                                <div class="col s12">
                                    <h5 class="purple-text center">{{$job->user->name}}</h5>
                                    <p class="center">{{$job->user->email}}</p>
                                    <p class="center"><a href="https://www.englishservices.com.ar/" class="center grey-text" target="_blank">{{$job->user->website}}</a></p>
                                    @if(isset($job->user->address))<p class="center">Location: {{$job->user->address}}</p>@endif
                                </div>

                                <div class="col s12 m6 l6">
                                    <a href="{{ route('offer.edit', ['id' => $job->id, 'slug' => $job->slug]) }}" class="transparent btn btn-edit"><i class="material-icons">edit</i></a>
                                </div>

                                <div class="col s12 m6 l6">
                                    <form action="{{ route('offer.delete') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="offer_id" value="{{$job->id}}">
                                        <button class="transparent btn btn-edit" type="submit"><i class="material-icons">delete</i></button>
                                    </form>
                                </div>
                            </div>

                            <div class="col s12 m8 l8 space-right resume">
                            	<h4>{{$job->title}}</h4>
								{!! $job->description !!}
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col s12">
                    <h5 class="grey-text">Candidates who applied</h5>
                </div>
                
                <div class="col s12">
                @foreach($job->postulations->chunk(3) as $chunk)
                    <div class="row">
                        @foreach($chunk as $postulation)
                        <div class="col s12 m4 l4">
                            <div class="message-dashboard">
                                <div class="row">
                                    <div class="col s10 offset-s1">
                                        <div class="col s12">
                                            <img src="{{url('/')}}{{$postulation->user->path_avatar}}" class="candidate-profile" style="width: 80px;height: 80px;">
                                        </div>

                                        <div class="col s12">
                                            <p class="title-offer-car center">{{$postulation->user->name}}</p>
                                            <p class="grey-text center">
                                                @if ($postulation->user->getRating() != 0)
                                                    @for($i = 0; $i < $postulation->user->points(); $i++)
                                                        <i class="yellow-text text-darken-1 material-icons">star</i>
                                                    @endfor
                                                    @for($j = 0; $j < 5 - $postulation->user->points(); $j++)
                                                        <i class="grey-text material-icons">star_border</i>
                                                    @endfor
                                                @else
                                                    @for($j = 0; $j < 5; $j++)
                                                        <i class="grey-text material-icons">star_border</i>
                                                    @endfor
                                                @endif
                                            </p>
                                        </div>
                                        <div class="col s12">
                                            <div class="col s12 center">
                                                <a href="{{route('candidate.resume', ['email' => $postulation->user->email, 'postulation_id' => $postulation->id ])}}" class="btn btn-info white-text green">More Info</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                @endforeach
                
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
