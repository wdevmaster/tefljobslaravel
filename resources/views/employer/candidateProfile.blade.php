@extends('layouts.mainv2')

@section('title', 'Public Profile')

@section('bodyClass', 'grey lighten-5')
@section('content')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>
            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">Profile</b></p>
                </div>
                
                <div class="col s12">
                    <div class="message-alert">
                        <div class="row">
                            <div class="col s12" style="padding-right: 2.5rem;">
                                <h5 class="amber-text text-darken-4">Important</h5>
                                <p class="admin-text" align="justify">this applicant has applied to the offer with the title: <b>{{$postulation->job->title}}</b> published on <b>               <?php
                                    $date= $postulation->job->created_at;
                                    $sqldate=date('d-m-Y',strtotime($date));
                                    echo $sqldate;
                                ?></b>, if you want to see this offer please <a href="{{ route('offer.show', ['id' => $postulation->job->id, 'slug' => $postulation->job->slug]) }}">click here</a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12">
                    <div class="card card-profile">
                        <div class="row">
                            <div class="col s12 m4 l4">
                                <div class="col s12">
                                    <div class="profile-avatar" style="{{ $postulation->user->path_avatar ? 'background: none; background-color: white;' : '' }}">
                                        <img src="{{url('/')}}{{$postulation->user->path_avatar}}">
                                    </div>
                                </div>
                                @if($postulation->status === 'review')
                                <div class="col s12">
                                    <form action="{{route('candidate.acepted')}}" method="POST">
                                        {{csrf_field()}}
                                        <input type="hidden" name="postulation_id" value="{{$postulation->id}}">
                                        <input type="hidden" name="candidate_id" value="{{$postulation->user->id}}">
                                        <input type="hidden" name="employer" value="{{Auth::user()->id}}">
                                        <button type="submit" class="green white-text btn btn-edit">Accept Profile</button>
                                    </form>
                                </div>
                                @endif

                            </div>

                            <div class="col s12 m7 l7">
                                <h5 class="title">{{$postulation->user->name}}</h5>
                                <p class="grey-text sub-title">YOUR PERSONAL DESCRIPTION</p>
                                <p class="content">{{$postulation->user->biography}}</p>

                                <div class="row">
                                    @if(isset($postulation->user->path_cv))
                                    <div class="col s12 m6 l6">
                                        <a href="{{url('/')}}{{$postulation->user->path_cv}}" target="_blank" class="btn amber darken-3 account-btn">Download digital CV</a>
                                    </div>
                                    @endif
                                     
                                    @if(isset($postulation->user->video_presentation))                   
                                    <div class="col s12 m6 l6">
                                        <a class="modal-trigger btn red darken-3 account-btn" href="#modalV">Presentation video</a>

                                        <!-- Modal Structure -->
                                        <div id="modalV" class="modal">
                                            <div class="modal-content">
                                                <h5 class="title-app">
                                                    Presentation <b class="purple-text color-title">Video</b>
                                                </h5>

                                                <div class="row">
                                                    <div class="col s10 offset-s1">
                                                        <div class="video-container">
                                                            <video class="video" frameborder="0" allowfullscreen controlslist="nodownload" controls>
                                                                <source src="{{url('/')}}{{$postulation->user->video_presentation}}" autostart="false"> Your browser does not support the video tag.
                                                            </video>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer modal-footer-video">
                                                <a href="#!" class="modal-action modal-close btn-flat">close</a>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>

                            <div class="col s12 m1 l1">
                                <!-- Modal Trigger -->
                                <a class="modal-trigger right black-text" href="#modal1"><i class="material-icons">help_outline</i></a>

                                <!-- Modal Structure -->
                                <div id="modal1" class="modal">
                                    <div class="modal-content">
                                        <h5 class="title-app">
                                            Recommendations for <b class="purple-text color-title">Accept one Profile</b>
                                        </h5>

                                        <p align="justify">Once the applicant is accepted, their contact information will be sent to them, in order to continue in their respective entry process according to their standards.</p>
                                        <p align="justify">If the applicant turns out not to be approved, he / she must publish the vacancy again so that another of our graduates can apply.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="#!" class="modal-action modal-close btn-flat">close</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col s12 m6 l6">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s10 offset-s1">
                                <div class="col s12 m3 l3">
                                    <img src="{{url('/')}}/img/v2/profile.png" class="icon-dashboard">
                                </div>

                                <div class="col s12 m9 l9">
                                    <p><b class="grey-text">ADDRESS:</b> {{$postulation->user->address}}</p>
                                    <p><b class="grey-text">PHONE / WHATSAPP:</b> {{$postulation->user->phone}}</p>
                                    <p><b class="grey-text">E-MAIL:</b> {{$postulation->user->email}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12 m6 l6">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s10 offset-s1">
                                <div class="col s12 m3 l3">
                                    <img src="{{url('/')}}/img/v2/maps-and-flags.png" class="icon-dashboard">
                                </div>

                                <div class="col s12 m9 l9"> 
                                    <p><b class="grey-text">NATIONALITY:</b> {{$postulation->user->country}}</p>
                                    <p><b class="grey-text">LANGUAGE:</b> {{$postulation->user->lenguage}}</p>
                                    <p><b class="grey-text">BRITHDAY:</b> {{$postulation->user->birthdate}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12">
                    <div class="col s12">
                        <h5 class="grey-text text-lighten-1">REFERENCES</h5>
                    </div>
                    @foreach($postulation->user->references as $reference)
                    <div class="col s12 m4 l4">
                        <div class="card-references">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12">
                                        <b class="purple-text text-darken-4">{{$reference->name}}</b>
                                        <p class="grey-text">Email: {{$reference->email}}</p>
                                        <p class="grey-text">Phone: {{$reference->phone}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>

                <div class="col s12 m6 l6">
                    <div class="col s12">
                        <h5 class="grey-text text-lighten-1">EDUCATION</h5>
                    </div>
                    @foreach($postulation->user->studies as $estudy)
                    <div class="col s12">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12 m3 l3">
                                        <img src="{{url('/')}}/img/v2/mortarboard.png" class="icon-dashboard">
                                    </div>

                                    <div class="col s12 m9 l9">
                                        <b class="purple-text text-darken-4">({{ $estudy->end_date}})</b>
                                        <p class="grey-text">{{ $estudy->school}}</p>
                                        <p>{{ $estudy->description}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

                <div class="col s12 m6 l6">
                    <div class="col s12">
                        <h5 class="grey-text text-lighten-1">WORK EXPERIENCE</h5>
                    </div>
                    @foreach($postulation->user->experiences as $work)
                    <div class="col s12">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12 m3 l3">
                                        <img src="{{url('/')}}/img/v2/briefcase.png" class="icon-dashboard">
                                    </div>

                                    <div class="col s12 m9 l9">
                                        <b class="purple-text text-darken-4">({{$work->date}})</b>
                                        <p class="grey-text">{{$work->date}}</p>
                                        <p>In {{$work->location}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

                <div class="row">
                    <div class="col s12">
                        <div class="col s12">
                            <h5 class="grey-text text-lighten-1">
                                REVIEWS ({{$postulation->user->reviews->count()}})
                            </h5>
                        </div>
                        @foreach($postulation->user->reviews as $review)
                        <div class="col s12">
                            <div class="card-references">
                                <div class="row">
                                    <div class="col s12">
                                        <div class="col s12">
                                            <div class="col {{ $review->author_id == Auth::id() ? 's11' : 's12' }} grey-text">
                                                <span class="right">
                                                    @for($i = 0; $i < $review->rating; $i++)
                                                        <i class="yellow-text text-darken-1 material-icons">star</i>
                                                    @endfor
                                                    @for($j = 0; $j < 5 - $review->rating; $j++)
                                                        <i class="grey-text material-icons">star_border</i>
                                                    @endfor
                                                </span>
                                                <b>{{$review->title}}</b>
                                                <p class="grey-text">
                                                    <i>"{{$review->body}}"</i>
                                                </p>
                                                <!--
                                                <b class="purple-text text-darken-4">
                                                    {{$review->author->name}}
                                                </b>
                                                -->
                                            </div>
                                            @if($review->author_id == Auth::id())
                                            <div class="col s1">
                                                <span class="right" style="margin-left: 10px;">
                                                    <a  class="btn-floating purple white-text"
                                                        href="{{route('review.create', $postulation->user->id)}}">
                                                        <i class="material-icons">mode_edit</i>
                                                    </a>
                                                </span>
                                            </div>   
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

            </div>

        </div>
    </div>   
@endsection