@extends('layouts.mainv2')

@section('title', 'Employer offers')

@section('bodyClass', 'grey lighten-5')

@section('content')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>

			<div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">My offers</b></p>
                </div>

                <div class="col s12">
					@if(!$show)
					<a href="{{ route('employer.offers.create') }}">
                        <div class="col s12 m4 l4">
                            <div class="card-offer-add">
                                <div class="row">
                                    <div class="col s10 offset-s1">
                                        <div class="col s12 center">
                                            <i class="material-icons add-icon grey-text">
                                                add_circle_outline
                                            </i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
					@endif
					@foreach($employerJobs as $job)
					<div class="col s12 m4 l4">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12" style="min-height: 200px;">
                                        <p class="title-offer-car center">{{$job->title}}</p>
                                        <p align="justify" class="grey-text">
											{{ str_limit(strip_tags($job->description), 200) }}
										</p>
                                    </div>
                                    <div class="col s12">
                                        <div class="row">
                                            <div class="col s12 m3 l3">
                                                <img src="{{url('/')}}{{$job->user->path_avatar}}" 
													class="profile-card-employee" 
													style="width: 40px;height: 40px;">
                                            </div>

                                            <div class="col s12 m7 l7" style="min-height: 78px;">
                                                <p>
													<b>{{$job->user->name}}</b><br>
			                                        {{$job->user->email}}
												</p>
                                            </div>

                                            <div class="col s12 center">
												@if($job->status === 'open')
												<a href="{{ route('offer.show', ['id' => $job->id, 'slug' => $job->slug]) }}" 
													class="btn btn-info white-text green">Open</a>
												@else
												<button type="button" class="btn btn-info white-text grey disabled">Close</button>
												@endif
                                            </div>
                                        </div>
                                    </div>
									@if($job->status === 'open')
                                    <div class="col s12 space-candidates-offer">
                                        <div class="col s4">
											@foreach($job->postulations as $key => $postulation)
												@if($key < 3)
                                            		<img src="{{url('/')}}{{$postulation->user->path_avatar}}" 
														class="candidates-img" 
														style="width: 30px;height: 30px;">
												@endif
											@endforeach
                                        </div>
                                        <div class="col s1">
                                            <p class="count-candidates">
												{{ 
													$job->postulations->count() < 5 
														? $job->postulations->count() 
														: '+5' 
												}}
											</p>
                                        </div>

                                        <div class="col s6 space-top-view">
											<a href="{{ route('offer.candidates.show', ['id' => $job->id, 'slug' => $job->slug]) }}" 
												class="purple-text all-text right">View all candidates</a>
                                        </div>
                                    </div>
									@else
									<div class="col s12" style="min-height: 52px;">
										<form action="{{ route('employer.offers.destroy') }}" method="POST" id="deleteJob">
			                                		{{csrf_field()}}
			                                <input type="hidden" name="job_id" value="{{$job->id}}">
			                                <button type="submit" 
												class="btn-flat right" 
												onclick="asegurar()">
													<i class="material-icons grey-text">delete</i>
											</button>
			                            </form>
									</div>
									@endif
                                </div>
                            </div>
                        </div>
                    </div>
					@endforeach
                </div>

				<center>{{$employerJobs->links()}}</center>
            </div>
        </div>
    </div>
@endsection
