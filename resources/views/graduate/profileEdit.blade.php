@extends('layouts.mainv2')

@section('title', 'Graduate | Profile Edit')
@section('head')
<!--CSRF-->
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('bodyClass', 'grey lighten-5')

@section('content')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>
        <!--CONTENT PROFILE  -->
            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">Profile</b></p>
                </div>

                <div class="col s12">
                    <div class="card card-profile">
                        <div class="row">
                            <!-- MENSAJES -->
                            @if(Session::has('success'))
                            <div class="col s12 message center">
                                <p><i class="material-icons green-text left">check_circle</i>{{Session::get('success')}} </p>
                            </div>
                            @endif
                            @if(Session::has('error'))
                            <div class="col s12 message center">
                                <p><i class="material-icons red-text left">highlight_off</i> {{Session::get('error')}}</p>
                            </div>
                            @endif
                            @if ($errors->has('biography'))
                                <div class="col s12 message center">
                                    <p><i class="material-icons red-text left">highlight_off</i> {{ $errors->first('biography') }}</p>
                                </div>
                            @endif
                            <!-- FIN DE MENSAJES -->
                            <div class="col s12 m4 l4">
                                <div class="col s12">
                                    <div class="profile-avatar" style="{{ Auth::user()->path_avatar ? 'background: none; background-color: white;' : '' }}">
                                        <img src="{{url('/')}}{{Auth::user()->path_avatar}}">
                                    </div>
                                </div>
                                <div class="col s12">
                                    <a href="{{route('graduate.profile')}}" class="transparent btn btn-edit"><i class="material-icons left">edit</i>Close edit</a>
                                </div>
                            </div>

                            <div class="col s12 m7 l7">
                                <h5 class="title">
                                    {{Auth::user()->name}}
                                    <a class="modal-trigger" href="#biographyEdit">
                                        <i class="material-icons left grey-text">edit</i>
                                    </a>
                                </h5>
                                <p class="grey-text sub-title">YOUR PERSONAL DESCRIPTION</p>
                                <p class="content resume">
                                    {{Auth::user()->biography}}
                                </p>

                                <div class="row">
                                    <div class="col s12 m6 l6">
                                        @if(empty(Auth::user()->path_cv))
                                            <a class="modal-trigger btn amber darken-3 account-btn" href="#uploadCV">Upload digital CV</a>
                                        @else
                                            <a class="modal-trigger btn amber darken-3 account-btn" href="#uploadCV">Change digital CV</a>
                                        @endif
                                    </div>

                                    <div class="col s12 m6 l6">
                                        @if(empty(Auth::user()->video_presentation))
                                            <a class="modal-trigger btn red darken-3 account-btn" href="#uploadVideo">Upload video presentation</a>
                                        @else
                                            <a class="modal-trigger btn red darken-3 account-btn" href="#uploadVideo">Change video presentation</a>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col s12 m1 l1">
                                <!-- Modal Trigger -->
                                <a class="modal-trigger right black-text" href="#modal1"><i class="material-icons">help_outline</i></a>

                                
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col s12 m6 l6">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s12">
                                <a class="modal-trigger" href="#modalAddress">
                                    <i class="material-icons right grey-text">edit</i>
                                </a>
                            </div>

                            <div class="col s10 offset-s1 edit-body">
                                <div class="col s12 m3 l3">
                                    <img src="{{url('/')}}/img/v2/profile.png" class="icon-dashboard">
                                </div>

                                <div class="col s12 m9 l9">
                                    <p><b class="grey-text">ADDRESS:</b> {{Auth::user()->address}}</p>
                                    <p><b class="grey-text">PHONE / WHATSAPP:</b> {{Auth::user()->phone}}</p>
                                    <p><b class="grey-text">E-MAIL:</b> {{Auth::user()->email}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12 m6 l6">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s12">
                                <a class="modal-trigger" href="#modalLanguaje">
                                    <i class="material-icons right grey-text">edit</i>
                                </a>
                            </div>

                            <div class="col s10 offset-s1 edit-body">
                                <div class="col s12 m3 l3">
                                    <img src="{{url('/')}}/img/v2/maps-and-flags.png" class="icon-dashboard">
                                </div>

                                <div class="col s12 m9 l9">
                                    <p><b class="grey-text">NATIONALITY:</b> {{Auth::user()->country}}</p>
                                    <p><b class="grey-text">LANGUAGE:</b> {{Auth::user()->lenguage}}</p>
                                    <p><b class="grey-text">BRITHDAY:</b> {{Auth::user()->birthdate}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12">
                    <div class="col s12">
                        <h5 class="grey-text text-lighten-1">REFERENCES</h5>
                    </div>
                    @foreach($references as $key => $reference)
                    @if($key < 3)
                    <div class="col s12 m4 l4">
                        <div class="card-references">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s10">
                                        <b class="purple-text text-darken-4">{{$reference->name}}</b>
                                        <p class="grey-text">Email: {{$reference->email}}</p>
                                        <p class="grey-text">phone: {{$reference->phone}}</p>
                                    </div>

                                    <div class="col s1">
                                        <form action="{{route('graduate.reference.delete')}}" method="POST" >
                                            {{csrf_field()}}
                                            <input type="hidden" name="id" value="{{$reference->id}}">
                                            <button type="submit" class="btn-flat">
                                                <i class="material-icons right grey-text ">delete</i>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                    @if(count($references) < 3) 
                    <a class="modal-trigger" href="#modalReferences">
                        <div class="col s12 m4 l4">
                            <div class="card-references-add">
                                <i class="material-icons add-icon grey-text">
                                    add_circle_outline
                                </i>
                            </div>
                        </div>
                    </a>
                    @endif
                </div>

                <div class="col s12 m6 l6">
                    <div class="col s12">
                        <h5 class="grey-text text-lighten-1">EDUCATION</h5>
                    </div>
                    
                    @foreach($studies as $study)
                    <div class="col s12">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12 m3 l3">
                                        <img src="{{url('/')}}/img/v2/mortarboard.png" class="icon-dashboard">
                                    </div>

                                    <div class="col s12 m7 l7">
                                        <b class="purple-text text-darken-4">({{$study->end_date}})</b>
                                        <p class="grey-text">{{$study->school}}</p>
                                        <p>{{$study->description}}</p>
                                    </div>
                                    <div class="col s1">
                                       <form action="{{route('graduate.education.delete')}}" method="POST" >
                                            {{csrf_field()}}
                                            <input type="hidden" name="id" value="{{$study->id}}">
                                            <button type="submit" class="btn-flat">
                                                <i class="material-icons right grey-text ">delete</i>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                    <a class="modal-trigger" href="#modalEducation">
                        <div class="col s12">
                            <div class="message-dashboard-add">
                                <div class="row">
                                    <div class="col s10 offset-s1">
                                        <i class="material-icons add-icon-2 grey-text">
                                            add_circle_outline
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col s12 m6 l6">
                    <div class="col s12">
                        <h5 class="grey-text text-lighten-1">WORK EXPERIENCE</h5>
                    </div>
                    @foreach($experiences as $exp)
                    <div class="col s12">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12 m3 l3">
                                        <img src="{{url('/')}}/img/v2/briefcase.png" class="icon-dashboard">
                                    </div>

                                    <div class="col s12 m7 l7">
                                        <b class="purple-text text-darken-4">({{$exp->date}})</b>
                                        <p class="grey-text">{{$exp->description}}</p>
                                        <p>{{$exp->location}}</p>
                                    </div>

                                    <div class="col s1">
                                       <form action="{{route('graduate.work.delete')}}" method="POST" >
                                            {{csrf_field()}}
                                            <input type="hidden" name="id" value="{{$exp->id}}">
                                            <button type="submit" class="btn-flat">
                                                <i class="material-icons right grey-text ">delete</i>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach


                    <a class="modal-trigger" href="#modalWork">
                        <div class="col s12">
                            <div class="message-dashboard-add">
                                <div class="row">
                                    <div class="col s10 offset-s1">
                                        <i class="material-icons add-icon-2 grey-text">
                                            add_circle_outline
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        <!--ENDCONTENT PROFILE  -->
        </div>
    </div>
    @include('graduate.modals.uploadCV')
    @include('graduate.modals.uploadVideo')
    @include('graduate.modals.biographyEdit')
    @include('graduate.modals.addressEdit')
    @include('graduate.modals.lenguageEdit')
    @include('graduate.modals.recomendations')
    @include('graduate.modals.referencesAdd')
    @include('graduate.modals.educationAdd')
    @include('graduate.modals.workAdd')
@endsection
@section('scripts')
    <script>
       $(document).ready(function(){
          $('#birthdateAF').datepicker({
            format: 'yyyy-mm-dd'
          });
        });

    </script>
@endsection