@extends('layouts.mainv2')

@section('title', 'Offer Details | '.$job->title)
@section('head')
<!--CSRF-->
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('bodyClass', 'grey lighten-5')

@section('content')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">Offer details</b></p>
                </div>

                <div class="col s12">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s12 m4 l4">
                                <div class="col s12">
                                    <img src="{{url('/')}}{{$job->user->path_avatar}}" class="profile-card-employee-two">
                                </div>

                                <div class="col s12">
                                    <h5 class="purple-text center">{{$job->user->name}}</h5>
                                    <p class="center">{{$job->user->email}}</p>
                                    <p class="center"><a href="https://www.englishservices.com.ar/" class="center grey-text" target="_blank">{{$job->user->website}}</a></p>
                                    @if(isset($job->user->address))<p class="center">Location: {{$job->user->address}}</p>@endif
                                    @role('supervisor')
                                        <p class="center">
                                            <b>Candidates</b>: {{$job->postulations->count()}}
                                        </p>
                                    @endrole
                                </div>
								@if($validateAplication === 'true')
                                <div class="col s12 center">
                                    <a href="" class="btn btn-info white-text green" disabled>You have applied</a>
                                </div>
                                @endif
								@if($validateAplication === 'false')
                                <div class="col s12 center" id="bntApply">
                                	<form action="{{ route('offer.apply') }}" method="POST" id="formApply">
                                		{{ csrf_field() }}
                                		<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                		<input type="hidden" name="job_id" value="{{$job->id}}">
                                        @role('graduate')
                                    	<button type="submit" class="btn btn-info white-text green" id="sendApply" data-id="{{$job->id}}">Submit Application</button>
                                        @endrole 
                                    </form>
                                </div>
                                <div id="loading" style="display: none;">
                                	<center>
                                		<img src="{{url('/')}}/img/v2/loading.gif">
                                		<center>
                                			<h5>Please wait</h5>
                                		</center>
                                	</center>
                                </div>
                                @endif
                            </div>

                            <div class="col s12 m8 l8 space-right resume">
                            	<h4>{{$job->title}}</h4>
								{!! $job->description !!}
                            </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    </script>
	<script>
		$('#formApply').on('submit', function(e){
			e.preventDefault();
			var url = $(this).attr('action');
			var data = $(this).serialize();
			$.ajax({
				url: url,
				type: 'POST',
				data: data,
				dataType: 'json',
		        beforeSend:function(){
		           $('#bntApply').css('display', 'none');
		           $('#loading').css('display', 'block');
		        },
				success:function(data){
					$('#loading').css('display', 'none');
					$('#sendApply').attr('disabled', true);
					$('#sendApply').text('You have applied');
					$('#bntApply').css('display', 'block');
				}   
			});
		});
	</script>
@endsection
