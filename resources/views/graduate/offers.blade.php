@extends('layouts.mainv2')

@section('title', 'My Offers')
@section('head')
<!--CSRF-->
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('bodyClass', 'grey lighten-5')

@section('content')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>
            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">My offers</b></p>
                </div>

                <div class="col s12">
                    <form class="col s12" autocomplete="off">
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="text" type="text" class="validate input-search" placeholder="Type the name employee">
                            </div>
                        </div>
                    </form>
                </div>
                @foreach($postulations->chunk(3) as $chunk)
                <div class="row">
                    @foreach($chunk as $postulation)
                    <div class="col s12 m4 l4">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12">
                                        <p class="title-offer-car center">{{$postulation->job->title}}</p>
                                        <p align="justify" class="grey-text">{!!str_limit($postulation->job->description, 200)!!}</p>
                                    </div>
                                    <div class="col s12">
                                        <div class="row">
                                            <div class="col s12 m3 l3">
                                                <img src="{{url('/')}}{{$postulation->job->user->path_avatar}}" class="profile-card-employee" style="width: 40px;height: 40px;">
                                            </div>

                                            <div class="col s12 m7 l7">
                                                <p>
                                                    <b>{{$postulation->job->user->name}}</b><br>
                                                    {{$postulation->job->user->email}}
                                                </p>
                                            </div>

                                            <div class="col s12 center">
                                                @if($postulation->status === 'close')
                                                    <a href="{{ route('graduate.offers.details', ['id' => $postulation->id, 'slug' => $postulation->job->slug]) }}" class="btn btn-info white-text grey">Close</a>
                                                @endif

                                                @if($postulation->status === 'review')
                                                    <a href="{{ route('graduate.offers.details', ['id' => $postulation->id, 'slug' => $postulation->job->slug]) }}" class="btn btn-info white-text amber">In process</a>
                                                @endif

                                                @if($postulation->status === 'acepted')
                                                    <a href="{{ route('graduate.offers.details', ['id' => $postulation->id, 'slug' => $postulation->job->slug]) }}" class="btn btn-info white-text purple">approved</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach
            </div>

        </div>
    </div>
@endsection

