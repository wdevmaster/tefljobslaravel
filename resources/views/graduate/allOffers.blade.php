@extends('layouts.mainv2')

@section('title', 'All Offers')
@section('head')
<!--CSRF-->
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('bodyClass', 'grey lighten-5')

@section('content')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">View offers</b></p>
                </div>

                <div class="col s12">
                    <form class="col s12" action="{{route('offers.searching')}}" method="POST" autocomplete="off">
                        <div class="row">
                            <div class="input-field col s12">
                                <input name="employer" type="text" class="validate input-search" placeholder="Type the name employee">
                            </div>
                        </div>
                    </form>
                </div>
                @foreach($offers->chunk(3) as $chunk)
                <div class="row">
                    @foreach($chunk as $offer)
                    <div class="col s12 m4 l4">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12">
                                        <p class="title-offer-car center">{{str_limit($offer->title, 34)}}</p>
                                        <p align="justify" class="grey-text">{!!str_limit(strip_tags($offer->description), 200)!!}</p>
                                    </div>
                                    <div class="col s12">
                                        <div class="row">
                                            <div class="col s12 m3 l3">
                                                <img src="{{url('/')}}{{$offer->user->path_avatar}}" class="profile-card-employee"  style="width: 40px;height: 40px;">
                                            </div>

                                            <div class="col s12 m7 l7">
                                                <p>
                                                    <b>{{str_limit($offer->user->name, 18)}}</b><br>
                                                    {{str_limit($offer->user->email, 20)}}</p>
                                            </div>

                                            <div class="col s12 center">
                                                @if($postulation = $offer->postulations->where('user_id', Auth::id())->first())
                                                    @if($postulation->status === 'close')
                                                        <a href="{{ route('graduate.offers.details', ['id' => $postulation->id, 'slug' => $offer->slug]) }}" class="btn btn-info white-text grey">Close</a>
                                                    @elseif($postulation->status === 'review')
                                                        <a href="{{ route('graduate.offers.details', ['id' => $postulation->id, 'slug' => $offer->slug]) }}" class="btn btn-info white-text amber">In process</a>
                                                    @elseif($postulation->status === 'acepted')
                                                        <a href="{{ route('graduate.offers.details', ['id' => $postulation->id, 'slug' => $offer->slug]) }}" class="btn btn-info white-text purple">approved</a>
                                                    @endif
                                                @else
                                                <a href="{{route('offer.detail', ['id' => $offer->id, 'slug' => $offer->slug])}}" class="btn btn-info white-text green">More Info</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach
                <div class="row">
                     <center>{{$offers->links()}}</center>
                </div>
            </div>
        </div>
    </div>
@endsection
