@extends('layouts.mainv2')

@section('title', 'Graduate | Profile')
@section('head')
<!--CSRF-->
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('bodyClass', 'grey lighten-5')

@section('content')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>
        <!--CONTENT PROFILE  -->
            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">Profile</b></p>
                </div>

                <div class="col s12">
                    <div class="card card-profile">
                        <div class="row">

                            <!-- MENSAJES -->
                            @if ($errors->has('cv_file'))
                                <div class="col s12 message center">
                                    <p><i class="material-icons red-text left">highlight_off</i> {{ $errors->first('cv_file') }}</p>
                                </div>
                            @endif
                            <!-- FIN DE MENSAJES -->
                            <div class="col s12 m4 l4">
                                <div class="col s12">
                                    <div class="profile-avatar" style="{{ Auth::user()->path_avatar ? 'background: none; background-color: white;' : '' }}">
                                        <img src="{{url('/')}}{{Auth::user()->path_avatar}}">
                                    </div>
                                </div>
                                <div class="col s12 m6 l6">
                                    <a href="{{route('graduate.profile.edit')}}" class="transparent btn btn-edit"><i class="material-icons">edit</i></a>
                                </div>

                                <div class="col s12 m6 l6">
                                    <a class="transparent btn btn-edit modal-trigger" href="#modaldelete"><i class="material-icons">delete</i></a>
                                    @include('modals.deleteUser')
                                </div>
                            </div>

                            <div class="col s12 m7 l7">
                                <h5 class="title">{{Auth::user()->name}}</h5>
                                <p class="grey-text sub-title">YOUR PERSONAL DESCRIPTION</p>
                                <p class="content resume">{{Auth::user()->biography}}</p>

                                <div class="row">
                                    @if(empty(Auth::user()->path_cv))
                                    <div class="col s12 m6 l6">
                                        <a href="#uploadCV" class="btn amber darken-3 account-btn modal-trigger">Upload your CV (Word/PDF)</a>
                                    </div>
                                    @else
                                    <div class="col s12 m6 l6">
                                        <a target="_blank" href="{{url('/')}}/{{Auth::user()->path_cv}}" target="_blank" class="btn amber darken-3 account-btn">Download digital CV</a>
                                    </div>
                                    @endif

                                    <div class="col s12 m6 l6">
                                        @if(empty(Auth::user()->video_presentation))
                                            <a class="modal-trigger btn red darken-3 account-btn" href="#uploadVideo">Upload your video</a>
                                        @else
                                            <a class="modal-trigger btn red darken-3 account-btn" href="#videoPresentation">Presentation video</a>
                                        @endif

                                        <!-- Modal Structure -->
                                        <div id="videoPresentation" class="modal">
                                            <div class="modal-content">
                                                <h5 class="title-app">
                                                    Presentation <b class="purple-text color-title">Video</b>
                                                </h5>

                                                <div class="row">
                                                    <div class="col s10 offset-s1">
                                                        <div class="video-container">
                                                            <video class="video" frameborder="0" allowfullscreen controlslist="nodownload" controls>
                                                                <source src="{{url('/')}}/{{Auth::user()->video_presentation}}" autostart="false"> Your browser does not support the video tag.
                                                            </video>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer modal-footer-video">
                                                <a href="#!" class="modal-action modal-close btn-flat">close</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col s12 m1 l1">
                                <!-- Modal Trigger -->
                                <a class="modal-trigger right black-text" href="#modal1"><i class="material-icons">help_outline</i></a>

                                <!-- Modal Structure -->
                                <div id="modal1" class="modal">
                                    <div class="modal-content">
                                        <h5 class="title-app">
                                            Recommendations for <b class="purple-text color-title">your Profile</b>
                                        </h5>

                                        <p align="justify">Take care of all the details of your CV to improve your work opportunities.</p>
                                        <p align="justify">Your CV is the first contact with the world of work: so you must work on it with professionalism and rigor.</p>
                                        <p align="justify">Do not limit yourself to listing your career, use the CV as the presentation of your personal brand.</p>

                                        <p align="justify">Always keep in mind the type of job and company you are heading. It adapts to the information and the presentation according to what they seek and to differentiate from the rest of the candidates.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="#!" class="modal-action modal-close btn-flat">close</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col s12 m6 l6">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s10 offset-s1">
                                <div class="col s12 m3 l3">
                                    <img src="{{url('/')}}/img/v2/profile.png" class="icon-dashboard">
                                </div>

                                <div class="col s12 m9 l9">
                                    <p><b class="grey-text">ADDRESS:</b> {{Auth::user()->address}}</p>
                                    <p><b class="grey-text">PHONE / WHATSAPP:</b> {{Auth::user()->phone}}</p>
                                    <p><b class="grey-text">E-MAIL:</b> {{Auth::user()->email}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12 m6 l6">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s10 offset-s1">
                                <div class="col s12 m3 l3">
                                    <img src="{{url('/')}}/img/v2/maps-and-flags.png" class="icon-dashboard">
                                </div>

                                <div class="col s12 m9 l9">
                                    <p><b class="grey-text">NATIONALITY:</b> {{Auth::user()->country}}</p>
                                    <p><b class="grey-text">LANGUAGE:</b> {{Auth::user()->lenguage}}</p>
                                    <p><b class="grey-text">BRITHDAY:</b> {{Auth::user()->birthdate}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12">
                    <div class="col s12">
                        <h5 class="grey-text text-lighten-1">REFERENCES</h5>
                    </div>
                    @foreach(Auth::user()->references as $key => $reference)
                    @if($key < 3)
                    <div class="col s12 m4 l4">
                        <div class="card-references">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s10">
                                        <b class="purple-text text-darken-4">{{$reference->name}}</b>
                                        <p class="grey-text">Email: {{$reference->email}}</p>
                                        <p class="grey-text">phone: {{$reference->phone}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
    
                </div>

                <div class="col s12 m6 l6">
                    <div class="col s12">
                        <h5 class="grey-text text-lighten-1">EDUCATION</h5>
                    </div>
                    @foreach(auth::user()->studies as $study)
                    <div class="col s12">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12 m3 l3">
                                        <img src="{{url('/')}}/img/v2/mortarboard.png" class="icon-dashboard">
                                    </div>

                                    <div class="col s12 m7 l7">
                                        <b class="purple-text text-darken-4">({{$study->end_date}})</b>
                                        <p class="grey-text">{{$study->school}}</p>
                                        <p>{{$study->description}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

                <div class="col s12 m6 l6">
                    <div class="col s12">
                        <h5 class="grey-text text-lighten-1">WORK EXPERIENCE</h5>
                    </div>
                    @foreach(Auth::user()->experiences as $exp)
                    <div class="col s12">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12 m3 l3">
                                        <img src="{{url('/')}}/img/v2/briefcase.png" class="icon-dashboard">
                                    </div>

                                    <div class="col s12 m7 l7">
                                        <b class="purple-text text-darken-4">({{$exp->date}})</b>
                                        <p class="grey-text">{{$exp->description}}</p>
                                        <p>{{$exp->location}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

            </div>
        <!--ENDCONTENT PROFILE  -->
        </div>
    </div>
    @include('graduate.modals.uploadCV')
    @include('graduate.modals.uploadVideo')
@endsection
