<!-- Logation and References -->
<div id="modalReferences" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            References <b class="purple-text color-title">information</b>
        </h5>

        <div class="row">
            <form class="col s12" autocomplete="off" action="{{ route('graduate.reference.info') }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Type the full name" name="name" id="namereference" type="text" class="validate input-delete">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type the email" name="email" id="text" type="text" class="validate input-delete">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type the phone number" name="phone" id="text" type="text" class="validate input-delete">
                    </div>

                    <div class="col s12">
                        <button type="submit" class="btn btn-delete purple white-text">save</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>