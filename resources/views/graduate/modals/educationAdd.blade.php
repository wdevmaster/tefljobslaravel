<!-- Education -->
<div id="modalEducation" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            Education <b class="purple-text color-title">information</b>
        </h5>

        <div class="row">
            <form class="col s12" autocomplete="off" method="POST" action="{{route('graduate.education.create')}}">
                {{csrf_field()}}
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Type the date when you finish your education level" name="end_date" id="education" type="text" class="validate input-delete" required>
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type the name of the site" name="school" id="text" type="text" class="validate input-delete" required>
                    </div>

                    <div class="input-field col s12">
                        <textarea placeholder="Type a short detail" name="description" id="description" class="materialize-textarea input-delete" data-length="200" required></textarea>
                    </div>

                    <div class="col s12">
                        <button type="submit" class="btn btn-delete purple white-text">save</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>