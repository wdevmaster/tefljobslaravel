 <!-- Modal Structure -->
  <div id="uploadVideo" class="modal">
    <div class="modal-content center">
      <h5>Select your video presentation <small>(only mp4 files)</small></h5>
      <form action="{{ route('graduate.upload.video') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
        {{csrf_field()}}
        <div class="row">
          <div class="col s6 offset-s3"><br>
            <div class="file-field input-field">
              <div class="btn purple">
                <span>File</span>
                <input type="file" name="video_file" id="cv_file">
              </div>
              <div class="file-path-wrapper">
                <input class="file-path validate" type="text">
              </div>
            </div>
          </div>
        </div>
      
    </div>
    <div class="modal-footer">
      <button type="submit" class="waves-effect waves-green btn purple">Upload</button>
      <button href="#!" class="modal-close waves-effect waves-green btn-flat">Cancel</button>
    </div>
    </form>
  </div>