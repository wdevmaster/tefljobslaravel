<!-- work -->
<div id="modalWork" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            Work <b class="purple-text color-title">information</b>
        </h5>

        <div class="row">
            <form class="col s12" autocomplete="off" method="POST" action="{{route('graduate.work.create')}}">
                {{csrf_field()}}
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Type the last year in your work" name="date" id="date" type="text" class="validate input-delete" required>
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type the position or role" name="description" id="text" type="text" class="validate input-delete" required>
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type the location" id="text" name="location" type="text" class="validate input-delete" required>
                    </div>

                    <div class="col s12">
                        <button type="submit" class="btn btn-delete purple white-text">save</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>