<!-- Modal Structure -->
<div id="modal1" class="modal">
    <div class="modal-content">
        <h5 class="title-app">
            Recommendations for <b class="purple-text color-title">your Profile</b>
        </h5>

        <p align="justify">Take care of all the details of your CV to improve your work opportunities.</p>
        <p align="justify">Your CV is the first contact with the world of work: so you must work on it with professionalism and rigor.</p>
        <p align="justify">Do not limit yourself to listing your career, use the CV as the presentation of your personal brand.</p>

        <p align="justify">Always keep in mind the type of job and company you are heading. It adapts to the information and the presentation according to what they seek and to differentiate from the rest of the candidates.</p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat">close</a>
    </div>
</div>