<!-- ADDRESS -->
<div id="modalAddress" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            Contact <b class="purple-text color-title">information</b>
        </h5>

        <div class="row">
            <form class="col s12" autocomplete="off" method="POST" action="{{ route('graduate.contact.info') }}">
                {{csrf_field()}}
                <p>Remember all fields are required.</p>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Type your address" id="address" name="address" type="text" class="validate input-delete" value="{{Auth::user()->address}}">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type your Phone numer or your whatsapp" name="phone" id="text" type="text" class="validate input-delete" value="{{Auth::user()->phone}}">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type your Emails" required id="text" type="email" name="email" class="validate input-delete" value="{{Auth::user()->email}}">
                    </div>

                    <div class="col s12">
                        <button type="submit" class="btn btn-delete purple white-text">save</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>