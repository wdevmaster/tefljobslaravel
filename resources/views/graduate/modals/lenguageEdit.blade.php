<!-- Logation and lenaguaje -->
<div id="modalLanguaje" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            Nationality <b class="purple-text color-title">information</b>
        </h5>

        <div class="row">
            <form class="col s12" autocomplete="off" method="POST" action="{{route('graduate.lenguage.info')}}">
                {{csrf_field()}}
                <p>Remember all fields are required.</p>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Type your nationality" name="country" id="address" type="text" class="validate input-delete" value="{{Auth::user()->country}}">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type your native languaje" name="lenguage" id="text" type="text" class="validate input-delete" value="{{Auth::user()->lenguage}}">
                    </div>

                    <div class="input-field col s12">
                        <input placeholder="Type your brithdate" name="birthdate" id="birthdateAF" type="date" class="validate input-delete datepicker" value="{{Auth::user()->birthdate}}">
                    </div>

                    <div class="col s12">
                        <button type="submit" class="btn btn-delete purple white-text">save</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>