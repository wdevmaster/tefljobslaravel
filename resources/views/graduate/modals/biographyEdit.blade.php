<div id="biographyEdit" class="modal">
    <div class="modal-content form-bottom-space">
        <h5 class="title-app">
            Personal <b class="purple-text color-title">information</b>
        </h5>

        <div class="row">
            <form class="col s12" method="POST" action="{{ route('graduate.personal.info') }}">
                {{csrf_field()}}
                <p>Remember all fields are required.</p>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Type your full name" id="name" name="name" type="text" class="validate input-delete" required value="{{Auth::user()->name}}">
                    </div>

                    <div class="input-field col s12">
                        <textarea placeholder="Type your personal description" name="biography" id="description" class="materialize-textarea input-delete" data-length="300" required>{{Auth::user()->biography}}</textarea>
                    </div>

                    <div class="col s12">
                        <button type="submit" class="btn btn-delete purple white-text">save</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close btn-flat space-special-close">Close</a>
    </div>
</div>