@extends('layouts.mainv2')

@section('title', 'My Offers')
@section('head')
<!--CSRF-->
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('bodyClass', 'grey lighten-5')

@section('content')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>
            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">Offer details</b></p>
                </div>

                <div class="col s12">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s12 m4 l4">
                                <div class="col s12">
                                    <div class="profile-avatar" style="{{ $postulation->job->user->path_avatar ? 'background: none; background-color: white;' : '' }}">
                                        <img src="{{url('/')}}{{$postulation->job->user->path_avatar}}">
                                    </div>
                                </div>

                                <div class="col s12">
                                    <h5 class="purple-text center">{{$postulation->job->user->name}}</h5>
                                    <p class="center">{{$postulation->job->user->email}}</p>
                                    <p class="center"><a href="https://www.englishservices.com.ar/" class="center grey-text" target="_blank">{{$postulation->job->user->website}}</a></p>
                                    <p class="center">{{$postulation->job->user->address}}</p>
                                </div>
								@if($postulation->status === 'review')
                                <div class="col s12 center">
                                    <form action="{{route('offers.cancel')}}" method="POST">
                                        {{csrf_field()}}
                                        <input type="hidden" name="postulation_id" value="{{$postulation->id}}">
                                        <input type="hidden" name="_method" value="delete">
                                        <button type="submit" class="btn btn-info white-text red">Cancel Application</button>
                                    </form>
                                </div>
                                @endif
								@if($postulation->status === 'close')
                                <div class="col s12 center">
                                    <a href="" class="btn btn-info white-text grey disabled">Close</a>
                                </div>
                                @endif
								@if($postulation->status === 'acepted')
                                 <div class="col s12 center">
                                    <div class="message-acepted">
                                        <h5 class="center green-text">Congratulations!</h5>
                                        <p class="grey-text">You have been accepted. The employer will contact you soon.</p>
                                    </div>
                                </div>
                                @endif
                            </div>

                            <div class="col s12 m8 l8 space-right">
                            	{!! $postulation->job->description !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

