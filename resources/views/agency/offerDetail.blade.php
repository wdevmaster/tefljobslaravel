@extends('layouts.mainv2')

@section('title', 'Offers Details')
@section('head')
<!--CSRF-->
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('bodyClass', 'grey lighten-5')

@section('content')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">Offer details</b></p>
                </div>

                <div class="col s12">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s12 m4 l4">
                                <div class="col s12">
                                    <div class="profile-avatar" style="{{ $offer->user->path_avatar ? 'background: none; background-color: white;' : '' }}">
                                        <img src="{{url('/')}}{{ $offer->user->path_avatar }}">
                                    </div>
                                </div>

                                <div class="col s12">
                                    <h5 class="purple-text center">{{ $offer->user->name }}</h5>
                                    <p class="center">{{ $offer->user->email }}</p>
                                    <p class="center"><a href="{{ $offer->user->website }}" class="center grey-text" target="_blank">{{ $offer->user->website }}</a></p>
                                    <p class="center">{{ $offer->user->address }}</p>
                                </div>
                            </div>

                            <div class="col s12 m8 l8 space-right">
                                {!! $offer->description !!}
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col s12">
                    <h5 class="grey-text">Candidates who applied</h5>
                </div>
                
                <div class="col s12">
                    <div class="row">
                        @foreach($offer->postulations as $postulation)
                        <div class="col s12 m4 l4">
                            <div class="message-dashboard">
                                <div class="row">
                                    <div class="col s10 offset-s1">
                                        <div class="col s12">
                                            <img src="{{url('/')}}{{$postulation->user->path_avatar}}" class="candidate-profile">
                                        </div>

                                        <div class="col s12">
                                            <p class="title-offer-car center">{{$postulation->user->name}}</p>
                                            <p align="justify" class="grey-text">{{str_limit($postulation->user->biography, 200)}}</p>
                                        </div>
                                        <div class="col s12">
                                            <div class="col s12 center">
                                                <a href="{{route('public.profile', $postulation->user->email)}}" class="btn btn-info white-text green">More Info</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

