@extends('layouts.mainv2')

@section('title', 'Offers')
@section('head')
<!--CSRF-->
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('bodyClass', 'grey lighten-5')

@section('content')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p>Home / <b class="purple-text">My offers</b></p>
                </div>
                @foreach($offers->chunk(3) as $chunk)
                <div class="row">
                    @foreach($chunk as $offer)
                        <div class="col s12 m4 l4">
                            <div class="message-dashboard">
                                <div class="row">
                                    <div class="col s10 offset-s1">
                                        <div class="col s12">
                                            <p class="title-offer-car center">{{$offer->title}}</p>
                                            <p align="justify" class="grey-text">{{str_limit($offer->description, 200)}}</p>
                                        </div>
                                        <div class="col s12">
                                            <div class="row">
                                                <div class="col s12 m3 l3">
                                                    <img src="{{url('/')}}{{$offer->user->path_avatar}}" class="profile-card-employee">
                                                </div>

                                                <div class="col s12 m7 l7">
                                                    <p>
                                                        <b>{{$offer->user->name}}</b><br>
                                                        {{$offer->user->email}}</p>
                                                </div>

                                                <div class="col s12 center">
                                                    <a href="{{Route('agency.offer.detail', ['id' => $offer->id, 'slug' => $offer->slug])}}" class="btn btn-info white-text green">Open</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                @endforeach
                <div class="row">
                    <center>{{$offers->links()}}</center>
                </div>
            </div>

        </div>
    </div>
@endsection

