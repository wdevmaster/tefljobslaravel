@extends('layouts.mainv2')

@section('title', 'Welcome to our Job Placement App!')

@section('bodyClass', 'grey lighten-5')
@section('content')
@role('admin')
    @include('navs.navCore') 
    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav') 
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p><b class="purple-text">Home</b> / Dashboard</p>
                </div>

                <div class="col s12">
                    <div class="message-wellcome">
                        <div class="row">
                            <div class="col" style="padding-right: 2.5rem;">
                            <h5 class="purple-text text-darken-4">Welcome to our Job Placement App!</h5>
                            <p align="justify" class="admin-text">This platform has been developed as an exclusive service for the Instituto Buenos Aires TEFL, by TECHMOV if you have any questions about its operation consult the help or send an email to info@techmov.net</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s12 m4 l4">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s10 offset-s1">
                                <div class="col s12 m3 l3">
                                    <img src="{{url('/')}}/img/v2/calendar.png" class="icon-dashboard">
                                </div>

                                <div class="col s12 m9 l9">
                                    <p class="grey-text center">YOUR LAST LOGIN</p>
                                    <p class="center">@php  
                                        $date = Auth::user()->last_login;
                                        $lastLog = date("d-m-Y", strtotime($date));
                                        if($lastLog === '01-01-1970'){
                                            echo 'Today is your first log';
                                        }else{
                                            echo $lastLog;
                                        }
                                    @endphp</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12 m4 l4">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s10 offset-s1">
                                <div class="col s12 m3 l3">
                                    <img src="{{url('/')}}/img/v2/network.png" class="icon-dashboard">
                                </div>

                                <div class="col s12 m9 l9">
                                    <p class="grey-text center">TOTAL USERS</p>
                                    <p class="center">{{$usersCount}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12 m4 l4">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s10 offset-s1">
                                <div class="col s12 m3 l3">
                                    <img src="{{url('/')}}/img/v2/3d-printing-software.png" class="icon-dashboard">
                                </div>

                                <div class="col s12 m9 l9">
                                    <p class="grey-text center">LAST SOTWARE UPDATE</p>
                                    <p class="center">12/02/2019</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12 m8 l8">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s12">
                                <p class="grey-text center">UPDATE HISTORY</p>

                                <ul class="collection">
                                    <li class="collection-item">Install Platform in the server</li>
                                    <li class="collection-item">Test new users accounts</li>
                                    <li class="collection-item">Change new interface</li>
                                    <li class="collection-item">More ideas</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col s12 m4 l4">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s12">
                                <p class="grey-text center">USERS HISTORY TODAY</p>

                                <ul class="collection">
                                    <li class="collection-item">Graduates Login: 120</li>
                                    <li class="collection-item">Employee Login: 20</li>
                                    <li class="collection-item">Agencies Login: 12</li>
                                    <li class="collection-item">Admin Login: 34</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endrole


@role('employer')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p><b class="purple-text">Home</b> / Dashboard</p>
                </div>

                <div class="col s12">
                    <div class="message-wellcome">
                        <div class="row">
                            <div class="col" style="padding-right: 2.5rem;">
                                <h5 class="purple-text text-darken-4">Welcome to our Job Placement App!</h5>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col s12 m4 l4">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s10 offset-s1">
                                <div class="col s12 m3 l3">
                                    <img src="{{url('/')}}/img/v2/calendar.png" class="icon-dashboard">
                                </div>

                                <div class="col s12 m9 l9">
                                    <p class="grey-text center">YOUR LAST LOGIN</p>
                                    <p class="center">@php  
                                        $date = Auth::user()->last_login;
                                        $lastLog = date("d-m-Y", strtotime($date));
                                        if($lastLog === '01-01-1970'){
                                            echo 'Today is your first log';
                                        }else{
                                            echo $lastLog;
                                        }
                                    @endphp</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12 m4 l4">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s10 offset-s1">
                                <div class="col s12 m3 l3">
                                    <img src="{{url('/')}}/img/v2/network.png" class="icon-dashboard">
                                </div>

                                <div class="col s12 m9 l9">
                                    <p class="grey-text">YOUR ACTIVE OFFERS</p>
                                    <p class="center">{{$employerJobsCount}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12 m4 l4">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s10 offset-s1">
                                <div class="col s12 m3 l3">
                                    <img src="{{url('/')}}/img/v2/portfolio.png" class="icon-dashboard">
                                </div>

                                <div class="col s12 m9 l9">
                                    <p class="grey-text">YOUR CANDIDATES OFFERS</p>
                                    <p class="center">{{ $postulationCount }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12">
                    <h5 class="grey-text title-offers">TOP 3 RECENTS CANDIDATES</h5>
                </div>

                @foreach($recentGraduates as $graduate)
                <div class="col s12 m4 l4">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s10 offset-s1">
                                <div class="col s12">
                                    <img src="{{url('/')}}{{$graduate->user->path_avatar}}" class="candidate-profile" style="width: 80px;height: 80px;">
                                </div>
                                
                                <div class="col s12">
                                    <p class="title-offer-car center">{{$graduate->user->name}}</p>
                                    <p class="grey-text center">
                                        @if ($graduate->user->getRating() != 0)
                                        @for($i = 0; $i < $graduate->user->points(); $i++)
                                            <i class="yellow-text text-darken-1 material-icons">star</i>
                                        @endfor
                                        @for($j = 0; $j < 5 - $graduate->user->points(); $j++)
                                            <i class="grey-text material-icons">star_border</i>
                                        @endfor
                                        @else
                                            @for($j = 0; $j < 5; $j++)
                                                <i class="grey-text material-icons">star_border</i>
                                            @endfor
                                        @endif
                                    </p>
                                </div>
                                
                                <div class="col s12">
                                    <div class="col s12 center">
                                        <a href="{{
                                                route('candidate.resume', [
                                                    'email' => $graduate->user->email, 
                                                    'postulation_id' => $graduate->id 
                                                ])
                                            }}" 
                                            class="btn btn-info white-text green">More Info</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach


            </div>
        </div>
    </div>
@endrole

@role('graduate')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                @include('navs.sideNav')
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p><b class="purple-text">Home</b> / Dashboard</p>
                </div>

                <div class="col s12">
                    <div class="message-wellcome">
                        <div class="row">
                            <div class="col" style="padding-right: 2.5rem;">
                                <h5 class="purple-text text-darken-4">Welcome to our Job Placement App!</h5>
                                <p class="admin-text" align="justify">You have infinite access to this app given the fact that you are a TEFL Graduate from Buenos Aires TEFL Institute. These are the current job opportunities you can apply directly by clicking below.</p>
                                <p class="admin-text" align="justify">Please read each one carefully and then, <b class="purple-text">click on submit application</b> in the job position of your interest. Best of luck!</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12 m4 l4">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s10 offset-s1">
                                <div class="col s12 m3 l3">
                                    <img src="{{url('/')}}/img/v2/calendar.png" class="icon-dashboard">
                                </div>

                                <div class="col s12 m9 l9">
                                    <p class="grey-text center">YOUR LAST LOGIN</p>
                                    <p class="center">@php  
                                        $date = Auth::user()->last_login;
                                        $lastLog = date("d-m-Y", strtotime($date));
                                        if($lastLog === '01-01-1970'){
                                            echo 'Today is your first log';
                                        }else{
                                            echo $lastLog;
                                        }
                                    @endphp</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col s12 m4 l4">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s10 offset-s1">
                                <div class="col s12 m3 l3">
                                    <img src="{{url('/')}}/img/v2/network.png" class="icon-dashboard">
                                </div>

                                <div class="col s12 m9 l9">
                                    <p class="grey-text center">YOUR POSTULATIONS ACTIVE</p>
                                    <p class="center">{{$postulationActive}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col s12 m4 l4">
                    <div class="message-dashboard">
                        <div class="row">
                            <div class="col s10 offset-s1">
                                <div class="col s12 m3 l3">
                                    <img src="{{url('/')}}/img/v2/favourites.png" class="icon-dashboard">
                                </div>

                                <div class="col s12 m9 l9">
                                    <p class="grey-text center">YOUR FEEDBACK SCORING</p>
                                    <p class="center">{{ Auth::user()->getRating() }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col s12">
                    <h5 class="grey-text title-offers">TOP 4 RECENTS OFFERS</h5>
                </div>
                @foreach($recentOffer->chunk(2) as $chunk)
                <div class="row">
                    @foreach($chunk as $job)
                    <div class="col s12 m6 l6">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12">
                                        <p class="title-offer-car center">{{str_limit($job->title, 34)}}</p>
                                        <p align="justify" class="grey-text">{!!str_limit(strip_tags($job->description), 200)!!}</p>
                                    </div>
                                    <div class="col s12">
                                        <div class="row">
                                            <div class="col s12 m2 l2">
                                                <img src="{{url('/')}}{{$job->user->path_avatar}}" class="profile-card-employee" style="width: 40px;height: 40px;">
                                            </div>
                                            
                                            <div class="col s12 m8 l8">
                                                <p>
                                                    <b>{{str_limit($job->user->name, 18)}}</b><br>
                                                    {{str_limit($job->user->email, 20)}}</p>
                                            </div>
                                            
                                            <div class="col s12 center">
                                                <a href="{{route('offer.detail', ['id' => $job->id, 'slug' => $job->slug])}}" class="btn btn-info white-text green">More Info</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endrole

@role('supervisor')
    @include('navs.navCore')

    <div class="row row-space">
        <div class="col s12">
            <div class="col s12 m2 l2 sidenavlist no-movile-element">
                 @include('navs.sideNav')
            </div>

            <div class="col s12 m10 l10">
                <div class="col s12 position">
                    <p><b class="purple-text">Home</b> / Dashboard</p>
                </div>

                <div class="col s12">
                    <div class="message-wellcome">
                        <div class="row">
                            <div class="col" style="padding-right: 2.5rem;">
                                <h5 class="purple-text text-darken-4">Welcome to our Job Placement App!</h5>
                            </div>
                        </div>
                    </div>
                </div>

               
                <div class="col s12">
                    <h5 class="grey-text title-offers">TOP 4 RECENTS OFFERS</h5>
                </div>
                
                @foreach($recentOffer->chunk(2) as $chunk)
                <div class="row">
                    @foreach($chunk as $job)
                    <div class="col s12 m6 l6">
                        <div class="message-dashboard">
                            <div class="row">
                                <div class="col s10 offset-s1">
                                    <div class="col s12">
                                    <p class="title-offer-car center">{{str_limit($job->title, 34)}}</p>
                                        <p align="justify" class="grey-text">{!!str_limit(strip_tags($job->description), 200)!!}</p>
                                    </div>
                                    <div class="col s12">
                                        <div class="row">
                                            <div class="col s12 m2 l2">
                                                <img src="{{url('/')}}{{$job->user->path_avatar}}" class="profile-card-employee" style="width: 40px;height: 40px;">
                                            </div>
                                            
                                            <div class="col s12 m8 l8">
                                                <p>
                                                    <b>{{str_limit($job->user->name, 18)}}</b><br>
                                                    {{str_limit($job->user->email, 20)}}</p>
                                            </div>
                                            
                                            <div class="col s12 center">
                                                <a href="{{route('offer.detail', ['id' => $job->id, 'slug' => $job->slug])}}" class="btn btn-info white-text green">More Info</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endrole
@endsection