<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Job;
class AgenciesController extends Controller
{
    public function offers()
    {
    	$offers = Job::where('status', 'open')->orderBy('id', 'DESC')->paginate(9);
    	return view('agency.offers', compact('offers'));
    }

    public function offerDetail($id, $slug)
    {
    	$offer = Job::where('id', $id)->where('slug', $slug)->firstOrFail();
    	return view('agency.offerDetail', compact('offer'));
    }


}
