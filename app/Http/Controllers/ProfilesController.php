<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Job;
use App\Phone;
use App\Review;
use App\Reference;
use App\Study;
use App\Experience;
use Validator;
use Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class ProfilesController extends Controller
{
    public function personalInfo(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'biography' => 'max:300'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }else{
            $user = User::find(Auth::user()->id);
            $user->name = $request->name;
            $user->biography = $request->biography;
            if ($user->save()) {
                return back()->with('success', 'Data save');
            }else{
                return back()->with('error', 'Error to save');
            }
        }
    }

    public function contactInfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }else{
            $user = User::find(Auth::user()->id);
            $user->address = $request->address;
            $user->phone = $request->phone;
            $user->email = $request->email;
            if ($user->save()) {
                return back()->with('success', 'Data save');
            }else{
                return back()->with('error', 'Error to save');
            }
        }
    }

    public function lenguageInfo(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $user->country = $request->country;
        $user->lenguage = $request->lenguage;
        $user->birthdate = $request->birthdate;


        if ($user->save()) {
            return back()->with('success', 'Data save');
        }else{
            return back()->with('error', 'Error to save');
        }
    }

    public function referenceInfo(Request $request)
    {
        $user = new Reference();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->user_id = Auth::user()->id;


        if ($user->save()) {
            return back()->with('success', 'Data save');
        }else{
            return back()->with('error', 'Error to save');
        }
    }

    public function referenceDelete(Request $request)
    {
        $d = Reference::find($request->id);
        if ($d->delete()) {
            return back()->with('success', 'Data save');
        }else{
            return back()->with('error', 'Error to save');
        }
    }

    public function educationCreate (Request $request){
        $user = new Study();
        $user->school = $request->school;
        $user->end_date = $request->end_date;
        $user->description = $request->description;
        $user->user_id = Auth::user()->id;


        if ($user->save()) {
            return back()->with('success', 'Data save');
        }else{
            return back()->with('error', 'Error to save');
        }  
    }

    public function educationDelete(Request $request)
    {
        $d = Study::find($request->id);
        if ($d->delete()) {
            return back()->with('success', 'Data save');
        }else{
            return back()->with('error', 'Error to save');
        }
    }

    public function workCreate(Request $request)
    {
        $user = new Experience();
        $user->date = $request->date;
        $user->description = $request->description;
        $user->location = $request->location;
        $user->user_id = Auth::user()->id;
        if ($user->save()) {
            return back()->with('success', 'Data save');
        }else{
            return back()->with('error', 'Error to save');
        } 
    }

    public function workDelete(Request $request)
    {
        $d = Experience::find($request->id);
        if ($d->delete()) {
            return back()->with('success', 'Data save');
        }else{
            return back()->with('error', 'Error to save');
        }
    }
}
