<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Postulation;
use App\User;
use App\Job;
use Mail;

class AplicattionsController extends Controller
{
    public function sendApplication(Request $request)
    {
    	if ($request->ajax()) {
    		$new = new Postulation();
    		$new->user_id = $request->user_id;
            $new->job_id = $request->job_id;
    		$new->status = 'review';

            $job = Job::find($request->job_id);
            $user = User::find($request->user_id);

            //MAIL EMPLOYER
            Mail::send('emails.applyEmployerNotification', ['job' => $job, 'user' => $user], function($mail) use($job, $user){
                $mail->subject('Buenos Aires TEFL CV');
                $mail->to($job->user->email, $job->user->name);
            });

            //MAIL EMPLOYER
            Mail::send('emails.applyGraduateNotification', ['job' => $job, 'user' => $user], function($mail) use($job, $user){
                $mail->subject('your application for vacancy "'.$job->title.'" has been sent to the employer');
                $mail->to($user->email, $user->name);
            });
    		if ($new->save()) {
    			return response()->json(['success'=>'Add success']);
    		}else{
    			return response()->json(['error'=>'Error']);
    		}
    	}
    }
}
