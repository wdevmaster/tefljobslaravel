<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Postulation;
use App\User;
use App\Job;
use DB;
use Mail;


class CandidatesController extends Controller
{
    public function candidatesIndex()
    {
		$users = User::leftJoin('reviews', 'users.id', '=', 'reviews.reviewable_id')
					->whereHas('roles', function ($query) {
						$query->where('name', '=', 'graduate');
					})
					->select(DB::raw('avg(rating) as rank, users.*'))
					->groupBy('users.id')
					->orderBy('rank', 'desc')
					->get();

    	return view('employer.candidates', compact('users'));
    }

	public function resumeCandidate($mail, $postulation_id)
	{
    	$postulation = Postulation::find($postulation_id);

    	return view('employer.candidateProfile', compact('postulation'));
    }

	public function resumeAcepted(Request $request)
	{
    	$postulation = Postulation::find($request->postulation_id);
    	$candidate = User::find($request->candidate_id);
    	$employer 	= User::find($request->employer);
    	$job	= Job::find($postulation->job_id);

		if ($postulation->update(['status' => 'acepted']) && $job->update(['status' => 'close']))
			//MAIL Candidate
			Mail::send('emails.aceptedCandidate', ['job' => $job, 'candidate' => $candidate, 'employer' => $employer], function($mail) use($job, $candidate, $employer){
				$mail->subject('Buenos Aires TEFL CV');
				$mail->to($candidate->email, $candidate->name);
			});
		
        return back();

    }
}
