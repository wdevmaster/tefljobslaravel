<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Job;
use App\User;
use App\Review;

class ReviewController extends Controller
{
    public function index(Request $request)
    {
        $users = collect([]);
        $reviews = collect([]); 

        if (Auth::user()->isRole('employer')) {
            $reviews = Review::where('author_id', Auth::id())->get();
            $users = User::with('postulations.job')
                        ->whereHas('postulations', function($query) {
                            $query->where('status', 'acepted');
                        })
                        ->whereHas('postulations.job', function($query) {
                            $query->where('user_id', Auth::id());    
                        })
                        ->whereDoesntHave('reviews', function($query) {
                            $query->where('author_id', Auth::id());
                        })->get();
        }
        elseif($user = User::with('reviews')->find(Auth::id()))
           $reviews = $user->reviews;
           
        return view('reviews.index', compact('reviews', 'users'));
    }   

    public function create($id, Request $request)
    {
        $user = User::find($id);

        $review = Review::where('reviewable_id', $user->id)
            ->where('author_id', Auth::id())->first();

        return view('reviews.createOrEdit', compact('user', 'review'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'candidate' => 'required',
            'body' => 'required|max:120',
            'rating' => 'required',
        ]);

        $user = User::find($request->candidate);
        $author = User::find(Auth::id());
        $job = Job::whereHas('postulations', function($query) use ($user, $author) {
            $query->where('user_id', $user->id);
        })->where('user_id', $author->id)->first();

        if ($user->createReview([
            'title' => $job->title,
            'body' => strip_tags($request->body),
            'rating' => $request->rating,
        ], $author)) {
            return redirect()->route('employer.feedback');
        }

    }

    public function update(Request $request)
    {
        $request->validate([
            'body' => 'required',
            'rating' => 'required',
        ]);

        $review = Review::where('reviewable_id', $user->id)
            ->where('author_id', Auth::id())->first();

        if ($user->updateReview($review->id, [
            'body' => strip_tags($request->body),
            'rating' => $request->rating,
        ])) {
            return redirect()->route('employer.feedback');
        }
    }
}
