<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;
use Validator;
use App\Postulation;
use Illuminate\Support\Facades\Route;

class OffersController extends Controller
{
    public function offerShow($id, $title)
    {
    	$job = Job::where('id', $id)->where('slug', $title)->firstOrFail();
    	return view('employer.offerEmployer', compact('job'));
    }

    public function offerEdit($id, $slug)
    {
		$job = Job::where('id', $id)->where('slug', $slug)->firstOrFail();
    	return view('employer.editOffer', compact('job'));
    }

    public function offerUpdate(Request $request)
    {
    	$validator = Validator($request->all(), [
    		'title' => 'required',
    		'description'	=> 'required',
    	]);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }else{
        	$update = Job::find($request->offer_id);
        	$update->title = $request->title;
        	$update->slug = str_slug($request->title);
        	$update->description = $request->description;
        	
        	if ($update->save()) {
        		return redirect(route('employer.offers'))->with('success', 'Job update succefully');
        	}else{
				return redirect(route('employer.offers'))->with('error', 'Error to update');
        	}
        }
    }

    public function offerDelete(Request $request)
    {
        $job = Job::find($request->offer_id);
        $job->status = 'close';
        if ($job->save()) {
            return redirect(route('employer.offers'))->with('success', 'Job deleted succefully');
        }else{
            return redirect(route('employer.offers'))->with('error', 'Error to delete');
        }
    }

    public function offerShowCandidates($id, $slug)
    {
        $job = Job::where('id', $id)->where('slug', $slug)->firstOrFail();
        return view('employer.candidatesOffer', compact('job'));
    }

    public function offersGraduate(Request $request)
    {
        $queryJob = Job::with('postulations')->where('status', 'open');
        if ($request->isMethod('post')) {
            if  ($request->search)
                $queryJob->title($request->search)
                        ->description($request->search);
            if ($request->employer)
                $queryJob->whereHas('user', function ($query) use ($request) {
                    $query->where('name', 'like', '%'.$request->employer.'%')
                          ->orWhere('email', 'like', '%'.$request->employer.'%');
                          
                });
        }

        $offers = $queryJob->orderBy('id', 'DESC')->paginate(9);
        return view('graduate.allOffers', compact('offers'));
    }


    public function cancelPostulation(Request $request)
    {
        Postulation::find($request->postulation_id)->update(['status' => 'close']);
        return redirect('/dashboard/graduate/my-Offers');
    }
}
