<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Job;
use App\User;
use App\Postulation;
use Carbon\Carbon;
use Auth;

class JobsController extends Controller
{
    public function index(Request $request)
    {
        $users  = User::select()->orderBy('id', 'DESC')->get();
        $jobs   = Job::select()->orderBy('id', 'DESC')->paginate(10);
        if ($request->ajax()) {
            return response()->json(view('jobs.partials.jobsTable', compact('jobs'))->render());
        }
    	return view('jobs.index')->with('users', $users);

    }

    public function getJobs()
    {
        $queryJob = Job::query();

        if (Auth::user()->isEmployer())
            $queryJob->where('user_id', Auth::id());

        $jobs   = $queryJob->orderBy('id', 'DESC')->paginate(10);
    	return view('jobs.partials.jobsTable')
    		->with('jobs', $jobs);
    }

    public function searchForEmail(Request $request){
        $user = User::where('email', $request->email)->firstOrFail();
        $jobs = Job::select()->where('user_id', $user->id)->orderBy('id', 'DESC')->paginate(10);
        return view('jobs.partials.jobsTable')
            ->with('jobs', $jobs);
    }

    public function store(Request $request){
        if ($request->ajax()) {
            $validator = Validator::make($request->all(), [
                'title'          =>  'required|min:2|Max:100',
                'email'          =>  'required',
                'description'    =>  'required',
            ]);
            if ($validator->passes()) {

                if (!Auth::user()->isEmployer())
                    $user = User::where('email', $request->email)->firstOrFail();
                else
                    $user = Auth::user();

                $job = new Job();
                $job->user_id = $user->id;
                $job->title = $request->title;
                $job->slug = str_slug($request->title);
                $job->website = $request->website;
                $job->location = $request->location;
                $job->description = $request->description;
                $job->status = $request->status;


                if ($job->save()) {
                    return response()->json(['success'=>'Add success']);
                }else{
                    return response()->json(['error'=>'Save error']);
                }
            }
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }

    public function destroy(Request $request)
    {
        if ($request->ajax()) {
            $job = Job::find($request->id);
            $job->delete();
            return response(['message'=>'Job deleted']);
        }
    }

    public function edit(Request $request){
        $job = Job::find($request->id);
        $users = User::all();
        return view('jobs.partials.edit', compact('job', 'users'));
    }

    public function update(Request $request)
    {
        $user = User::where('email', $request->email)->firstOrFail();
        $new = Job::find($request->id);
        $new->title = $request->title;
        $new->user_id = $user->id;
        $new->location = $request->location;
        $new->website = $request->website;
        $new->description = $request->description;

        if ($new->save()) {
            return redirect(route('jobs.index'))->with('success', 'Update Successfully');
        }else{
            return redirect(route('jobs.index'))->with('error', 'Error to update');
        }
    }


    /*METHODS FOR GRADUATED*/
    public function getOffersForGraduated(){
        $jobs = Job::select()->where('status', 'open')->orderBy('id', 'DESC')->paginate(12);
        return view('jobs', compact('jobs'));
    }

    public function showOffersForGraduated($id, $slug)
    {
        $job = Job::find($id);
        $postulations = Postulation::where('job_id', $job->id)->get();
        $lastOffer = Job::select()->where('status', 'open')->orderBy('id', 'DESC')->take(4)->get();
        return view('details', compact('job', 'lastOffer', 'postulations'));
    }

    public function filterSearch(Request $request)
    {

        return redirect('dashboard/jobs/filter/'.$request->filter_job);
    }

    public function jobFilterOrder($order)
    {
        if ($order === 'alphabetical-order') {
            $jobs = Job::select()->where('status', 'open')->orderBy('title', 'ASC')->paginate(12);
            $active = 'alphabetical-order';
            return view('jobs')
                ->with('jobs', $jobs)
                ->with('active', $active);
        }

        if ($order === 'publication-date') {
            $jobs = Job::select()->where('status', 'open')->orderBy('id', 'DESC')->paginate(12);
            $active = 'publication-date';
            return view('jobs')
                ->with('jobs', $jobs)
                ->with('active', $active);
        } 

        if ($order === 'today') {
            $jobs = Job::select()->whereDate('created_at', '=', Carbon::now()->format('Y-m-d'))->orderBy('id', 'DESC')->paginate(12);
            $active = 'today';
            return view('jobs')
                ->with('jobs', $jobs)
                ->with('active', $active);
        }   

        // if ($order === 'week') {
        //     $ini = Carbon::now();
        //     $fin = new Carbon('2019/01/10');

        //     $jobs = Job::whereBetween('created_at', [$ini, $fin])->get();
        //     $active = 'week';
        //     // return view('jobs')
        //     //     ->with('jobs', $jobs)
        //     //     ->with('active', $active);
        //     return $jobs;

        // }  

        if ($order === 'month') {
            $mes = date('m');
            $jobs = Job::select()->whereMonth('created_at', '=', $mes)->orderBy('id', 'DESC')->paginate(12);
            $active = 'month';
            return view('jobs')
                ->with('jobs', $jobs)
                ->with('active', $active);
        }
    }
}
