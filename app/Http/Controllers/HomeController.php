<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;
use App\Postulation;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $usersCount = count($users);
        $employerJobs = Job::where('user_id', '=', Auth::user()->id)->orderBy('id', 'DESC')->paginate();
        $employerJobsCount = count($employerJobs);
        $graduateActivePostulations = Postulation::where('user_id', Auth::user()->id)->where('status', 'review')->get();
        $postulationActive = count($graduateActivePostulations);

        $postulationCount = Postulation::with(['user', 'job'])
        ->whereHas('job', function($query) {
            $query->where('user_id', Auth::id())
                  ->where('status', 'open');
        })->count();

        $recentOffer = Job::select()->orderBy('id', 'DESC')->take(4)->get();

        $recentGraduates = Postulation::with(['user', 'job'])
        ->whereHas('job', function($query) {
            $query->where('user_id', Auth::id())
                  ->where('status', 'open');
        })
        ->has('user')
        ->has('job')
        ->where('status', 'review')
        ->orderBy('id', 'DESC')->take(3)->get();

        return view('home', compact('usersCount', 'employerJobsCount', 'postulationActive', 'recentOffer', 'recentGraduates', 'postulationCount'));
    }

    public function profile()
    {   
        return view('profile');
    }



}
