<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Job;
use App\Study;
use App\Experience;
use Caffeinated\Shinobi\Models\Role;
use Validator;
use Mail;
use Storage;
use App\Postulation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class UsersController extends Controller
{
	public function index(Request $request)
	{
    	$users = User::select()->orderBy('id', 'DESC')->paginate(12);
    	return view('users.index')->with('users',$users);
	}

    public function getUsers()
    {
    	$users = User::select()->orderBy('id', 'DESC')->paginate(10);
    	return view('users.partials.usersTable')
    		->with('users', $users);
    }

    public function store(Request $request)
    {
        $rules = [
            'name'  => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'role'  => 'required',
        ];

        if ($request->role == 4) 
            $rules['password'] = 'required|string|min:6';
        
        $request->validate($rules);

        if ($request->role == 4) 
            $data['password'] = $pass = $request->password;
        else
            $data['password'] = $pass = str_random(1).'$'.str_random(2).'*'.str_random(2);

        $n = new User();
        $data['name'] 		= $n->name = $request->name;
        $data['to'] 		= $n->email = $request->email;
        $n->password = bcrypt($pass);
        
        if ($n->save()) {
            $n->roles()->attach($request->role);
            //SEND NOTIFICATION TO USER
            Mail::send('emails.userNotification', ['data' => $data], function($mail) use($data){
                $mail->subject('Suscription for '.env('APP_NAME'));
                $mail->to($data['to'], $data['name']);
            });
            return redirect(Route('users.index'))->with('success', 'Created user succefully');
        }
        return redirect(Route('users.index'))->with('error', 'Error to created');
    }

    public function destroy(Request $request)
    {
        if($request->user_id != 1){
            $user = User::find($request->user_id);
            $jobs = Job::where('user_id', $user->id)->get();
            foreach($jobs as $job){
                $job->delete();
            }
            $user->delete_motive = $request->motive;
            $user->save();
            $user->delete();
            return redirect(Route('main'));
            
        }else{
            return back()->with("message", "You can't delete this user");
        }

    }   

    public function sendNotification(Request $request){
        if ($request->ajax()){
            $user = User::find($request->id);
            $data['password']   = $pass = str_random(1).'$'.str_random(2).'*'.str_random(2);
            $data['name']       = $user->name;
            $data['to']         = $user->email;
            $user->password = bcrypt($pass);
            $user->save();
            Mail::send('emails.userNotification', ['data' => $data], function($mail) use($data){
                $mail->subject('Suscription for '.env('APP_NAME'));
                $mail->to($data['to'], $data['name']);
            });
        }
    }

    public function addStudy(Request $request){
        if ($request->ajax()) {
            $new = new Study();
            $new->user_id = Auth::user()->id;
            $new->school = $request->school;
            $new->degree = $request->degree;
            $new->start_date = $request->start_date;
            $new->end_date = $request->end_date;
            $new->description = $request->description;
            if ($new->save()) {
                return response()->json(['success'=>'Add study']);
            }else{
                return response()->json(['error'=>'Error to add']);
            }
        }
    }

    public function getUserStudies()
    {
        $studies = Study::select()->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->get();
        return view ('users.partials.getStudies', compact('studies'));
    }

    public function deleteUserStudies(Request $request){
        if($request->ajax()){
            $study = Study::find($request->id);
            if ($study->delete()) {
                return response()->json(['success'=>'Deleted study']);
            }else{
                return response()->json(['error'=>'Error to delete']);
            }
        }
    }

    public function addWork(Request $request){
        if ($request->ajax()) {
            $new = new Experience();
            $new->user_id       = Auth::user()->id;
            $new->company       = $request->company;
            $new->position     = $request->position;
            $new->location      = $request->location;
            $new->description   = $request->description;
            $new->start_date    = $request->start_date;
            $new->end_date      = $request->end_date;

            if ($new->save()) {
                return response()->json(['success'=>'Add work']);
            }else{
                return response()->json(['error'=>'Error to add']);
            }
        }
    }

    public function getUserWorks(){
        $works = Experience::select()->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->get();
        return view ('users.partials.getWorks', compact('works'));      
    }

    public function deleteUserWorks(Request $request)
    {
        if($request->ajax()){
            $work = Experience::find($request->id);
            if ($work->delete()) {
                return response()->json(['success'=>'Deleted work']);
            }else{
                return response()->json(['error'=>'Error to delete']);
            }
        }
    }

    public function freelancers(Request $request)
    {
        $students = User::whereHas('roles', function($query) {
            $query->where('slug', 'graduate');
        })->get();
        return view('users.freelancer', compact('students'));
    }

    public function filterSearch(Request $request)
    {

        return redirect('dashboard/user/filter/'.$request->filter_professional);
    }

    public function jobFilterOrder($order)
    {
        if ($order === 'alphabetical-order') {
            $students = User::whereHas('roles', function($query) {
                $query->where('slug', 'graduate');
            })->orderBy('name', 'ASC')->get();
            $active = 'alphabetical-order';
            return view('users.freelancer', compact('students', 'active'));
           
        }

        if ($order === 'graduation_date') {
            $students = User::whereHas('roles', function($query) {
                $query->where('slug', 'graduate');
            })->orderBy('graduation_date', 'ASC')->get();
            $active = 'graduation_date';
            return view('users.freelancer', compact('students', 'active'));
        }

    }

    public function uploadCV(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cv_file'          =>  'required|mimes:pdf,doc,docx',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $user = Auth::user();

        $resume = $request->cv_file;
        $file_route = time().'_'.$resume->getClientOriginalName();
        $user->path_cv = '/resume/'.$user->id.'/'.$file_route;
        Storage::disk('resume')->put($user->id.'/'.$file_route, file_get_contents($resume->getRealPath()));
        $user->save();

        return back();
    }

    public function deleteCV()
    {
        $user = User::find(Auth::user()->id);
        $user->path_cv = null;
        $user->save();
        return back();
    }

    public function edit($id)
    {
        if (!$user = User::find($id))
            return redirect(Route('users.index'))->with('error', 'User not found');

        $roles = DB::table('roles')->orderBy('name', 'ASC')->get();
        return view('users.edit', compact('user', 'roles'));
    }

    public function update(Request $request)
    {
        if (!$user = User::with('roles')->find($request->user_id))
            return redirect(Route('users.index'))->with('error', 'User not found');

        if ($request->name != $user->name)
            $user->name = $request->name;

        if ($request->email != $user->email)
            $user->email = $request->email;
        
        if ($request->role != $user->roles->first()->id)
            $user->roles()->sync($request->role);

        if (!Hash::check($request->password, $user->password)) {
            $user->password = bcrypt($request->password);
            $data['name']   = $request->name;
            $data['to']     = $request->email;
            $data['password']     = $request->password;
        }

        if ($user->save()) {
            if (isset($data)) {
                Mail::send('emails.changePasswordNotification', ['data' => $data], function($mail) use($data){
                    $mail->subject('Change password in '.env('APP_NAME'));
                    $mail->to($data['to'], $data['name']);
                });
            }

            return redirect(Route('users.index'))->with('success', 'Updated user succefully');
        }else{
            return redirect(Route('users.index'))->with('error', 'Error to update');
        }
    }

    public function create()
    {
        $roles = DB::table('roles')->orderBy('name', 'ASC')->get();
        return view('users.create', compact('roles'));
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            'password' => 'confirmed|min:6|max:18'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }else{
            if (Hash::check($request->current_password, Auth::user()->password)) {
               $user = User::find(Auth::user()->id);
               $user->password = bcrypt($request->password);
               $user->save();
               return back()->with('success', 'Password update');
            }
            return back()->with('error', 'Error to update password');
        }
    }

    public function adminPerfilUpdate(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $user->name = $request->name;
        $user->email = $request->email;

        if (isset($request->avatar)) {
            $img = $request->avatar;
            $file_route = time().'_'.$img->getClientOriginalName();
            $user->path_avatar  = '/img/profiles/'.$file_route;
            Storage::disk('profiles')->put($file_route, file_get_contents($img->getRealPath()));
        }
        if ($user->save()) {
            return back()->with('success', 'Data updated');
        }else{
            return back()->with('error', 'Error to update information');
        }
    }

    public function uploadVideo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'video_file'          =>  'required|mimes:mp4',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $user = Auth::user();

        $resume = $request->video_file;
        $file_route = time().'_'.$resume->getClientOriginalName();
        $user->video_presentation = '/video/'.$user->id.'/'.$file_route;
        Storage::disk('video')->put($user->id.'/'.$file_route, file_get_contents($resume->getRealPath()));
        if ($user->save()) {
            return back()->with('success', 'Data updated');
        }else{
            return back()->with('error', 'Error to update information');
        }
    }

    public function graduateOffers()
    {
        $postulations = Postulation::with('job.user')
                            ->where('user_id', Auth::id())
                            ->has('job')->orderBy('id', 'DESC')->paginate(9);
        return view('graduate.offers')
            ->with('postulations', $postulations);
    }

    public function graduateOfferDetails($id, $slug)
    {
        $postulation = Postulation::find($id);
        return view('graduate.postulationDetail', compact('postulation'));
    }

    public function publicProfile($email)
    {
        $user = User::where('email', $email)->firstOrFail();
        return view('publicProfile', compact('user'));
    }

    public function delete(Request $request)
    {
        if ($user = User::find($request->id)) {
            if($user->delete()) {
                return response()->json([], 204);
            }
        }
    }

}
