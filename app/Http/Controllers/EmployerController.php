<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Storage;

class EmployerController extends Controller
{
    public function myOffers(Request $request)
    {
        $n = 5;
        if ($show = isset($request->page) && $request->page != 1)
            $n = 6;

        $employerJobs = Job::with(['postulations' => function($query) {
            $query->whereHas('user');
        }, 'postulations.user'])->where('user_id', '=', Auth::id())->orderBy('status')->paginate($n);

    	return view('employer.myOffers', compact('employerJobs', 'show'));
    }

    public function myOffersDestroy(Request $request){
    	$job = Job::find($request->job_id);
    	
    	if ($job->delete()) {
    		return back()->with('success', 'Deleted offer successfully');
    	}else{
    		return back()->with('error', 'Error to deleted');
    	}
    }

    public function myOffersCreate()
    {
        return view('employer.createOffer');
    }

    public function myOffersStore(Request $request){
        $job = new Job();
        $job->user_id = Auth::user()->id;
        $job->title = $request->title;
        $job->description = $request->description;
        $job->slug = str_slug($request->title);
        $job->status = 'open';

        if ($job->save()) {
            return redirect(Route('employer.offers'))->with('success', 'Job save');
        }else{
            return redirect(Route('employer.offers'))->with('error', 'Job save');
        }
    }

    public function profileUpdate(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->website = $request->website;
        if (isset($request->avatar)) {
            $img = $request->avatar;
            $file_route = time().'_'.$img->getClientOriginalName();
            $user->path_avatar  = '/img/profiles/'.$file_route;
            Storage::disk('profiles')->put($file_route, file_get_contents($img->getRealPath()));
        }
        if ($user->save()) {
            return back()->with('success', 'Data updated');
        }else{
            return back()->with('error', 'Error to update information');
        }
    }

    public function searching(Request $request)
    {
        $employerJobs = Job::where('user_id', '=', Auth::user()->id)->orderBy('id', 'DESC')->paginate(6);
    	return view('employer.myOffers', compact('employerJobs'));
    }
}
