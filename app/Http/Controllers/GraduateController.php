<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;
use App\User;
use App\Postulation;
use App\Reference;
use App\Study;
use App\Experience;
use Illuminate\Support\Facades\Auth;
use Mail;

class GraduateController extends Controller
{
    public function offerDetail($id, $slug)
    {
    	$job = Job::where('id', $id)->where('slug', $slug)->firstOrFail();
    	$postulation = Postulation::where('job_id', $job->id)->where('user_id', Auth::user()->id)->get();
    	$postulationCont = count($postulation);

    	if ($postulationCont > 0) {
    		$validateAplication = 'true';
    	}else{
			$validateAplication = 'false';    	
		}
    	return view('graduate.offerDetail', compact('job', 'validateAplication'));
    }

    public function offerApply(Request $request)
    {
    	if ($request->ajax()) {
    		$new = new Postulation();
    		$new->job_id 	= $request->job_id;
    		$new->user_id 	= $request->user_id;
    		$new->status 	= 'review';

            $job = Job::find($request->job_id);
            $user = User::find($request->user_id);

            if ($new->save()) {
                //MAIL EMPLOYER
                Mail::send('emails.applyEmployerNotification', ['job' => $job, 'user' => $user], function($mail) use($job, $user){
                    $mail->subject('Buenos Aires TEFL CV');
                    $mail->to($job->user->email, $job->user->name);
                });

                //MAIL GRADUATE
                Mail::send('emails.applyGraduateNotification', ['job' => $job, 'user' => $user], function($mail) use($job, $user){
                    $mail->subject('your application for vacancy "'.$job->title.'" has been sent to the employer');
                    $mail->to($user->email, $user->name);
                });
                return response()->json(['success'=>'Add success']);
            }else{
                return response()->json(['error'=>'Save error']);
            }
    	}
    }

    public function graduateProfile()
    {
        return view('graduate.profile');
    }

    public function graduateProfileEdit()
    {
        $references = Reference::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->get();
        $studies = Study::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->get();
        $experiences = Experience::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->get();
        return view('graduate.profileEdit', compact('references', 'studies', 'experiences'));
    }
}
