<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study extends Model
{
    protected $table = 'studies';

    protected $fillable = [
    	'user_id', 'school', 'degree', 'description', 'start_date', 'end_date', 'note',
    ];

    /**
     * RELATIONSHIPS
     */
	
	//USERS: Un estudio pertenece a un solo usuario
    public function user(){
        return $this->belongsTo('App\User');
    }
}
