<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model
{
    use SoftDeletes;

    protected $table = 'jobs';

    protected $fillable = [
    	'user_id', 'title', 'slug', 'description', 'status', 
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * RELATIONSHIPS
     */
	
	//USER: 1 trabajo pertenece a un solo usuario
    public function user(){
        return $this->belongsTo('App\User');
    }
    
    //Postulation: Un job puede tener varias postulaciones
    public function postulations(){
        return $this->hasMany('App\Postulation');
    }

    /*QUERY SCOPES*/
    public function scopeTitle($query, $title)
    {
        if ($title) {
            return $query->where('title', 'like', '%'.$title.'%');        
        }
    }

    public function scopeDescription($query, $description)
    {
        if ($description) {
            return $query->orWhere('description', 'like', '%'.$description.'%');        
        }
    }
}
