<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postulation extends Model
{
    protected $table = 'postulations';

    protected $fillable = [
    	'job_id', 'user_id', 'status',
    ];


    /**
     * RELATIONSHIPS
     */
	
	//USERS: Una postulacion pertenece a un solo usuario
    public function user(){
        return $this->belongsTo('App\User');
    }

    //USERS: Una postulacion pertenece a un solo job
    public function job(){
        return $this->belongsTo('App\Job');
    }
}
