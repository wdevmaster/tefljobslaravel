<?php

namespace App\Traits;

use App\Review;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait HasReviews
{
    public function reviews(): MorphMany
    {
        return $this
                ->morphMany('App\Review', 'reviewable');
    }

    public function createReview($data, Model $author, Model $parent = null): bool
    {
        return $this
                ->getReviewModel()
                ->createReview($this, $data, $author);
    }

    public function updateReview($id, $data, Model $parent = null): bool
    {
        return $this
                ->getReviewModel()
                ->updateReview($id, $data);
    }

    public function deleteReview($id): bool
    {
        return $this
                ->getReviewModel()
                ->deleteReview($id);
    }

    public function getRating(): float
    {
        return round($this->reviews()->avg('rating'));
    }

    public function points(): float
    {
        return round($this->getRating()/$this->reviews()->count());
    }
    
    protected function getReviewModel(): Model
    {
        return new Review();
    }
}