<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $table = 'experiences';

    protected $fillable = [
    	'user_id', 'company', 'position', 'location', 'description', 'start_date', 'end_date',
    ];

    /**
     * RELATIONSHIPS
     */
	
	//USER: una experiencia le pertenece a un solo usuario
    public function user(){
        return $this->belongsTo('App\User');
    }
}
