<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $table = 'phones';

    protected $fillable = [
    	'user_id', 'code', 'number', 
    ];

    /**
     * RELATIONSHIPS
     */
	
	//USER: Un numero de telefono le pertenece a un solo usuario
    public function user(){
        return $this->belongsTo('App\User');
    }
}
