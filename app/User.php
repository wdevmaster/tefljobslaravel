<?php

namespace App;

use Caffeinated\Shinobi\Traits\ShinobiTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\HasReviews;

class User extends Authenticatable
{
    use Notifiable, ShinobiTrait, SoftDeletes, HasReviews;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'country', 'state', 'city', 'path_avatar', 'path_cv', 'address', 'birthdate'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    
    /**
     * RELATIONSHIPS
     */

    //JOBS: Un usuario puede tener varias ofertas de trabajos
    public function jobs(){
        return $this->hasMany('App\Jobs');
    }
    //JOBS: Un usuario puede tener varias referencias
    public function references(){
        return $this->hasMany('App\Reference');
    }
    //Postulation: Un usuario puede tener varias postulaciones
    public function postulations(){
        return $this->hasMany('App\Postulation');
    }
    
    //Phones: Un usuario puede tener varios numeros telefonicos
    public function phones(){
        return $this->hasMany('App\Phone');
    }

    //Studies: Un usuario puede tener varios estudios
    public function studies(){
        return $this->hasMany('App\Study');
    }

    //Experiences: Un usuario puede tener varias experiencias
    public function experiences(){
        return $this->hasMany('App\Experience');
    }

}
