<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
    protected $table = 'references';

    protected $fillable = [
    	'name', 'email', 'phone', 'user_id'
    ];


    /**
     * RELATIONSHIPS
     */
	
	//USERS: Una postulacion pertenece a un solo usuario
    public function user(){
        return $this->belongsTo('App\User');
    }
}
