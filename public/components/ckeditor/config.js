      CKEDITOR.editorConfig = function( config ) {
        config.toolbar = [
          { name: 'basicstyles', items: [ 'Bold', ] },
          { name: 'paragraph', items: [ 'NumberedList', 'BulletedList',] },
        ];
      };