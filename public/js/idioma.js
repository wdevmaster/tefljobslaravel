jQuery('#lang_es').click(function () {
    jQuery.ajax({
        url: 'https://www.laserairlines.com/locale/es',
        type: "get",
        data: 'lang_code=' + jQuery(this).val(),
        success: function () {
            window.location.reload();
        }
    });
});

jQuery('#lang_en').click(function () {
    jQuery.ajax({
        url: 'https://www.laserairlines.com/locale/en',
        type: "get",
        data: 'lang_code=' + jQuery(this).val(),
        success: function () {
            window.location.reload();
        }
    });
});
