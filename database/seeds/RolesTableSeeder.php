<?php

use Illuminate\Database\Seeder;

use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = Permission::all();

        $roles = [
            [
                'name' => 'admin',
                'slug' => 'admin',
                'description' => 'Administrador del sistema',
                'special' => 'all-access',
            ],
            [
                'name' => 'employer',
                'slug' => 'employer',
                'description' => 'User that publishes job offer',
                'special' => null,
                'permissions' => [ 
                    'users.edit', 
                    'users.show',
                    'jobs.create',
                    'jobs.index',
                    'jobs.edit',
                    'jobs.destroy',
                    'jobs.show',
                    'graduates.index',
                ]
            ],
            [
                'name' => 'supervisor',
                'slug' => 'supervisor',
                'description' => 'User who can only see everything without interacting with anything',
                'special' => null,
                'permissions' => [ 
                    'jobs.index',
                    'jobs.show',
                    'graduates.index',
                    'users.show'
                ]
            ],
            [
                'name' => 'graduate',
                'slug' => 'graduate',
                'description' => 'User that search job offer',
                'special' => null,
                'permissions' => [ 
                    'jobs.index',
                    'jobs.show',
                    'jobs.application',
                    'graduate.profile.edit',
                    'graduate.study',
                    'graduate.experience',
                ]
            ],

        ];

        foreach ($roles as $row) {
            $role = Role::create([
                'name' => $row['name'],
                'slug' => $row['slug'],
                'description' => $row['description'],
                'special' => $row['special'],
            ]);

            if (isset($row['permissions'])) {
                foreach ($row['permissions'] as $slug) {
                    if (!$permissions->where('slug', $slug)->first())
                        dd($slug);
                    $role->assignPermission($permissions->where('slug', $slug)->first()->id);
                }
            }
                
        }

    }
}
