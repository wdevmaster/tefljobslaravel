<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*------------------------------------------------------------------
        //                    ROLES
        -------------------------------------------------------------------*/
        //1
        Permission::create([
    		'name'	=> 'Crear rol',
    		'slug'	=> 'roles.create',
    		'description' => 'Crear un nuevo rol para usuarios del sistema',
        ]);
        //2
        Permission::create([
            'name'  => 'Navegar roles',
            'slug'  => 'roles.index',
            'description' => 'Lista y navega los roles de usuarios del sistema',
        ]);
        //3
        Permission::create([
            'name'  => 'Editar rol',
            'slug'  => 'roles.edit',
            'description' => 'Editar un rol del sistema',
        ]);
        //4
        Permission::create([
            'name'  => 'Eliminar rol',
            'slug'  => 'roles.destroy',
            'description' => 'Eliminar un rol de usuario del sistema',
        ]);
        //5
        Permission::create([
            'name'  => 'Ver rol',
            'slug'  => 'roles.show',
            'description' => 'Ver en detalle un rol del sistema',
        ]);


        /*------------------------------------------------------------------
        //                        USUARIOS
        -------------------------------------------------------------------*/
        //6
        Permission::create([
            'name'  => 'Crear usuario',
            'slug'  => 'users.create',
            'description' => 'Crear un nuevo usuario del sistema',
        ]);
        //7
        Permission::create([
            'name'  => 'Navegar usuarios',
            'slug'  => 'users.index',
            'description' => 'Lista y navega los usuarios del sistema',
        ]);
        //8
        Permission::create([
            'name'  => 'Editar usuario',
            'slug'  => 'users.edit',
            'description' => 'Editar un usuario del sistema',
        ]);
        //9
        Permission::create([
            'name'  => 'Eliminar usuario',
            'slug'  => 'users.destroy',
            'description' => 'Eliminar un usuario de usuario del sistema',
        ]);
        //10
        Permission::create([
            'name'  => 'Ver usuario',
            'slug'  => 'users.show',
            'description' => 'Ver en detalle un usuario del sistema',
        ]);
        Permission::create([
            'name'  => 'Cambiar Contraseña',
            'slug'  => 'users.password',
            'description' => 'Permite editar la contraseña para ingresar al sistema',
        ]);


        /*------------------------------------------------------------------
        //                        JOBS
        -------------------------------------------------------------------*/
        //11
        Permission::create([
            'name'  => 'Crear ofertas',
            'slug'  => 'jobs.create',
            'description' => 'Crear una nueva oferta de trabajo',
        ]);
        //12
        Permission::create([
            'name'  => 'Navegar ofertas',
            'slug'  => 'jobs.index',
            'description' => 'Lista y navega las ofertas publicadas en el sistema',
        ]);
        //13
        Permission::create([
            'name'  => 'Editar ofertas',
            'slug'  => 'jobs.edit',
            'description' => 'Editar una oferta del sistema',
        ]);
        //14
        Permission::create([
            'name'  => 'Eliminar oferta',
            'slug'  => 'jobs.destroy',
            'description' => 'Eliminar una oferta de trabajo del sistema',
        ]);
        //15
        Permission::create([
            'name'  => 'Ver oferta',
            'slug'  => 'jobs.show',
            'description' => 'Ver en detalle una oferta de trabajo del sistema',
        ]);
        //16
        Permission::create([
            'name'  => 'Aplicar a oferta',
            'slug'  => 'jobs.application',
            'description' => 'Aplicar para una oferta de trabajo',
        ]);

        /*------------------------------------------------------------------
        //                        graduate especial permissions
        -------------------------------------------------------------------*/
        //17
        Permission::create([
            'name'  => 'Editar perfil (graduate)',
            'slug'  => 'graduate.profile.edit',
            'description' => 'Editar el perfil de un egresado',
        ]);
        //18
        Permission::create([
            'name'  => 'Agregar estudio',
            'slug'  => 'graduate.study',
            'description' => 'Agregar un estudio al cv digital de un graduado',
        ]);
        //19
        Permission::create([
            'name'  => 'Agregar experiencia laboral',
            'slug'  => 'graduate.experience',
            'description' => 'Agregar experiencia laboral al cv digital de un graduado',
        ]);

        /*------------------------------------------------------------------
        //                        Students
        -------------------------------------------------------------------*/
        //20
        Permission::create([
            'name'  => 'Navegar freelacers',
            'slug'  => 'graduates.index',
            'description' => 'Lista y navega los perfiles de estudiantes',
        ]);
    }
}
