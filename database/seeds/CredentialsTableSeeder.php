<?php

use Illuminate\Database\Seeder;

use Caffeinated\Shinobi\Models\Role;
use App\User;

class CredentialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $roles = Role::all();
        $password = bcrypt('tefljobsmaster17.');
        $users = [
            [
                'name'	=> 'Admin',
                'email'	=> 'admin@user.com',
                'role' => 'admin',
            ],
            [
                'name'	=> 'Agencies',
                'email'	=> 'agencies@user.com',
                'role' => 'supervisor',
            ],
            [
                'name'  => 'Buenos Aires Employer',
                'email' => 'buenosairesemployer@user.com',
                'role' => 'employer',
            ],            
            [
                'name'	=> 'Graduate',
                'email'	=> 'graduate@user.com',
                'role' => 'graduate',
            ],
        ];

        foreach ($users as $row) {
            $user = User::create([
                'name'	=> $row['name'],
                'email'	=> $row['email'],
                'password' => $password,
            ]);

            if(isset($row['role'])) {
                if (!$roles->where('slug', $row['role'])->first()) 
                    dd($row['role']);
                $user->assignRole($roles->where('slug', $row['role'])->first()->id);
            }
        }
    }
}
