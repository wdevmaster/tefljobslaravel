<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Facker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Facker::create();

        for ($i=0; $i < $faker->numberBetween(5, 20); $i++) { 
            
            $role = $faker->randomElement([2, 4]);

            $user = factory(App\User::class)->create();

            $user->assignRole($role);

            factory(App\Phone::class)->create([
                'user_id' => $user->id
            ]);

            if ($role == 2) {
                factory(App\Job::class, $faker->numberBetween(1, 5))->create([
                    'user_id' => $user->id
                ]);
            } else {
                factory(App\Experience::class, $faker->numberBetween(1, 3))->create([
                    'user_id' => $user->id
                ]);
                factory(App\Study::class, $faker->numberBetween(1, 3))->create([
                    'user_id' => $user->id
                ]);
            }
        }
    }
}
