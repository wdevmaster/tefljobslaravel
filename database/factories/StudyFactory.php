<?php

use Faker\Generator as Faker;

$factory->define(App\Study::class, function (Faker $faker) {
    return [
        'user_id'       => random_int(2, 21),
        'school'	    => $faker->company,
        'description'   => $faker->sentence(15),
        'end_date'	    => $faker->randomElement([null, $faker->date('Y-m-d', 'now')]),
        'note'          => random_int(15, 20),
    ];
});
