<?php

use Faker\Generator as Faker;

$factory->define(App\Experience::class, function (Faker $faker) {
    return [
        'user_id'       => random_int(2, 21),
        'location'	    => $faker->address.' '.$faker->country,
        'description'   => $faker->sentence(15),
        'date'	    => $faker->randomElement([null, $faker->date('Y-m-d', 'now')]),
    ];
});
