<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Phone::class, function (Faker $faker) {
    return [
        'user_id' => random_int(2, 21),
        'number' => random_int(1000000, 9000000),
    ];
});
