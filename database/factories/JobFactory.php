<?php

use Faker\Generator as Faker;

$factory->define(App\Job::class, function (Faker $faker) {
    $title = $faker->sentence(4);
    return [
        'user_id'	=> random_int(2, 21),
        'title'	=> $title,
        'slug' => str_slug($title),
        'description' => $faker->sentence(200),
        'status' => 'open',
        'location' => 'Buenos Aires - Argentina',
        'website' => 'www.example.com',
    ];
});
